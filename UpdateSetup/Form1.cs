﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Threading;
using System.Windows.Forms;

[assembly: CLSCompliant(true)]
namespace UpdateSetup
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CurrentCulture;
            InitializeComponent();
            Thread thread = new Thread(Setup);
            thread.Start();
        }
        private string SetupFile = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + @"\GClient GT Setup.exe";
        private void Setup()
        {
            try
            {
                if (File.Exists(SetupFile))
                {
                    using (Process process2 = new Process())
                    {
                        process2.StartInfo.Arguments = "/passive /promptrestart";
                        process2.StartInfo.FileName = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + @"\GClient GT Setup.exe";
                        process2.Start();
                    }
                }
                Environment.Exit(0);
            }
            catch (Exception ex)
            {
                MessageBox.Show(Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n\n" + ex.Message, Properties.Resources.ResourceManager.GetString("FailedOperationTitle"), MessageBoxButtons.OK, MessageBoxIcon.Stop);
                Environment.Exit(0);
            }
        }
    }
}