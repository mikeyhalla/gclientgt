﻿using Launcher.Properties;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Threading;
using System.Windows.Forms;

namespace Launcher
{
    public partial class Update : Form
    {
        public Update()
        {
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CurrentCulture;
            InitializeComponent();
            Thread thread = new Thread(startDownload);
            thread.Start();
        }
        static string result = Path.GetTempPath();
        static string GClientGTZip = result + @"\GClientGT\GClientGT-Setup.zip";
        private void startDownload()
        {
            Directory.CreateDirectory(result + @"\GClientGT");
            DirectoryInfo di = new DirectoryInfo(result + @"\GClientGT");
            foreach (FileInfo file in di.GetFiles())
            {
                file.Delete();
            }
            foreach (DirectoryInfo dir in di.GetDirectories())
            {
                dir.Delete(true);
            }
            Directory.CreateDirectory(result + @"\GClientGT\Setup");
            Thread thread = new Thread(() => {
                using (WebClient client = new WebClient())
                {
                    client.DownloadProgressChanged += new DownloadProgressChangedEventHandler(client_DownloadProgressChanged);
                    client.DownloadFileCompleted += new AsyncCompletedEventHandler(client_DownloadFileCompleted);
                    client.DownloadFileAsync(new Uri("https://github.com/mikeyhalla/GClient/blob/master/Resources/UPLOAD/GClient%20GT%20Setup.zip?raw=true"), GClientGTZip);
                }
            });
            thread.Start();
        }
        void client_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
                this.BeginInvoke((MethodInvoker)delegate
                {
                    double bytesIn = double.Parse(e.BytesReceived.ToString(new CultureInfo("en-US")), CultureInfo.InvariantCulture);
                    double totalBytes = double.Parse(e.TotalBytesToReceive.ToString(new CultureInfo("en-US")), CultureInfo.InvariantCulture);
                    double percentage = bytesIn / totalBytes * 100;
                    label2.Text = Resources.ResourceManager.GetString("DownLoadDownloaded") + e.BytesReceived + Resources.ResourceManager.GetString("DownLoadOf") + e.TotalBytesToReceive;
                    progressBar1.Value = int.Parse(Math.Truncate(percentage).ToString(new CultureInfo("en-US")), CultureInfo.InvariantCulture);
                });
        }
        void client_DownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
        {
            this.BeginInvoke((MethodInvoker)delegate {
                label2.Text = Resources.ResourceManager.GetString("Complete");
            });
            ZipFile.ExtractToDirectory(GClientGTZip, result + @"\GClientGT\Setup");
            File.Copy(result + @"GClientGT\Setup\GClient GT Setup.exe", Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + @"\GClient GT Setup.exe", true);
            using (Process process = new Process())
            {
                process.StartInfo.FileName = Environment.CurrentDirectory + @"\UpdateSetup.exe";
                process.Start();
            }
            Environment.Exit(0);
        }
    }
}
