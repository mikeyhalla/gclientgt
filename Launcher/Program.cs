﻿using Launcher.Properties;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Net;
using System.ServiceProcess;
using System.Threading;
using System.Windows.Forms;
using System.Xml;
using GClientGTLibrary;

[assembly: CLSCompliant(true)]
namespace Launcher
{
    public partial class Form1 : Form
    {
        static double GClientGT = 23.5; //Version
        static string SysPath = (Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) + @"\GClientGT");
        static string xFile = SysPath + @"\File_Check.xml";
        static string SysFile = SysPath + @"\GClientGT.ini";
        static string GameListX = SysPath + @"\GameList.xml";
        static string Temp = Path.GetDirectoryName(Path.GetTempPath());
        static string TempPath = Temp + @"\GClientGT\";
        static string update = "NOTUPDATED";
        [STAThread]
        static void Main()
        {
            try
            {
                Thread.CurrentThread.CurrentUICulture = CultureInfo.CurrentCulture;
                Directory.CreateDirectory(SysPath);
                if (File.Exists(SysFile) && File.Exists(GameListX))
                {
                    Stream stream = null;
                    try
                    {
                        stream = new FileStream(SysFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                        using (StreamReader SysFile = new StreamReader(stream))
                        {
                            stream = null;
                            var readset = SysFile.ReadToEnd();
                            if (readset.Contains("Update=true"))
                            {
                                updateCheck();
                                xit();
                            }
                            else
                            {
                                check();
                                xit();
                            }
                        }
                    }
                    finally
                    {
                        if (stream != null)
                            stream.Dispose();
                    }
                }
                else
                {
                    GameList();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(Resources.ResourceManager.GetString("FailedOperation") + "\n\n" + ex.Message, Resources.ResourceManager.GetString("OperationFailed"), MessageBoxButtons.OK, MessageBoxIcon.Stop, MessageBoxDefaultButton.Button1);
            }
        }
        public static bool CheckForInternetConnection()
        {
            try
            {
                using (var client = new WebClient())
                {
                    using (var stream = client.OpenRead("http://www.google.com"))
                    {
                        return true;
                    }
                }
            }
            catch (Exception)
            {
                string result = DialogNoBox.Show(Resources.ResourceManager.GetString("NoInternet"));
                if (result == "OK")
                {
                    return false;
                }
                return false;
            }
        }
        static void updateCheck()
        {
            try
            {
                string url = @"https://raw.githubusercontent.com/mikeyhalla/GClient/master/Launcher/version.xml";
                using (WebClient client = new WebClient())
                {
                    client.DownloadFileCompleted += new AsyncCompletedEventHandler(client_DownloadFileCompleted);
                    client.DownloadFileAsync(new Uri(url), xFile);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(Resources.ResourceManager.GetString("FailedOperation") + "\n\n" + ex.Message, Resources.ResourceManager.GetString("OperationFailed"), MessageBoxButtons.OK, MessageBoxIcon.Stop, MessageBoxDefaultButton.Button1);
            }
        }
        static void GameList()
        {
            try
            {
                if (!File.Exists(GameListX))
                {
                    using (StringWriter stringwriter = new StringWriter(new CultureInfo("en-US")))
                    {
                        XmlTextWriter xmlTextWriter = new XmlTextWriter(stringwriter);
                        xmlTextWriter.Formatting = Formatting.Indented;
                        xmlTextWriter.WriteStartDocument();
                        xmlTextWriter.WriteStartElement("Games");
                        xmlTextWriter.WriteElementString("Game1", "0");
                        xmlTextWriter.WriteElementString("G1Args", "0");
                        xmlTextWriter.WriteElementString("GameImage1", "0");
                        xmlTextWriter.WriteElementString("Game2", "0");
                        xmlTextWriter.WriteElementString("G2Args", "0");
                        xmlTextWriter.WriteElementString("GameImage2", "0");
                        xmlTextWriter.WriteElementString("Game3", "0");
                        xmlTextWriter.WriteElementString("G3Args", "0");
                        xmlTextWriter.WriteElementString("GameImage3", "0");
                        xmlTextWriter.WriteElementString("Game4", "0");
                        xmlTextWriter.WriteElementString("G4Args", "0");
                        xmlTextWriter.WriteElementString("GameImage4", "0");
                        xmlTextWriter.WriteElementString("Game5", "0");
                        xmlTextWriter.WriteElementString("G5Args", "0");
                        xmlTextWriter.WriteElementString("GameImage5", "0");
                        xmlTextWriter.WriteElementString("Game6", "0");
                        xmlTextWriter.WriteElementString("G6Args", "0");
                        xmlTextWriter.WriteElementString("GameImage6", "0");
                        xmlTextWriter.WriteElementString("Game7", "0");
                        xmlTextWriter.WriteElementString("G7Args", "0");
                        xmlTextWriter.WriteElementString("GameImage7", "0");
                        xmlTextWriter.WriteElementString("Game8", "0");
                        xmlTextWriter.WriteElementString("G8Args", "0");
                        xmlTextWriter.WriteElementString("GameImage8", "0");
                        xmlTextWriter.WriteElementString("Game9", "0");
                        xmlTextWriter.WriteElementString("G9Args", "0");
                        xmlTextWriter.WriteElementString("GameImage9", "0");
                        xmlTextWriter.WriteElementString("Game10", "0");
                        xmlTextWriter.WriteElementString("G10Args", "0");
                        xmlTextWriter.WriteElementString("GameImage10", "0");
                        xmlTextWriter.WriteElementString("Game11", "0");
                        xmlTextWriter.WriteElementString("G11Args", "0");
                        xmlTextWriter.WriteElementString("GameImage11", "0");
                        xmlTextWriter.WriteElementString("Game12", "0");
                        xmlTextWriter.WriteElementString("G12Args", "0");
                        xmlTextWriter.WriteElementString("GameImage12", "0");
                        xmlTextWriter.WriteElementString("Game13", "0");
                        xmlTextWriter.WriteElementString("G13Args", "0");
                        xmlTextWriter.WriteElementString("GameImage13", "0");
                        xmlTextWriter.WriteElementString("Game14", "0");
                        xmlTextWriter.WriteElementString("G14Args", "0");
                        xmlTextWriter.WriteElementString("GameImage14", "0");
                        xmlTextWriter.WriteElementString("Game15", "0");
                        xmlTextWriter.WriteElementString("G15Args", "0");
                        xmlTextWriter.WriteElementString("GameImage15", "0");
                        xmlTextWriter.WriteElementString("Game16", "0");
                        xmlTextWriter.WriteElementString("G16Args", "0");
                        xmlTextWriter.WriteElementString("GameImage16", "0");
                        xmlTextWriter.WriteElementString("Game17", "0");
                        xmlTextWriter.WriteElementString("G17Args", "0");
                        xmlTextWriter.WriteElementString("GameImage17", "0");
                        xmlTextWriter.WriteElementString("Game18", "0");
                        xmlTextWriter.WriteElementString("G18Args", "0");
                        xmlTextWriter.WriteElementString("GameImage18", "0");
                        xmlTextWriter.WriteElementString("Game19", "0");
                        xmlTextWriter.WriteElementString("G19Args", "0");
                        xmlTextWriter.WriteElementString("GameImage19", "0");
                        xmlTextWriter.WriteElementString("Game20", "0");
                        xmlTextWriter.WriteElementString("G20Args", "0");
                        xmlTextWriter.WriteElementString("GameImage20", "0");
                        xmlTextWriter.WriteElementString("Game21", "0");
                        xmlTextWriter.WriteElementString("G21Args", "0");
                        xmlTextWriter.WriteElementString("GameImage21", "0");
                        xmlTextWriter.WriteElementString("Game22", "0");
                        xmlTextWriter.WriteElementString("G22Args", "0");
                        xmlTextWriter.WriteElementString("GameImage22", "0");
                        xmlTextWriter.WriteElementString("Game23", "0");
                        xmlTextWriter.WriteElementString("G23Args", "0");
                        xmlTextWriter.WriteElementString("GameImage23", "0");
                        xmlTextWriter.WriteElementString("Game24", "0");
                        xmlTextWriter.WriteElementString("G24Args", "0");
                        xmlTextWriter.WriteElementString("GameImage24", "0");
                        xmlTextWriter.WriteElementString("Game25", "0");
                        xmlTextWriter.WriteElementString("G25Args", "0");
                        xmlTextWriter.WriteElementString("GameImage25", "0");
                        xmlTextWriter.WriteElementString("Game26", "0");
                        xmlTextWriter.WriteElementString("G26Args", "0");
                        xmlTextWriter.WriteElementString("GameImage26", "0");
                        xmlTextWriter.WriteElementString("Game27", "0");
                        xmlTextWriter.WriteElementString("G27Args", "0");
                        xmlTextWriter.WriteElementString("GameImage27", "0");
                        xmlTextWriter.WriteElementString("Game28", "0");
                        xmlTextWriter.WriteElementString("G28Args", "0");
                        xmlTextWriter.WriteElementString("GameImage28", "0");
                        xmlTextWriter.WriteElementString("Game29", "0");
                        xmlTextWriter.WriteElementString("G29Args", "0");
                        xmlTextWriter.WriteElementString("GameImage29", "0");
                        xmlTextWriter.WriteElementString("Game30", "0");
                        xmlTextWriter.WriteElementString("G30Args", "0");
                        xmlTextWriter.WriteElementString("GameImage30", "0");
                        xmlTextWriter.WriteElementString("Game31", "0");
                        xmlTextWriter.WriteElementString("G31Args", "0");
                        xmlTextWriter.WriteElementString("GameImage31", "0");
                        xmlTextWriter.WriteElementString("Game32", "0");
                        xmlTextWriter.WriteElementString("G32Args", "0");
                        xmlTextWriter.WriteElementString("GameImage32", "0");
                        xmlTextWriter.WriteElementString("Game33", "0");
                        xmlTextWriter.WriteElementString("G33Args", "0");
                        xmlTextWriter.WriteElementString("GameImage33", "0");
                        xmlTextWriter.WriteElementString("Game34", "0");
                        xmlTextWriter.WriteElementString("G34Args", "0");
                        xmlTextWriter.WriteElementString("GameImage34", "0");
                        xmlTextWriter.WriteElementString("Game35", "0");
                        xmlTextWriter.WriteElementString("G35Args", "0");
                        xmlTextWriter.WriteElementString("GameImage35", "0");
                        xmlTextWriter.WriteElementString("Game36", "0");
                        xmlTextWriter.WriteElementString("G36Args", "0");
                        xmlTextWriter.WriteElementString("GameImage36", "0");
                        xmlTextWriter.WriteElementString("Game37", "0");
                        xmlTextWriter.WriteElementString("G37Args", "0");
                        xmlTextWriter.WriteElementString("GameImage37", "0");
                        xmlTextWriter.WriteElementString("Game38", "0");
                        xmlTextWriter.WriteElementString("G38Args", "0");
                        xmlTextWriter.WriteElementString("GameImage38", "0");
                        xmlTextWriter.WriteElementString("Game39", "0");
                        xmlTextWriter.WriteElementString("G39Args", "0");
                        xmlTextWriter.WriteElementString("GameImage39", "0");
                        xmlTextWriter.WriteElementString("Game40", "0");
                        xmlTextWriter.WriteElementString("G40Args", "0");
                        xmlTextWriter.WriteElementString("GameImage40", "0");
                        xmlTextWriter.WriteElementString("Game41", "0");
                        xmlTextWriter.WriteElementString("G41Args", "0");
                        xmlTextWriter.WriteElementString("GameImage41", "0");
                        xmlTextWriter.WriteElementString("Game42", "0");
                        xmlTextWriter.WriteElementString("G42Args", "0");
                        xmlTextWriter.WriteElementString("GameImage42", "0");
                        xmlTextWriter.WriteElementString("Game43", "0");
                        xmlTextWriter.WriteElementString("G43Args", "0");
                        xmlTextWriter.WriteElementString("GameImage43", "0");
                        xmlTextWriter.WriteElementString("Game44", "0");
                        xmlTextWriter.WriteElementString("G44Args", "0");
                        xmlTextWriter.WriteElementString("GameImage44", "0");
                        xmlTextWriter.WriteElementString("Game45", "0");
                        xmlTextWriter.WriteElementString("G45Args", "0");
                        xmlTextWriter.WriteElementString("GameImage45", "0");
                        xmlTextWriter.WriteElementString("Game46", "0");
                        xmlTextWriter.WriteElementString("G46Args", "0");
                        xmlTextWriter.WriteElementString("GameImage46", "0");
                        xmlTextWriter.WriteElementString("Game47", "0");
                        xmlTextWriter.WriteElementString("G47Args", "0");
                        xmlTextWriter.WriteElementString("GameImage47", "0");
                        xmlTextWriter.WriteElementString("Game48", "0");
                        xmlTextWriter.WriteElementString("G48Args", "0");
                        xmlTextWriter.WriteElementString("GameImage48", "0");
                        xmlTextWriter.WriteElementString("Game49", "0");
                        xmlTextWriter.WriteElementString("G49Args", "0");
                        xmlTextWriter.WriteElementString("GameImage49", "0");
                        xmlTextWriter.WriteElementString("Game50", "0");
                        xmlTextWriter.WriteElementString("G50Args", "0");
                        xmlTextWriter.WriteElementString("GameImage50", "0");
                        xmlTextWriter.WriteElementString("Game51", "0");
                        xmlTextWriter.WriteElementString("G51Args", "0");
                        xmlTextWriter.WriteElementString("GameImage51", "0");
                        xmlTextWriter.WriteElementString("Game52", "0");
                        xmlTextWriter.WriteElementString("G52Args", "0");
                        xmlTextWriter.WriteElementString("GameImage52", "0");
                        xmlTextWriter.WriteElementString("Game53", "0");
                        xmlTextWriter.WriteElementString("G53Args", "0");
                        xmlTextWriter.WriteElementString("GameImage53", "0");
                        xmlTextWriter.WriteElementString("Game54", "0");
                        xmlTextWriter.WriteElementString("G54Args", "0");
                        xmlTextWriter.WriteElementString("GameImage54", "0");
                        xmlTextWriter.WriteElementString("Game55", "0");
                        xmlTextWriter.WriteElementString("G55Args", "0");
                        xmlTextWriter.WriteElementString("GameImage55", "0");
                        xmlTextWriter.WriteElementString("Game56", "0");
                        xmlTextWriter.WriteElementString("G56Args", "0");
                        xmlTextWriter.WriteElementString("GameImage56", "0");
                        xmlTextWriter.WriteElementString("Game57", "0");
                        xmlTextWriter.WriteElementString("G57Args", "0");
                        xmlTextWriter.WriteElementString("GameImage57", "0");
                        xmlTextWriter.WriteElementString("Game58", "0");
                        xmlTextWriter.WriteElementString("G58Args", "0");
                        xmlTextWriter.WriteElementString("GameImage58", "0");
                        xmlTextWriter.WriteElementString("Game59", "0");
                        xmlTextWriter.WriteElementString("G59Args", "0");
                        xmlTextWriter.WriteElementString("GameImage59", "0");
                        xmlTextWriter.WriteElementString("Game60", "0");
                        xmlTextWriter.WriteElementString("G60Args", "0");
                        xmlTextWriter.WriteElementString("GameImage60", "0");
                        xmlTextWriter.WriteEndElement();
                        xmlTextWriter.WriteEndDocument();
                        XmlDocument docSave = new XmlDocument();
                        docSave.LoadXml(stringwriter.ToString());
                        docSave.Save(GameListX);
                        SetIni();
                    }
                }
                else
                {
                    SetIni();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(Resources.ResourceManager.GetString("FailedOperation") + "\n\n" + ex.Message, Resources.ResourceManager.GetString("OperationFailed"), MessageBoxButtons.OK, MessageBoxIcon.Stop, MessageBoxDefaultButton.Button1);
            }
        }
        static void SetIni()
        {
            try
            {
                if (!File.Exists(SysFile))
                {
                    var mySysFile = File.Create(SysFile);
                    mySysFile.Close();
                    check();
                }
                else
                {
                    check();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(Resources.ResourceManager.GetString("FailedOperation") + "\n\n" + ex.Message, Resources.ResourceManager.GetString("OperationFailed"), MessageBoxButtons.OK, MessageBoxIcon.Stop, MessageBoxDefaultButton.Button1);
            }
        }
        static void check()
        {
            try
            {
                Process[] aProc = Process.GetProcessesByName("GClient");
                if (aProc.Length > 0)
                {
                    string result = DialogNoBox.Show(Resources.ResourceManager.GetString("GClientRunning"));
                    if (result == null)
                    {
                        Environment.Exit(0);
                    }
                }
                else
                {
                    Process.Start(Path.GetDirectoryName(Application.ExecutablePath) + @"\GClient.exe");
                    Thread.Sleep(10000);
                    Stream stream = null;
                    try
                    {
                        stream = new FileStream(SysFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                        using (StreamReader SysFile = new StreamReader(stream))
                        {
                            stream = null;
                            using (ServiceController GClientBoost = new ServiceController("GClientBoost"))
                            {
                                if (GClientBoost.Status.Equals(ServiceControllerStatus.Stopped))
                                {
                                    Thread.Sleep(5000);
                                    if (SysFile.ReadToEnd().Contains("Boost=true"))
                                    {
                                        if (GClientBoost.Status.Equals(ServiceControllerStatus.Stopped))
                                        {
                                            Thread thread = new Thread(BringMessageTop);
                                            thread.Start();
                                            string message = DialogNoBoxYesNo.Show(Resources.ResourceManager.GetString("StartService"));
                                            if (message == "YES")
                                            {
                                                GClientBoost.Start();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    finally
                    {
                        if (stream != null)
                            stream.Dispose();
                    }
                    update = "NONE";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(Resources.ResourceManager.GetString("FailedOperation") + "\n\n" + ex.Message, Resources.ResourceManager.GetString("OperationFailed"), MessageBoxButtons.OK, MessageBoxIcon.Stop, MessageBoxDefaultButton.Button1);
                throw;
            }
        }
        static void BringMessageTop()
        {
            bool NotTop = true;
            while (NotTop)
            {
                Thread.Sleep(500);
                UIntPtr handle = NativeMethods.CriticalHandle.Find(null, @"Message");
                if (!handle.Equals(null) || !handle.Equals(0))
                {
                    NativeMethods.CriticalHandle.BringToFront(null, Resources.ResourceManager.GetString("Message"));
                    NotTop = false;
                }
                else
                {
                    Thread.Sleep(50);
                }
            }
        }
        static void client_DownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
        {
            try
            {
                if (CheckForInternetConnection())
                {
                    using (XmlTextReader reader = new XmlTextReader(xFile))
                    {
                        XmlNodeType type;
                        while (reader.Read())
                        {
                            type = reader.NodeType;
                            if (type == XmlNodeType.Element)
                            {
                                if (reader.Name == "GClientGT")
                                {
                                    reader.Read();
                                    double result = 0.0;
                                    if (double.TryParse(reader.Value, out result))
                                    {
                                        if (result > GClientGT)
                                        {
                                            Thread thread = new Thread(BringMessageTop);
                                            thread.Start();
                                            string message = DialogNoBoxYesNo.Show(Resources.ResourceManager.GetString("UpdateAvailable") + "\n" + Resources.ResourceManager.GetString("UpdateNow"));
                                            if (message == "YES")
                                            {
                                                using (ServiceController GClientBoost = new ServiceController("GClientBoost"))
                                                {
                                                    if (GClientBoost.Status.Equals(ServiceControllerStatus.Running))
                                                    {
                                                        GClientBoost.Stop();
                                                    }
                                                }
                                                using (Update update = new Launcher.Update())
                                                {
                                                    update.ShowDialog();
                                                }
                                            }
                                            else
                                            {
                                                check();
                                            }
                                        }
                                        else
                                        {
                                            check();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    check();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(Resources.ResourceManager.GetString("FailedOperation") + "\n\n" + ex.Message, Resources.ResourceManager.GetString("OperationFailed"), MessageBoxButtons.OK, MessageBoxIcon.Stop, MessageBoxDefaultButton.Button1);
                throw;
            }
        }
        static void xit()
        {
            try
            {
                while (true)
                {
                    Thread.Sleep(2000);
                    if (update == "NONE")
                    {
                        if (Directory.Exists(TempPath + @"\Setup"))
                        {
                            DirectoryInfo info2 = new DirectoryInfo(TempPath + @"\Setup");
                            foreach (FileInfo Allfiles in info2.GetFiles())
                            {
                                try
                                {
                                    Allfiles.Delete();
                                    Directory.Delete(TempPath + @"\Setup");
                                }
                                catch (Exception ex)
                                {
                                    Trace.WriteLine(ex);
                                }
                            }
                        }
                        Environment.Exit(0);
                    }
                    else
                    {
                        Thread.Sleep(50);
                    }
                }
            }
            catch (Exception ex)
            {
                Trace.WriteLine(Resources.ResourceManager.GetString("FailedOperation") + "\n\n" + ex.Message, Resources.ResourceManager.GetString("OperationFailed"));
            }
        }
    }
}