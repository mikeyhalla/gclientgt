﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Launcher.Properties {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Launcher.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Completed.
        /// </summary>
        internal static string Complete {
            get {
                return ResourceManager.GetString("Complete", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Downloaded .
        /// </summary>
        internal static string DownLoadDownloaded {
            get {
                return ResourceManager.GetString("DownLoadDownloaded", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Downloading GClient GT Setup Files....
        /// </summary>
        internal static string Downloading {
            get {
                return ResourceManager.GetString("Downloading", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Downloading....
        /// </summary>
        internal static string Downloading2 {
            get {
                return ResourceManager.GetString("Downloading2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to  of .
        /// </summary>
        internal static string DownLoadOf {
            get {
                return ResourceManager.GetString("DownLoadOf", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Failed to perform the specified operation:.
        /// </summary>
        internal static string FailedOperation {
            get {
                return ResourceManager.GetString("FailedOperation", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to GClient GT.
        /// </summary>
        internal static string GClientGT {
            get {
                return ResourceManager.GetString("GClientGT", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Icon similar to (Icon).
        /// </summary>
        internal static System.Drawing.Icon GClientGTLogo {
            get {
                object obj = ResourceManager.GetObject("GClientGTLogo", resourceCulture);
                return ((System.Drawing.Icon)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to GClient GT is already running!.
        /// </summary>
        internal static string GClientRunning {
            get {
                return ResourceManager.GetString("GClientRunning", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to GClient GT.
        /// </summary>
        internal static string GClientTitle {
            get {
                return ResourceManager.GetString("GClientTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Message.
        /// </summary>
        internal static string Message {
            get {
                return ResourceManager.GetString("Message", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to NO.
        /// </summary>
        internal static string NO {
            get {
                return ResourceManager.GetString("NO", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to It appears that you do not have an internet connection. You may consider turning off automatic updates..
        /// </summary>
        internal static string NoInternet {
            get {
                return ResourceManager.GetString("NoInternet", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to OK.
        /// </summary>
        internal static string OK {
            get {
                return ResourceManager.GetString("OK", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Operation failed.
        /// </summary>
        internal static string OperationFailed {
            get {
                return ResourceManager.GetString("OperationFailed", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to GClientBoost Service.
        /// </summary>
        internal static string ServiceTitle {
            get {
                return ResourceManager.GetString("ServiceTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Do you want to start the GClientBoost Service?.
        /// </summary>
        internal static string StartService {
            get {
                return ResourceManager.GetString("StartService", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Update.
        /// </summary>
        internal static string Update {
            get {
                return ResourceManager.GetString("Update", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to There is an update available for GClient GT!.
        /// </summary>
        internal static string UpdateAvailable {
            get {
                return ResourceManager.GetString("UpdateAvailable", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Would you like to update now?.
        /// </summary>
        internal static string UpdateNow {
            get {
                return ResourceManager.GetString("UpdateNow", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to GClient GT Update.
        /// </summary>
        internal static string UpdateTitle {
            get {
                return ResourceManager.GetString("UpdateTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Updating....
        /// </summary>
        internal static string Updating {
            get {
                return ResourceManager.GetString("Updating", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to YES.
        /// </summary>
        internal static string YES {
            get {
                return ResourceManager.GetString("YES", resourceCulture);
            }
        }
    }
}
