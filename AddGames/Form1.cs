﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.IO;
using System.Xml;
using System.Linq;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using GClientGTLibrary;
using System.Threading;
using System.ComponentModel;
using System.Globalization;

[assembly: CLSCompliant(true)]
namespace AddGames
{
    public class Form1 : Form
    {
        Thread backgroundThread = null;
        private Container components = null;
        private void Form1_Load(object sender, EventArgs e)
        {
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CurrentCulture;
            Directory.CreateDirectory(Variables.homeFolder);
            backgroundThread = new Thread(RefreshView);
            backgroundThread.IsBackground = true;
            backgroundThread.Start();
        }
        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (backgroundThread.IsAlive)
            {
                backgroundThread.Abort();
            }
            if (Variables.GameFolder)
            {
                Variables.GameFolder = false;
            }
            else if (!Variables.GameFolder)
            {
                Variables.GameFolder = true;
            }
            Variables.panel3.Show();
            Variables.richTextbox3.Show();
            Variables.progressBar1.Show();
            backgroundThread = new Thread(RefreshView);
            backgroundThread.IsBackground = true;
            backgroundThread.Start();
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (backgroundThread.IsAlive)
            {
                backgroundThread.Abort();
            }
            if (Variables.StartGames)
            {
                Variables.StartGames = false;
            }
            else if (!Variables.StartGames)
            {
                Variables.StartGames = true;
            }
            Variables.panel3.Show();
            Variables.richTextbox3.Show();
            Variables.progressBar1.Show();
            backgroundThread = new Thread(RefreshView);
            backgroundThread.IsBackground = true;
            backgroundThread.Start();
        }

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {
            if (backgroundThread.IsAlive)
            {
                backgroundThread.Abort();
            }
            if (Variables.StartPrograms)
            {
                Variables.StartPrograms = false;
            }
            else if (!Variables.StartPrograms)
            {
                Variables.StartPrograms = true;
            }
            Variables.panel3.Show();
            Variables.richTextbox3.Show();
            Variables.progressBar1.Show();
            backgroundThread = new Thread(RefreshView);
            backgroundThread.IsBackground = true;
            backgroundThread.Start();
        }

        private void checkBox4_CheckedChanged(object sender, EventArgs e)
        {
            if (backgroundThread.IsAlive)
            {
                backgroundThread.Abort();
            }
            if (Variables.ProgFiles)
            {
                Variables.ProgFiles = false;
            }
            else if (!Variables.ProgFiles)
            {
                Variables.ProgFiles = true;
            }
            Variables.panel3.Show();
            Variables.richTextbox3.Show();
            Variables.progressBar1.Show();
            backgroundThread = new Thread(RefreshView);
            backgroundThread.IsBackground = true;
            backgroundThread.Start();
        }

        private void checkBox5_CheckedChanged(object sender, EventArgs e)
        {
            if (backgroundThread.IsAlive)
            {
                backgroundThread.Abort();
            }
            if (Variables.SteamGames)
            {
                Variables.SteamGames = false;
            }
            else if (!Variables.SteamGames)
            {
                Variables.SteamGames = true;
            }
            Variables.panel3.Show();
            Variables.richTextbox3.Show();
            Variables.progressBar1.Show();
            backgroundThread = new Thread(RefreshView);
            backgroundThread.IsBackground = true;
            backgroundThread.Start();
        }
        public Form1()
        {
            InitializeComponent();
            Directory.CreateDirectory(Variables.TempPath);
        }
        private void RefreshView()
        {
            Variables.listBox1.Invoke(new Action(() => Variables.listBox1.Items.Clear()));
            Variables.empty = true;
            //Common Start menu games
            if (Directory.Exists(Variables.CommonStartMenu) && Variables.StartGames)
            {
                int items = 0;
                string[] CommonStartMenufiles = Directory.GetFiles(Variables.CommonStartMenu, "*.lnk", SearchOption.AllDirectories);
                foreach (string file in CommonStartMenufiles)
                {
                    items++;
                    string newFile = Path.GetFileNameWithoutExtension(file);
                    if (!newFile.StartsWith("unin") && !newFile.StartsWith("UNIN") && !newFile.StartsWith("Unin"))
                        Variables.listBox1.Invoke(new Action(() => Variables.listBox1.Items.Add(file)));
                    if (items > 0)
                        Variables.empty = false;
                }
            }
            //User start menu games
            if (Directory.Exists(Variables.StartMenu) && Variables.StartGames)
            {
                int items = 0;
                string[] StartMenufiles = Directory.GetFiles(Variables.StartMenu, "*.lnk", SearchOption.AllDirectories);
                foreach (string file in StartMenufiles)
                {
                    items++;
                    string newFile = Path.GetFileNameWithoutExtension(file);
                    if (!newFile.StartsWith("unin") && !newFile.StartsWith("UNIN") && !newFile.StartsWith("Unin"))
                        Variables.listBox1.Invoke(new Action(() => Variables.listBox1.Items.Add(file)));
                    if (items > 0)
                        Variables.empty = false;
                }
            }
            //Games Folder
            if (Directory.Exists(Variables.GamesFolder) && Variables.GameFolder)
            {
                int items = 0;
                string[] Gamesfiles = Directory.GetFiles(Variables.GamesFolder, "*.exe", SearchOption.AllDirectories);
                foreach (string file in Gamesfiles)
                {
                    items++;
                    string newFile = Path.GetFileNameWithoutExtension(file);
                    if (!newFile.StartsWith("unin") && !newFile.StartsWith("UNIN") && !newFile.StartsWith("Unin"))
                        Variables.listBox1.Invoke(new Action(() => Variables.listBox1.Items.Add(file)));
                    if (items > 0)
                        Variables.empty = false;
                }
            }
            //Desktop *.lnk
            if (Directory.Exists(Variables.homeFolder))
            {
                int items = 0;
                string[] homeFolderfiles = Directory.GetFiles(Variables.homeFolder, "*.lnk");
                foreach (string file in homeFolderfiles)
                {
                    items++;
                    string newFile = Path.GetFileNameWithoutExtension(file);
                    if (!newFile.StartsWith("unin") && !newFile.StartsWith("UNIN") && !newFile.StartsWith("Unin"))
                        Variables.listBox1.Invoke(new Action(() => Variables.listBox1.Items.Add(file)));
                    if (items > 0)
                        Variables.empty = false;
                }
            }
            //Public Desktop *.lnk
            if (Directory.Exists(System.Environment.GetFolderPath(Environment.SpecialFolder.CommonDesktopDirectory)))
            {
                int items = 0;
                string[] homeFolderfiles = Directory.GetFiles(System.Environment.GetFolderPath(Environment.SpecialFolder.CommonDesktopDirectory), "*.lnk");
                foreach (string file in homeFolderfiles)
                {
                    items++;
                    string newFile = Path.GetFileNameWithoutExtension(file);
                    if (!newFile.StartsWith("unin") && !newFile.StartsWith("UNIN") && !newFile.StartsWith("Unin"))
                        Variables.listBox1.Invoke(new Action(() => Variables.listBox1.Items.Add(file)));
                    if (items > 0)
                        Variables.empty = false;
                }
            }
            //Desktop *.url (Steam)
            if (Directory.Exists(Variables.homeFolder) && Variables.SteamGames)
            {
                int items = 0;
                string[] homeFolderfiles = Directory.GetFiles(Variables.homeFolder, "*.url");
                foreach (string file in homeFolderfiles)
                {
                    items++;
                    string newFile = Path.GetFileNameWithoutExtension(file);
                    if (!newFile.StartsWith("unin") && !newFile.StartsWith("UNIN") && !newFile.StartsWith("Unin"))
                        Variables.listBox1.Invoke(new Action(() => Variables.listBox1.Items.Add(file)));
                    if (items > 0)
                        Variables.empty = false;
                }
            }
            //Public Desktop *.url (Steam)
            if (Directory.Exists(System.Environment.GetFolderPath(Environment.SpecialFolder.CommonDesktopDirectory)) && Variables.SteamGames)
            {
                int items = 0;
                string[] homeFolderfiles = Directory.GetFiles(System.Environment.GetFolderPath(Environment.SpecialFolder.CommonDesktopDirectory), "*.url");
                foreach (string file in homeFolderfiles)
                {
                    items++;
                    string newFile = Path.GetFileNameWithoutExtension(file);
                    if (!newFile.StartsWith("unin") && !newFile.StartsWith("UNIN") && !newFile.StartsWith("Unin"))
                        Variables.listBox1.Invoke(new Action(() => Variables.listBox1.Items.Add(file)));
                    if (items > 0)
                        Variables.empty = false;
                }
            }
            //Start Menu All Users Programs
            if (Directory.Exists(Variables.CommonPrograms) && Variables.StartPrograms)
            {
                int items = 0;
                string[] CommonProgramsfiles = Directory.GetFiles(Variables.CommonPrograms, "*.lnk", SearchOption.AllDirectories);
                foreach (string file in CommonProgramsfiles)
                {
                    items++;
                    string newFile = Path.GetFileNameWithoutExtension(file);
                    if (!newFile.StartsWith("unin") && !newFile.StartsWith("UNIN") && !newFile.StartsWith("Unin"))
                        Variables.listBox1.Invoke(new Action(() => Variables.listBox1.Items.Add(file)));
                    if (items > 0)
                        Variables.empty = false;
                }
            }
            //Start Menu User Programs
            if (Directory.Exists(Variables.Programs) && Variables.StartPrograms)
            {
                int items = 0;
                string[] Programsfiles = Directory.GetFiles(Variables.Programs, "*.lnk", SearchOption.AllDirectories);
                foreach (string file in Programsfiles)
                {
                    items++;
                    string newFile = Path.GetFileNameWithoutExtension(file);
                    if (!newFile.StartsWith("unin") && !newFile.StartsWith("UNIN") && !newFile.StartsWith("Unin"))
                        Variables.listBox1.Invoke(new Action(() => Variables.listBox1.Items.Add(file)));
                    if (items > 0)
                        Variables.empty = false;
                }
            }
            //Program Files x86
            if (Directory.Exists(Variables.ProgramFilesx86) && Variables.ProgFiles)
            {
                int items = 0;
                string[] PFilesx86 = Directory.GetFiles(Variables.ProgramFilesx86, "*.exe", SearchOption.AllDirectories);
                foreach (string file in PFilesx86)
                {
                    items++;
                    string newFile = Path.GetFileNameWithoutExtension(file);
                    if (!newFile.StartsWith("unin") && !newFile.StartsWith("UNIN") && !newFile.StartsWith("Unin"))
                        Variables.listBox1.Invoke(new Action(() => Variables.listBox1.Items.Add(file)));
                    if (items > 0)
                        Variables.empty = false;
                }
            }
            //Program Files
            if (Directory.Exists(Variables.ProgamFilesStandard) && Variables.ProgFiles)
            {
                int items = 0;
                string[] PFilesStandard = Directory.GetFiles(Variables.ProgamFilesStandard, "*.exe", SearchOption.AllDirectories);
                foreach (string file in PFilesStandard)
                {
                    items++;
                    string newFile = Path.GetFileNameWithoutExtension(file);
                    if (!newFile.StartsWith("unin") && !newFile.StartsWith("UNIN") && !newFile.StartsWith("Unin"))
                        Variables.listBox1.Invoke(new Action(() => Variables.listBox1.Items.Add(file)));
                    if (items > 0)
                        Variables.empty = false;
                }
            }
            if (Variables.empty)
            {
                DialogNoBox.Show("There doesn't appear to be any games in these directories! \n" + "Try adding games manually.");
            }
            Thread.Sleep(500);
            Variables.progressBar1.Invoke(new Action(() => Variables.progressBar1.Hide()));
            Variables.panel3.Invoke(new Action(() => Variables.panel3.Hide()));
            Variables.richTextbox3.Invoke(new Action(() => Variables.richTextbox3.Hide()));
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    if (Variables.listBox1 != null)
                        Variables.listBox1.Dispose();
                    if (Variables.button1 != null)
                        Variables.button1.Dispose();
                    if (Variables.panel1 != null)
                        Variables.panel1.Dispose();
                    if (Variables.newImage != null)
                        Variables.newImage.Dispose();
                    if (Variables.label1 != null)
                        Variables.label1.Dispose();
                    if (Variables.pictureBox1 != null)
                        Variables.pictureBox1.Dispose();
                    if (Variables.panel2 != null)
                        Variables.panel2.Dispose();
                    if (Variables.bitmap != null)
                        Variables.bitmap.Dispose();
                    if (Variables.encoderParameter != null)
                        Variables.encoderParameter.Dispose();
                    if (Variables.encoderParameters != null)
                        Variables.encoderParameters.Dispose();
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }
        private void InitializeComponent()
        {
            ComponentResourceManager resources = new ComponentResourceManager(typeof(Form1));
            Variables.button1 = new Button();
            Variables.panel1 = new Panel();
            Variables.checkBox5 = new CheckBox();
            Variables.checkBox4 = new CheckBox();
            Variables.checkBox3 = new CheckBox();
            Variables.checkBox2 = new CheckBox();
            Variables.checkBox1 = new CheckBox();
            Variables.progressBar1 = new ProgressBar();
            Variables.button3 = new Button();
            Variables.button2 = new Button();
            Variables.label1 = new Label();
            Variables.panel2 = new Panel();
            Variables.pictureBox1 = new PictureBox();
            Variables.listBox1 = new CheckedListBox();
            Variables.richTextbox3 = new RichTextBox();
            Variables.panel3 = new Panel();
            Variables.panel1.SuspendLayout();
            Variables.panel2.SuspendLayout();
            ((ISupportInitialize)(Variables.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            Variables.button1.Anchor = ((AnchorStyles)((AnchorStyles.Top | AnchorStyles.Right)));
            Variables.button1.BackColor = Color.FromArgb(70, 70, 70);
            Variables.button1.FlatStyle = FlatStyle.Popup;
            Variables.button1.ForeColor = Color.White;
            Variables.button1.Location = new Point(663, 1);
            Variables.button1.Name = "button1";
            Variables.button1.Size = new Size(50, 25);
            Variables.button1.TabIndex = 1;
            Variables.button1.Text = " X";
            Variables.button1.UseVisualStyleBackColor = false;
            Variables.button1.Click += new EventHandler(button1_Click);
            // 
            // panel1
            // 
            Variables.panel1.Anchor = ((AnchorStyles)((((AnchorStyles.Top | AnchorStyles.Bottom)
            | AnchorStyles.Left)
            | AnchorStyles.Right)));
            Variables.panel1.BackColor = Color.FromArgb(35, 35, 35);
            Variables.panel1.Controls.Add(Variables.checkBox5);
            Variables.panel1.Controls.Add(Variables.checkBox4);
            Variables.panel1.Controls.Add(Variables.checkBox3);
            Variables.panel1.Controls.Add(Variables.checkBox2);
            Variables.panel1.Controls.Add(Variables.checkBox1);
            Variables.panel1.Controls.Add(Variables.progressBar1);
            Variables.panel1.Controls.Add(Variables.button3);
            Variables.panel1.Controls.Add(Variables.button2);
            Variables.panel1.Location = new Point(1, 1);
            Variables.panel1.Name = "panel1";
            Variables.panel1.Size = new Size(715, 407);
            Variables.panel1.TabIndex = 8;
            // 
            // checkBox5
            // 
            Variables.checkBox5.AutoSize = true;
            Variables.checkBox5.Checked = true;
            Variables.checkBox5.CheckState = CheckState.Checked;
            Variables.checkBox5.ForeColor = Color.FromArgb(224, 224, 224);
            Variables.checkBox5.Location = new Point(524, 376);
            Variables.checkBox5.Name = "checkBox5";
            Variables.checkBox5.Size = new Size(92, 17);
            Variables.checkBox5.TabIndex = 16;
            Variables.checkBox5.Text = "Steam Games";
            Variables.checkBox5.UseVisualStyleBackColor = true;
            Variables.checkBox5.CheckedChanged += new EventHandler(checkBox5_CheckedChanged);
            // 
            // checkBox4
            // 
            Variables.checkBox4.AutoSize = true;
            Variables.checkBox4.ForeColor = Color.FromArgb(224, 224, 224);
            Variables.checkBox4.Location = new Point(429, 376);
            Variables.checkBox4.Name = "checkBox4";
            Variables.checkBox4.Size = new Size(89, 17);
            Variables.checkBox4.TabIndex = 15;
            Variables.checkBox4.Text = "Program Files";
            Variables.checkBox4.UseVisualStyleBackColor = true;
            Variables.checkBox4.CheckedChanged += new EventHandler(checkBox4_CheckedChanged);
            // 
            // checkBox3
            // 
            Variables.checkBox3.AutoSize = true;
            Variables.checkBox3.ForeColor = Color.FromArgb(224, 224, 224);
            Variables.checkBox3.Location = new Point(298, 376);
            Variables.checkBox3.Name = "checkBox3";
            Variables.checkBox3.Size = new Size(125, 17);
            Variables.checkBox3.TabIndex = 14;
            Variables.checkBox3.Text = "Start Menu Programs";
            Variables.checkBox3.UseVisualStyleBackColor = true;
            Variables.checkBox3.CheckedChanged += new EventHandler(checkBox3_CheckedChanged);
            // 
            // checkBox2
            // 
            Variables.checkBox2.AutoSize = true;
            Variables.checkBox2.Checked = true;
            Variables.checkBox2.CheckState = CheckState.Checked;
            Variables.checkBox2.ForeColor = Color.FromArgb(224, 224, 224);
            Variables.checkBox2.Location = new Point(178, 376);
            Variables.checkBox2.Name = "checkBox2";
            Variables.checkBox2.Size = new Size(114, 17);
            Variables.checkBox2.TabIndex = 13;
            Variables.checkBox2.Text = "Start Menu Games";
            Variables.checkBox2.UseVisualStyleBackColor = true;
            Variables.checkBox2.CheckedChanged += new EventHandler(checkBox2_CheckedChanged);
            // 
            // checkBox1
            // 
            Variables.checkBox1.AutoSize = true;
            Variables.checkBox1.Checked = true;
            Variables.checkBox1.CheckState = CheckState.Checked;
            Variables.checkBox1.ForeColor = Color.FromArgb(224, 224, 224);
            Variables.checkBox1.Location = new Point(98, 376);
            Variables.checkBox1.Name = "checkBox1";
            Variables.checkBox1.Size = new Size(74, 17);
            Variables.checkBox1.TabIndex = 12;
            Variables.checkBox1.Text = "C:\\Games";
            Variables.checkBox1.UseVisualStyleBackColor = true;
            Variables.checkBox1.CheckedChanged += new EventHandler(checkBox1_CheckedChanged);
            // 
            // progressBar1
            // 
            Variables.progressBar1.Location = new Point(7, 354);
            Variables.progressBar1.MarqueeAnimationSpeed = 20;
            Variables.progressBar1.Name = "progressBar1";
            Variables.progressBar1.Size = new Size(701, 12);
            Variables.progressBar1.Style = ProgressBarStyle.Marquee;
            Variables.progressBar1.TabIndex = 11;
            // 
            // button3
            // 
            Variables.button3.Anchor = ((AnchorStyles)((AnchorStyles.Bottom | AnchorStyles.Left)));
            Variables.button3.BackColor = Color.FromArgb(64, 64, 64);
            Variables.button3.FlatStyle = FlatStyle.Popup;
            Variables.button3.ForeColor = Color.FromArgb(224, 224, 224);
            Variables.button3.Location = new Point(7, 371);
            Variables.button3.Name = "button3";
            Variables.button3.Size = new Size(87, 25);
            Variables.button3.TabIndex = 9;
            Variables.button3.Text = "Select All";
            Variables.button3.UseVisualStyleBackColor = false;
            Variables.button3.Click += new EventHandler(button3_Click);
            // 
            // button2
            // 
            Variables.button2.Anchor = ((AnchorStyles)((AnchorStyles.Bottom | AnchorStyles.Right)));
            Variables.button2.BackColor = Color.FromArgb(64, 64, 64);
            Variables.button2.FlatStyle = FlatStyle.Popup;
            Variables.button2.ForeColor = Color.FromArgb(224, 224, 224);
            Variables.button2.Location = new Point(621, 371);
            Variables.button2.Name = "button2";
            Variables.button2.Size = new Size(87, 25);
            Variables.button2.TabIndex = 7;
            Variables.button2.Text = "Add Games";
            Variables.button2.UseVisualStyleBackColor = false;
            Variables.button2.Click += new EventHandler(button2_Click);
            // 
            // label1
            // 
            Variables.label1.AutoSize = true;
            Variables.label1.BackColor = Color.FromArgb(45, 45, 45);
            Variables.label1.Font = new Font("Century Gothic", 11.25F);
            Variables.label1.ForeColor = Color.White;
            Variables.label1.Location = new Point(35, 7);
            Variables.label1.Name = "label1";
            Variables.label1.Size = new Size(96, 20);
            Variables.label1.TabIndex = 3;
            Variables.label1.Text = "Add Games";
            // 
            // panel2
            // 
            Variables.panel2.Anchor = ((AnchorStyles)(((AnchorStyles.Top | AnchorStyles.Left)
            | AnchorStyles.Right)));
            Variables.panel2.BackColor = Color.FromArgb(45, 45, 45);
            Variables.panel2.Controls.Add(Variables.label1);
            Variables.panel2.Controls.Add(Variables.button1);
            Variables.panel2.Controls.Add(Variables.pictureBox1);
            Variables.panel2.Location = new Point(2, 1);
            Variables.panel2.Name = "panel2";
            Variables.panel2.Size = new Size(713, 30);
            Variables.panel2.TabIndex = 6;
            // 
            // pictureBox1
            // 
            Variables.pictureBox1.BackColor = Color.FromArgb(45, 45, 45);
            Variables.pictureBox1.Image = global::AddGames.Properties.Resources.GClientGTTopBar;
            Variables.pictureBox1.Location = new Point(3, 3);
            Variables.pictureBox1.Name = "pictureBox1";
            Variables.pictureBox1.Size = new Size(27, 27);
            Variables.pictureBox1.SizeMode = PictureBoxSizeMode.Zoom;
            Variables.pictureBox1.TabIndex = 5;
            Variables.pictureBox1.TabStop = false;
            // 
            // listBox1
            // 
            Variables.listBox1.Anchor = ((AnchorStyles)((((AnchorStyles.Top | AnchorStyles.Bottom)
            | AnchorStyles.Left)
            | AnchorStyles.Right)));
            Variables.listBox1.BackColor = Color.FromArgb(244, 244, 244);
            Variables.listBox1.BorderStyle = BorderStyle.FixedSingle;
            Variables.listBox1.CheckOnClick = true;
            Variables.listBox1.Font = new Font("Microsoft Sans Serif", 9F, FontStyle.Regular, GraphicsUnit.Point, 0);
            Variables.listBox1.ForeColor = Color.Black;
            Variables.listBox1.HorizontalScrollbar = true;
            Variables.listBox1.Location = new Point(8, 37);
            Variables.listBox1.Name = "listBox1";
            Variables.listBox1.Size = new Size(701, 322);
            Variables.listBox1.Sorted = true;
            Variables.listBox1.TabIndex = 6;
            Variables.listBox1.ThreeDCheckBoxes = true;
            // 
            // richTextbox3
            // 
            Variables.richTextbox3.BackColor = Color.FromArgb(244, 244, 244);
            Variables.richTextbox3.BorderStyle = BorderStyle.None;
            Variables.richTextbox3.ForeColor = Color.Black;
            Variables.richTextbox3.Location = new Point(210, 171);
            Variables.richTextbox3.Name = "richTextbox3";
            Variables.richTextbox3.Size = new Size(297, 50);
            Variables.richTextbox3.TabIndex = 26;
            // 
            // panel3
            // 
            Variables.panel3.BackColor = Color.FromArgb(244, 244, 244);
            Variables.panel3.Location = new Point(8, 37);
            Variables.panel3.Name = "panel3";
            Variables.panel3.Size = new Size(701, 322);
            Variables.panel3.TabIndex = 25;
            // 
            // Form1
            // 
            this.AutoScaleBaseSize = new Size(5, 13);
            this.BackColor = Color.FromArgb(10, 100, 255);
            this.ClientSize = new Size(717, 409);
            this.ControlBox = false;
            this.Controls.Add(Variables.richTextbox3);
            this.Controls.Add(Variables.panel3);
            this.Controls.Add(Variables.listBox1);
            this.Controls.Add(Variables.panel2);
            this.Controls.Add(Variables.panel1);
            this.FormBorderStyle = FormBorderStyle.None;
            this.Icon = ((Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.StartPosition = FormStartPosition.CenterScreen;
            this.Text = "Add Games";
            this.Load += new EventHandler(Form1_Load);
            Variables.panel1.ResumeLayout(false);
            Variables.panel1.PerformLayout();
            Variables.panel2.ResumeLayout(false);
            Variables.panel2.PerformLayout();
            ((ISupportInitialize)(Variables.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }
        static void Save(Image image, int maxWidth, int maxHeight, int quality, string filePath)
        {
            int originalWidth = image.Width;
            int originalHeight = image.Height;
            float ratioX = maxWidth / (float)originalWidth;
            float ratioY = maxHeight / (float)originalHeight;
            float ratio = Math.Min(ratioX, ratioY);
            int newWidth = (int)(originalWidth * ratio);
            int newHeight = (int)(originalHeight * ratio);
            Variables.newImage = new Bitmap(newWidth, newHeight, PixelFormat.Format24bppRgb);
            using (Graphics graphics = Graphics.FromImage(Variables.newImage))
            {
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.DrawImage(image, 0, 0, newWidth, newHeight);
            }
            Variables.imageCodecInfo = GetEncoderInfo(ImageFormat.Jpeg);
            Variables.encoder = Encoder.Quality;
            Variables.encoderParameters = new EncoderParameters(1);
            Variables.encoderParameter = new EncoderParameter(Variables.encoder, quality);
            Variables.encoderParameters.Param[0] = Variables.encoderParameter;
            Variables.newImage.Save(filePath, Variables.imageCodecInfo, Variables.encoderParameters);
        }
        static ImageCodecInfo GetEncoderInfo(ImageFormat format)
        {
            return ImageCodecInfo.GetImageDecoders().SingleOrDefault(c => c.FormatID == format.Guid);
        }
        static int Files = 1;
        private void button1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        static bool Message = true;
        private void button2_Click(object sender, EventArgs e)
        {
            foreach (object file in Variables.listBox1.CheckedItems)
            {
                Files++;
            }
            foreach (object file in Variables.listBox1.CheckedItems)
            {
                Files--;
                if (Path.GetExtension(file.ToString()).Contains("url"))
                {
                    using (var fileOpen = File.OpenText(file.ToString()))
                    {
                        while (!fileOpen.EndOfStream)
                        {
                            string line = fileOpen.ReadLine();
                            if (line.StartsWith("URL"))
                            {
                                //Game Images
                                using (OpenFileDialog openFileDialog = new OpenFileDialog())
                                {
                                    openFileDialog.Title = file.ToString();
                                    openFileDialog.CheckFileExists = true;
                                    openFileDialog.ReadOnlyChecked = true;
                                    openFileDialog.InitialDirectory = Variables.previousPath;
                                    openFileDialog.Filter = Properties.Resources.ResourceManager.GetString("ImageExtension");
                                    openFileDialog.FilterIndex = 1;
                                    DialogResult result = openFileDialog.ShowDialog();
                                    if (result == DialogResult.OK)
                                    {
                                        Variables.previousPath = Path.GetDirectoryName(openFileDialog.FileName);
                                        Variables.bitmap = new Bitmap(openFileDialog.FileName);
                                        var newBitmap = Path.GetFileNameWithoutExtension(openFileDialog.SafeFileName);
                                        if (!File.Exists(Variables.ImagePath + newBitmap + ".jpg"))
                                        {
                                            Save(Variables.bitmap, 460, 215, 100, Variables.ImagePath + newBitmap + ".jpg");
                                        }
                                        XmlDocument xml2 = new XmlDocument();
                                        xml2.Load(Variables.XFile);
                                        XmlNodeList nodes2 = xml2.SelectNodes("//Games");
                                        foreach (XmlElement element2 in nodes2)
                                        {
                                            if (element2.SelectSingleNode("GameImage1").InnerText == "0" && element2.SelectSingleNode("Game1").InnerText == "0" && !Variables.ImageAdded)
                                            {
                                                Variables.ImageAdded = true;
                                                element2.SelectSingleNode("GameImage1").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                                xml2.Save(Variables.XFile);
                                            }
                                            if (element2.SelectSingleNode("GameImage2").InnerText == "0" && element2.SelectSingleNode("Game2").InnerText == "0" && !Variables.ImageAdded)
                                            {
                                                Variables.ImageAdded = true;
                                                element2.SelectSingleNode("GameImage2").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                                xml2.Save(Variables.XFile);
                                            }
                                            if (element2.SelectSingleNode("GameImage3").InnerText == "0" && element2.SelectSingleNode("Game3").InnerText == "0" && !Variables.ImageAdded)
                                            {
                                                Variables.ImageAdded = true;
                                                element2.SelectSingleNode("GameImage3").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                                xml2.Save(Variables.XFile);
                                            }
                                            if (element2.SelectSingleNode("GameImage4").InnerText == "0" && element2.SelectSingleNode("Game4").InnerText == "0" && !Variables.ImageAdded)
                                            {
                                                Variables.ImageAdded = true;
                                                element2.SelectSingleNode("GameImage4").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                                xml2.Save(Variables.XFile);
                                            }
                                            if (element2.SelectSingleNode("GameImage5").InnerText == "0" && element2.SelectSingleNode("Game5").InnerText == "0" && !Variables.ImageAdded)
                                            {
                                                Variables.ImageAdded = true;
                                                element2.SelectSingleNode("GameImage5").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                                xml2.Save(Variables.XFile);
                                            }
                                            if (element2.SelectSingleNode("GameImage6").InnerText == "0" && element2.SelectSingleNode("Game6").InnerText == "0" && !Variables.ImageAdded)
                                            {
                                                Variables.ImageAdded = true;
                                                element2.SelectSingleNode("GameImage6").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                                xml2.Save(Variables.XFile);
                                            }
                                            if (element2.SelectSingleNode("GameImage7").InnerText == "0" && element2.SelectSingleNode("Game7").InnerText == "0" && !Variables.ImageAdded)
                                            {
                                                Variables.ImageAdded = true;
                                                element2.SelectSingleNode("GameImage7").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                                xml2.Save(Variables.XFile);
                                            }
                                            if (element2.SelectSingleNode("GameImage8").InnerText == "0" && element2.SelectSingleNode("Game8").InnerText == "0" && !Variables.ImageAdded)
                                            {
                                                Variables.ImageAdded = true;
                                                element2.SelectSingleNode("GameImage8").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                                xml2.Save(Variables.XFile);
                                            }
                                            if (element2.SelectSingleNode("GameImage9").InnerText == "0" && element2.SelectSingleNode("Game9").InnerText == "0" && !Variables.ImageAdded)
                                            {
                                                Variables.ImageAdded = true;
                                                element2.SelectSingleNode("GameImage9").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                                xml2.Save(Variables.XFile);
                                            }
                                            if (element2.SelectSingleNode("GameImage10").InnerText == "0" && element2.SelectSingleNode("Game10").InnerText == "0" && !Variables.ImageAdded)
                                            {
                                                Variables.ImageAdded = true;
                                                element2.SelectSingleNode("GameImage10").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                                xml2.Save(Variables.XFile);
                                            }
                                            if (element2.SelectSingleNode("GameImage11").InnerText == "0" && element2.SelectSingleNode("Game11").InnerText == "0" && !Variables.ImageAdded)
                                            {
                                                Variables.ImageAdded = true;
                                                element2.SelectSingleNode("GameImage11").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                                xml2.Save(Variables.XFile);
                                            }
                                            if (element2.SelectSingleNode("GameImage12").InnerText == "0" && element2.SelectSingleNode("Game12").InnerText == "0" && !Variables.ImageAdded)
                                            {
                                                Variables.ImageAdded = true;
                                                element2.SelectSingleNode("GameImage12").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                                xml2.Save(Variables.XFile);
                                            }
                                            if (element2.SelectSingleNode("GameImage13").InnerText == "0" && element2.SelectSingleNode("Game13").InnerText == "0" && !Variables.ImageAdded)
                                            {
                                                Variables.ImageAdded = true;
                                                element2.SelectSingleNode("GameImage13").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                                xml2.Save(Variables.XFile);
                                            }
                                            if (element2.SelectSingleNode("GameImage14").InnerText == "0" && element2.SelectSingleNode("Game14").InnerText == "0" && !Variables.ImageAdded)
                                            {
                                                Variables.ImageAdded = true;
                                                element2.SelectSingleNode("GameImage14").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                                xml2.Save(Variables.XFile);
                                            }
                                            if (element2.SelectSingleNode("GameImage15").InnerText == "0" && element2.SelectSingleNode("Game15").InnerText == "0" && !Variables.ImageAdded)
                                            {
                                                Variables.ImageAdded = true;
                                                element2.SelectSingleNode("GameImage15").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                                xml2.Save(Variables.XFile);
                                            }
                                            if (element2.SelectSingleNode("GameImage16").InnerText == "0" && element2.SelectSingleNode("Game16").InnerText == "0" && !Variables.ImageAdded)
                                            {
                                                Variables.ImageAdded = true;
                                                element2.SelectSingleNode("GameImage16").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                                xml2.Save(Variables.XFile);
                                            }
                                            if (element2.SelectSingleNode("GameImage17").InnerText == "0" && element2.SelectSingleNode("Game17").InnerText == "0" && !Variables.ImageAdded)
                                            {
                                                Variables.ImageAdded = true;
                                                element2.SelectSingleNode("GameImage17").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                                xml2.Save(Variables.XFile);
                                            }
                                            if (element2.SelectSingleNode("GameImage18").InnerText == "0" && element2.SelectSingleNode("Game18").InnerText == "0" && !Variables.ImageAdded)
                                            {
                                                Variables.ImageAdded = true;
                                                element2.SelectSingleNode("GameImage18").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                                xml2.Save(Variables.XFile);
                                            }
                                            if (element2.SelectSingleNode("GameImage19").InnerText == "0" && element2.SelectSingleNode("Game19").InnerText == "0" && !Variables.ImageAdded)
                                            {
                                                Variables.ImageAdded = true;
                                                element2.SelectSingleNode("GameImage19").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                                xml2.Save(Variables.XFile);
                                            }
                                            if (element2.SelectSingleNode("GameImage20").InnerText == "0" && element2.SelectSingleNode("Game20").InnerText == "0" && !Variables.ImageAdded)
                                            {
                                                Variables.ImageAdded = true;
                                                element2.SelectSingleNode("GameImage20").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                                xml2.Save(Variables.XFile);
                                            }
                                            if (element2.SelectSingleNode("GameImage21").InnerText == "0" && element2.SelectSingleNode("Game21").InnerText == "0" && !Variables.ImageAdded)
                                            {
                                                Variables.ImageAdded = true;
                                                element2.SelectSingleNode("GameImage21").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                                xml2.Save(Variables.XFile);
                                            }
                                            if (element2.SelectSingleNode("GameImage22").InnerText == "0" && element2.SelectSingleNode("Game22").InnerText == "0" && !Variables.ImageAdded)
                                            {
                                                Variables.ImageAdded = true;
                                                element2.SelectSingleNode("GameImage22").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                                xml2.Save(Variables.XFile);
                                            }
                                            if (element2.SelectSingleNode("GameImage23").InnerText == "0" && element2.SelectSingleNode("Game23").InnerText == "0" && !Variables.ImageAdded)
                                            {
                                                Variables.ImageAdded = true;
                                                element2.SelectSingleNode("GameImage23").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                                xml2.Save(Variables.XFile);
                                            }
                                            if (element2.SelectSingleNode("GameImage24").InnerText == "0" && element2.SelectSingleNode("Game24").InnerText == "0" && !Variables.ImageAdded)
                                            {
                                                Variables.ImageAdded = true;
                                                element2.SelectSingleNode("GameImage24").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                                xml2.Save(Variables.XFile);
                                            }
                                            if (element2.SelectSingleNode("GameImage25").InnerText == "0" && element2.SelectSingleNode("Game25").InnerText == "0" && !Variables.ImageAdded)
                                            {
                                                Variables.ImageAdded = true;
                                                element2.SelectSingleNode("GameImage25").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                                xml2.Save(Variables.XFile);
                                            }
                                            if (element2.SelectSingleNode("GameImage26").InnerText == "0" && element2.SelectSingleNode("Game26").InnerText == "0" && !Variables.ImageAdded)
                                            {
                                                Variables.ImageAdded = true;
                                                element2.SelectSingleNode("GameImage26").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                                xml2.Save(Variables.XFile);
                                            }
                                            if (element2.SelectSingleNode("GameImage27").InnerText == "0" && element2.SelectSingleNode("Game27").InnerText == "0" && !Variables.ImageAdded)
                                            {
                                                Variables.ImageAdded = true;
                                                element2.SelectSingleNode("GameImage27").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                                xml2.Save(Variables.XFile);
                                            }
                                            if (element2.SelectSingleNode("GameImage28").InnerText == "0" && element2.SelectSingleNode("Game28").InnerText == "0" && !Variables.ImageAdded)
                                            {
                                                Variables.ImageAdded = true;
                                                element2.SelectSingleNode("GameImage28").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                                xml2.Save(Variables.XFile);
                                            }
                                            if (element2.SelectSingleNode("GameImage29").InnerText == "0" && element2.SelectSingleNode("Game29").InnerText == "0" && !Variables.ImageAdded)
                                            {
                                                Variables.ImageAdded = true;
                                                element2.SelectSingleNode("GameImage29").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                                xml2.Save(Variables.XFile);
                                            }
                                            if (element2.SelectSingleNode("GameImage30").InnerText == "0" && element2.SelectSingleNode("Game30").InnerText == "0" && !Variables.ImageAdded)
                                            {
                                                Variables.ImageAdded = true;
                                                element2.SelectSingleNode("GameImage30").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                                xml2.Save(Variables.XFile);
                                            }
                                            if (element2.SelectSingleNode("GameImage31").InnerText == "0" && element2.SelectSingleNode("Game31").InnerText == "0" && !Variables.ImageAdded)
                                            {
                                                Variables.ImageAdded = true;
                                                element2.SelectSingleNode("GameImage31").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                                xml2.Save(Variables.XFile);
                                            }
                                            if (element2.SelectSingleNode("GameImage32").InnerText == "0" && element2.SelectSingleNode("Game32").InnerText == "0" && !Variables.ImageAdded)
                                            {
                                                Variables.ImageAdded = true;
                                                element2.SelectSingleNode("GameImage32").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                                xml2.Save(Variables.XFile);
                                            }
                                            if (element2.SelectSingleNode("GameImage33").InnerText == "0" && element2.SelectSingleNode("Game33").InnerText == "0" && !Variables.ImageAdded)
                                            {
                                                Variables.ImageAdded = true;
                                                element2.SelectSingleNode("GameImage33").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                                xml2.Save(Variables.XFile);
                                            }
                                            if (element2.SelectSingleNode("GameImage34").InnerText == "0" && element2.SelectSingleNode("Game34").InnerText == "0" && !Variables.ImageAdded)
                                            {
                                                Variables.ImageAdded = true;
                                                element2.SelectSingleNode("GameImage34").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                                xml2.Save(Variables.XFile);
                                            }
                                            if (element2.SelectSingleNode("GameImage35").InnerText == "0" && element2.SelectSingleNode("Game35").InnerText == "0" && !Variables.ImageAdded)
                                            {
                                                Variables.ImageAdded = true;
                                                element2.SelectSingleNode("GameImage35").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                                xml2.Save(Variables.XFile);
                                            }
                                            if (element2.SelectSingleNode("GameImage36").InnerText == "0" && element2.SelectSingleNode("Game36").InnerText == "0" && !Variables.ImageAdded)
                                            {
                                                Variables.ImageAdded = true;
                                                element2.SelectSingleNode("GameImage36").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                                xml2.Save(Variables.XFile);
                                            }
                                            if (element2.SelectSingleNode("GameImage37").InnerText == "0" && element2.SelectSingleNode("Game37").InnerText == "0" && !Variables.ImageAdded)
                                            {
                                                Variables.ImageAdded = true;
                                                element2.SelectSingleNode("GameImage37").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                                xml2.Save(Variables.XFile);
                                            }
                                            if (element2.SelectSingleNode("GameImage38").InnerText == "0" && element2.SelectSingleNode("Game38").InnerText == "0" && !Variables.ImageAdded)
                                            {
                                                Variables.ImageAdded = true;
                                                element2.SelectSingleNode("GameImage38").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                                xml2.Save(Variables.XFile);
                                            }
                                            if (element2.SelectSingleNode("GameImage39").InnerText == "0" && element2.SelectSingleNode("Game39").InnerText == "0" && !Variables.ImageAdded)
                                            {
                                                Variables.ImageAdded = true;
                                                element2.SelectSingleNode("GameImage39").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                                xml2.Save(Variables.XFile);
                                            }
                                            if (element2.SelectSingleNode("GameImage40").InnerText == "0" && element2.SelectSingleNode("Game40").InnerText == "0" && !Variables.ImageAdded)
                                            {
                                                Variables.ImageAdded = true;
                                                element2.SelectSingleNode("GameImage40").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                                xml2.Save(Variables.XFile);
                                            }
                                            if (element2.SelectSingleNode("GameImage41").InnerText == "0" && element2.SelectSingleNode("Game41").InnerText == "0" && !Variables.ImageAdded)
                                            {
                                                Variables.ImageAdded = true;
                                                element2.SelectSingleNode("GameImage41").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                                xml2.Save(Variables.XFile);
                                            }
                                            if (element2.SelectSingleNode("GameImage42").InnerText == "0" && element2.SelectSingleNode("Game42").InnerText == "0" && !Variables.ImageAdded)
                                            {
                                                Variables.ImageAdded = true;
                                                element2.SelectSingleNode("GameImage42").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                                xml2.Save(Variables.XFile);
                                            }
                                            if (element2.SelectSingleNode("GameImage43").InnerText == "0" && element2.SelectSingleNode("Game43").InnerText == "0" && !Variables.ImageAdded)
                                            {
                                                Variables.ImageAdded = true;
                                                element2.SelectSingleNode("GameImage43").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                                xml2.Save(Variables.XFile);
                                            }
                                            if (element2.SelectSingleNode("GameImage44").InnerText == "0" && element2.SelectSingleNode("Game44").InnerText == "0" && !Variables.ImageAdded)
                                            {
                                                Variables.ImageAdded = true;
                                                element2.SelectSingleNode("GameImage44").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                                xml2.Save(Variables.XFile);
                                            }
                                            if (element2.SelectSingleNode("GameImage45").InnerText == "0" && element2.SelectSingleNode("Game45").InnerText == "0" && !Variables.ImageAdded)
                                            {
                                                Variables.ImageAdded = true;
                                                element2.SelectSingleNode("GameImage45").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                                xml2.Save(Variables.XFile);
                                            }
                                            if (element2.SelectSingleNode("GameImage46").InnerText == "0" && element2.SelectSingleNode("Game46").InnerText == "0" && !Variables.ImageAdded)
                                            {
                                                Variables.ImageAdded = true;
                                                element2.SelectSingleNode("GameImage46").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                                xml2.Save(Variables.XFile);
                                            }
                                            if (element2.SelectSingleNode("GameImage47").InnerText == "0" && element2.SelectSingleNode("Game47").InnerText == "0" && !Variables.ImageAdded)
                                            {
                                                Variables.ImageAdded = true;
                                                element2.SelectSingleNode("GameImage47").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                                xml2.Save(Variables.XFile);
                                            }
                                            if (element2.SelectSingleNode("GameImage48").InnerText == "0" && element2.SelectSingleNode("Game48").InnerText == "0" && !Variables.ImageAdded)
                                            {
                                                Variables.ImageAdded = true;
                                                element2.SelectSingleNode("GameImage48").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                                xml2.Save(Variables.XFile);
                                            }
                                            if (element2.SelectSingleNode("GameImage49").InnerText == "0" && element2.SelectSingleNode("Game49").InnerText == "0" && !Variables.ImageAdded)
                                            {
                                                Variables.ImageAdded = true;
                                                element2.SelectSingleNode("GameImage49").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                                xml2.Save(Variables.XFile);
                                            }
                                            if (element2.SelectSingleNode("GameImage50").InnerText == "0" && element2.SelectSingleNode("Game50").InnerText == "0" && !Variables.ImageAdded)
                                            {
                                                Variables.ImageAdded = true;
                                                element2.SelectSingleNode("GameImage50").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                                xml2.Save(Variables.XFile);
                                            }
                                            if (element2.SelectSingleNode("GameImage51").InnerText == "0" && element2.SelectSingleNode("Game51").InnerText == "0" && !Variables.ImageAdded)
                                            {
                                                Variables.ImageAdded = true;
                                                element2.SelectSingleNode("GameImage51").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                                xml2.Save(Variables.XFile);
                                            }
                                            if (element2.SelectSingleNode("GameImage52").InnerText == "0" && element2.SelectSingleNode("Game52").InnerText == "0" && !Variables.ImageAdded)
                                            {
                                                Variables.ImageAdded = true;
                                                element2.SelectSingleNode("GameImage52").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                                xml2.Save(Variables.XFile);
                                            }
                                            if (element2.SelectSingleNode("GameImage53").InnerText == "0" && element2.SelectSingleNode("Game53").InnerText == "0" && !Variables.ImageAdded)
                                            {
                                                Variables.ImageAdded = true;
                                                element2.SelectSingleNode("GameImage53").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                                xml2.Save(Variables.XFile);
                                            }
                                            if (element2.SelectSingleNode("GameImage54").InnerText == "0" && element2.SelectSingleNode("Game54").InnerText == "0" && !Variables.ImageAdded)
                                            {
                                                Variables.ImageAdded = true;
                                                element2.SelectSingleNode("GameImage54").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                                xml2.Save(Variables.XFile);
                                            }
                                            if (element2.SelectSingleNode("GameImage55").InnerText == "0" && element2.SelectSingleNode("Game55").InnerText == "0" && !Variables.ImageAdded)
                                            {
                                                Variables.ImageAdded = true;
                                                element2.SelectSingleNode("GameImage55").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                                xml2.Save(Variables.XFile);
                                            }
                                            if (element2.SelectSingleNode("GameImage56").InnerText == "0" && element2.SelectSingleNode("Game56").InnerText == "0" && !Variables.ImageAdded)
                                            {
                                                Variables.ImageAdded = true;
                                                element2.SelectSingleNode("GameImage56").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                                xml2.Save(Variables.XFile);
                                            }
                                            if (element2.SelectSingleNode("GameImage57").InnerText == "0" && element2.SelectSingleNode("Game57").InnerText == "0" && !Variables.ImageAdded)
                                            {
                                                Variables.ImageAdded = true;
                                                element2.SelectSingleNode("GameImage57").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                                xml2.Save(Variables.XFile);
                                            }
                                            if (element2.SelectSingleNode("GameImage58").InnerText == "0" && element2.SelectSingleNode("Game58").InnerText == "0" && !Variables.ImageAdded)
                                            {
                                                Variables.ImageAdded = true;
                                                element2.SelectSingleNode("GameImage58").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                                xml2.Save(Variables.XFile);
                                            }
                                            if (element2.SelectSingleNode("GameImage59").InnerText == "0" && element2.SelectSingleNode("Game59").InnerText == "0" && !Variables.ImageAdded)
                                            {
                                                Variables.ImageAdded = true;
                                                element2.SelectSingleNode("GameImage59").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                                xml2.Save(Variables.XFile);
                                            }
                                            if (element2.SelectSingleNode("GameImage60").InnerText == "0" && element2.SelectSingleNode("Game60").InnerText == "0" && !Variables.ImageAdded)
                                            {
                                                Variables.ImageAdded = true;
                                                element2.SelectSingleNode("GameImage60").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                                xml2.Save(Variables.XFile);
                                            }
                                        }
                                        string arg = Dialog.Show(Properties.Resources.ResourceManager.GetString("SteamCommands"));
                                        XmlDocument xml3 = new XmlDocument();
                                        xml3.Load(Variables.XFile);
                                        XmlNodeList nodes3 = xml3.SelectNodes("//Games");
                                        foreach (XmlElement element3 in nodes3)
                                        {
                                            if (!(string.IsNullOrEmpty(arg)))
                                            {
                                                if (element3.SelectSingleNode("G1Args").InnerText == "0" && element3.SelectSingleNode("Game1").InnerText == "0" && !Variables.ArgsAdded)
                                                {
                                                    Variables.ArgsAdded = true;
                                                    element3.SelectSingleNode("G1Args").InnerText = arg;
                                                    xml3.Save(Variables.XFile);
                                                }
                                                if (element3.SelectSingleNode("G2Args").InnerText == "0" && element3.SelectSingleNode("Game2").InnerText == "0" && !Variables.ArgsAdded)
                                                {
                                                    Variables.ArgsAdded = true;
                                                    element3.SelectSingleNode("G2Args").InnerText = arg;
                                                    xml3.Save(Variables.XFile);
                                                }
                                                if (element3.SelectSingleNode("G3Args").InnerText == "0" && element3.SelectSingleNode("Game3").InnerText == "0" && !Variables.ArgsAdded)
                                                {
                                                    Variables.ArgsAdded = true;
                                                    element3.SelectSingleNode("G3Args").InnerText = arg;
                                                    xml3.Save(Variables.XFile);
                                                }
                                                if (element3.SelectSingleNode("G4Args").InnerText == "0" && element3.SelectSingleNode("Game4").InnerText == "0" && !Variables.ArgsAdded)
                                                {
                                                    Variables.ArgsAdded = true;
                                                    element3.SelectSingleNode("G4Args").InnerText = arg;
                                                    xml3.Save(Variables.XFile);
                                                }
                                                if (element3.SelectSingleNode("G5Args").InnerText == "0" && element3.SelectSingleNode("Game5").InnerText == "0" && !Variables.ArgsAdded)
                                                {
                                                    Variables.ArgsAdded = true;
                                                    element3.SelectSingleNode("G5Args").InnerText = arg;
                                                    xml3.Save(Variables.XFile);
                                                }
                                                if (element3.SelectSingleNode("G6Args").InnerText == "0" && element3.SelectSingleNode("Game6").InnerText == "0" && !Variables.ArgsAdded)
                                                {
                                                    Variables.ArgsAdded = true;
                                                    element3.SelectSingleNode("G6Args").InnerText = arg;
                                                    xml3.Save(Variables.XFile);
                                                }
                                                if (element3.SelectSingleNode("G7Args").InnerText == "0" && element3.SelectSingleNode("Game7").InnerText == "0" && !Variables.ArgsAdded)
                                                {
                                                    Variables.ArgsAdded = true;
                                                    element3.SelectSingleNode("G7Args").InnerText = arg;
                                                    xml3.Save(Variables.XFile);
                                                }
                                                if (element3.SelectSingleNode("G8Args").InnerText == "0" && element3.SelectSingleNode("Game8").InnerText == "0" && !Variables.ArgsAdded)
                                                {
                                                    Variables.ArgsAdded = true;
                                                    element3.SelectSingleNode("G8Args").InnerText = arg;
                                                    xml3.Save(Variables.XFile);
                                                }
                                                if (element3.SelectSingleNode("G9Args").InnerText == "0" && element3.SelectSingleNode("Game9").InnerText == "0" && !Variables.ArgsAdded)
                                                {
                                                    Variables.ArgsAdded = true;
                                                    element3.SelectSingleNode("G9Args").InnerText = arg;
                                                    xml3.Save(Variables.XFile);
                                                }
                                                if (element3.SelectSingleNode("G10Args").InnerText == "0" && element3.SelectSingleNode("Game10").InnerText == "0" && !Variables.ArgsAdded)
                                                {
                                                    Variables.ArgsAdded = true;
                                                    element3.SelectSingleNode("G10Args").InnerText = arg;
                                                    xml3.Save(Variables.XFile);
                                                }
                                                if (element3.SelectSingleNode("G11Args").InnerText == "0" && element3.SelectSingleNode("Game11").InnerText == "0" && !Variables.ArgsAdded)
                                                {
                                                    Variables.ArgsAdded = true;
                                                    element3.SelectSingleNode("G11Args").InnerText = arg;
                                                    xml3.Save(Variables.XFile);
                                                }
                                                if (element3.SelectSingleNode("G12Args").InnerText == "0" && element3.SelectSingleNode("Game12").InnerText == "0" && !Variables.ArgsAdded)
                                                {
                                                    Variables.ArgsAdded = true;
                                                    element3.SelectSingleNode("G12Args").InnerText = arg;
                                                    xml3.Save(Variables.XFile);
                                                }
                                                if (element3.SelectSingleNode("G13Args").InnerText == "0" && element3.SelectSingleNode("Game13").InnerText == "0" && !Variables.ArgsAdded)
                                                {
                                                    Variables.ArgsAdded = true;
                                                    element3.SelectSingleNode("G13Args").InnerText = arg;
                                                    xml3.Save(Variables.XFile);
                                                }
                                                if (element3.SelectSingleNode("G14Args").InnerText == "0" && element3.SelectSingleNode("Game14").InnerText == "0" && !Variables.ArgsAdded)
                                                {
                                                    Variables.ArgsAdded = true;
                                                    element3.SelectSingleNode("G14Args").InnerText = arg;
                                                    xml3.Save(Variables.XFile);
                                                }
                                                if (element3.SelectSingleNode("G15Args").InnerText == "0" && element3.SelectSingleNode("Game15").InnerText == "0" && !Variables.ArgsAdded)
                                                {
                                                    Variables.ArgsAdded = true;
                                                    element3.SelectSingleNode("G15Args").InnerText = arg;
                                                    xml3.Save(Variables.XFile);
                                                }
                                                if (element3.SelectSingleNode("G16Args").InnerText == "0" && element3.SelectSingleNode("Game16").InnerText == "0" && !Variables.ArgsAdded)
                                                {
                                                    Variables.ArgsAdded = true;
                                                    element3.SelectSingleNode("G16Args").InnerText = arg;
                                                    xml3.Save(Variables.XFile);
                                                }
                                                if (element3.SelectSingleNode("G17Args").InnerText == "0" && element3.SelectSingleNode("Game17").InnerText == "0" && !Variables.ArgsAdded)
                                                {
                                                    Variables.ArgsAdded = true;
                                                    element3.SelectSingleNode("G17Args").InnerText = arg;
                                                    xml3.Save(Variables.XFile);
                                                }
                                                if (element3.SelectSingleNode("G18Args").InnerText == "0" && element3.SelectSingleNode("Game18").InnerText == "0" && !Variables.ArgsAdded)
                                                {
                                                    Variables.ArgsAdded = true;
                                                    element3.SelectSingleNode("G18Args").InnerText = arg;
                                                    xml3.Save(Variables.XFile);
                                                }
                                                if (element3.SelectSingleNode("G19Args").InnerText == "0" && element3.SelectSingleNode("Game19").InnerText == "0" && !Variables.ArgsAdded)
                                                {
                                                    Variables.ArgsAdded = true;
                                                    element3.SelectSingleNode("G19Args").InnerText = arg;
                                                    xml3.Save(Variables.XFile);
                                                }
                                                if (element3.SelectSingleNode("G20Args").InnerText == "0" && element3.SelectSingleNode("Game20").InnerText == "0" && !Variables.ArgsAdded)
                                                {
                                                    Variables.ArgsAdded = true;
                                                    element3.SelectSingleNode("G20Args").InnerText = arg;
                                                    xml3.Save(Variables.XFile);
                                                }
                                                if (element3.SelectSingleNode("G21Args").InnerText == "0" && element3.SelectSingleNode("Game21").InnerText == "0" && !Variables.ArgsAdded)
                                                {
                                                    Variables.ArgsAdded = true;
                                                    element3.SelectSingleNode("G21Args").InnerText = arg;
                                                    xml3.Save(Variables.XFile);
                                                }
                                                if (element3.SelectSingleNode("G22Args").InnerText == "0" && element3.SelectSingleNode("Game22").InnerText == "0" && !Variables.ArgsAdded)
                                                {
                                                    Variables.ArgsAdded = true;
                                                    element3.SelectSingleNode("G22Args").InnerText = arg;
                                                    xml3.Save(Variables.XFile);
                                                }
                                                if (element3.SelectSingleNode("G23Args").InnerText == "0" && element3.SelectSingleNode("Game23").InnerText == "0" && !Variables.ArgsAdded)
                                                {
                                                    Variables.ArgsAdded = true;
                                                    element3.SelectSingleNode("G23Args").InnerText = arg;
                                                    xml3.Save(Variables.XFile);
                                                }
                                                if (element3.SelectSingleNode("G24Args").InnerText == "0" && element3.SelectSingleNode("Game24").InnerText == "0" && !Variables.ArgsAdded)
                                                {
                                                    Variables.ArgsAdded = true;
                                                    element3.SelectSingleNode("G24Args").InnerText = arg;
                                                    xml3.Save(Variables.XFile);
                                                }
                                                if (element3.SelectSingleNode("G25Args").InnerText == "0" && element3.SelectSingleNode("Game25").InnerText == "0" && !Variables.ArgsAdded)
                                                {
                                                    Variables.ArgsAdded = true;
                                                    element3.SelectSingleNode("G25Args").InnerText = arg;
                                                    xml3.Save(Variables.XFile);
                                                }
                                                if (element3.SelectSingleNode("G26Args").InnerText == "0" && element3.SelectSingleNode("Game26").InnerText == "0" && !Variables.ArgsAdded)
                                                {
                                                    Variables.ArgsAdded = true;
                                                    element3.SelectSingleNode("G26Args").InnerText = arg;
                                                    xml3.Save(Variables.XFile);
                                                }
                                                if (element3.SelectSingleNode("G27Args").InnerText == "0" && element3.SelectSingleNode("Game27").InnerText == "0" && !Variables.ArgsAdded)
                                                {
                                                    Variables.ArgsAdded = true;
                                                    element3.SelectSingleNode("G27Args").InnerText = arg;
                                                    xml3.Save(Variables.XFile);
                                                }
                                                if (element3.SelectSingleNode("G28Args").InnerText == "0" && element3.SelectSingleNode("Game28").InnerText == "0" && !Variables.ArgsAdded)
                                                {
                                                    Variables.ArgsAdded = true;
                                                    element3.SelectSingleNode("G28Args").InnerText = arg;
                                                    xml3.Save(Variables.XFile);
                                                }
                                                if (element3.SelectSingleNode("G29Args").InnerText == "0" && element3.SelectSingleNode("Game29").InnerText == "0" && !Variables.ArgsAdded)
                                                {
                                                    Variables.ArgsAdded = true;
                                                    element3.SelectSingleNode("G29Args").InnerText = arg;
                                                    xml3.Save(Variables.XFile);
                                                }
                                                if (element3.SelectSingleNode("G30Args").InnerText == "0" && element3.SelectSingleNode("Game30").InnerText == "0" && !Variables.ArgsAdded)
                                                {
                                                    Variables.ArgsAdded = true;
                                                    element3.SelectSingleNode("G30Args").InnerText = arg;
                                                    xml3.Save(Variables.XFile);
                                                }
                                                if (element3.SelectSingleNode("G31Args").InnerText == "0" && element3.SelectSingleNode("Game31").InnerText == "0" && !Variables.ArgsAdded)
                                                {
                                                    Variables.ArgsAdded = true;
                                                    element3.SelectSingleNode("G31Args").InnerText = arg;
                                                    xml3.Save(Variables.XFile);
                                                }
                                                if (element3.SelectSingleNode("G32Args").InnerText == "0" && element3.SelectSingleNode("Game32").InnerText == "0" && !Variables.ArgsAdded)
                                                {
                                                    Variables.ArgsAdded = true;
                                                    element3.SelectSingleNode("G32Args").InnerText = arg;
                                                    xml3.Save(Variables.XFile);
                                                }
                                                if (element3.SelectSingleNode("G33Args").InnerText == "0" && element3.SelectSingleNode("Game33").InnerText == "0" && !Variables.ArgsAdded)
                                                {
                                                    Variables.ArgsAdded = true;
                                                    element3.SelectSingleNode("G33Args").InnerText = arg;
                                                    xml3.Save(Variables.XFile);
                                                }
                                                if (element3.SelectSingleNode("G34Args").InnerText == "0" && element3.SelectSingleNode("Game34").InnerText == "0" && !Variables.ArgsAdded)
                                                {
                                                    Variables.ArgsAdded = true;
                                                    element3.SelectSingleNode("G34Args").InnerText = arg;
                                                    xml3.Save(Variables.XFile);
                                                }
                                                if (element3.SelectSingleNode("G35Args").InnerText == "0" && element3.SelectSingleNode("Game35").InnerText == "0" && !Variables.ArgsAdded)
                                                {
                                                    Variables.ArgsAdded = true;
                                                    element3.SelectSingleNode("G35Args").InnerText = arg;
                                                    xml3.Save(Variables.XFile);
                                                }
                                                if (element3.SelectSingleNode("G36Args").InnerText == "0" && element3.SelectSingleNode("Game36").InnerText == "0" && !Variables.ArgsAdded)
                                                {
                                                    Variables.ArgsAdded = true;
                                                    element3.SelectSingleNode("G36Args").InnerText = arg;
                                                    xml3.Save(Variables.XFile);
                                                }
                                                if (element3.SelectSingleNode("G37Args").InnerText == "0" && element3.SelectSingleNode("Game37").InnerText == "0" && !Variables.ArgsAdded)
                                                {
                                                    Variables.ArgsAdded = true;
                                                    element3.SelectSingleNode("G37Args").InnerText = arg;
                                                    xml3.Save(Variables.XFile);
                                                }
                                                if (element3.SelectSingleNode("G38Args").InnerText == "0" && element3.SelectSingleNode("Game38").InnerText == "0" && !Variables.ArgsAdded)
                                                {
                                                    Variables.ArgsAdded = true;
                                                    element3.SelectSingleNode("G38Args").InnerText = arg;
                                                    xml3.Save(Variables.XFile);
                                                }
                                                if (element3.SelectSingleNode("G39Args").InnerText == "0" && element3.SelectSingleNode("Game39").InnerText == "0" && !Variables.ArgsAdded)
                                                {
                                                    Variables.ArgsAdded = true;
                                                    element3.SelectSingleNode("G39Args").InnerText = arg;
                                                    xml3.Save(Variables.XFile);
                                                }
                                                if (element3.SelectSingleNode("G40Args").InnerText == "0" && element3.SelectSingleNode("Game40").InnerText == "0" && !Variables.ArgsAdded)
                                                {
                                                    Variables.ArgsAdded = true;
                                                    element3.SelectSingleNode("G40Args").InnerText = arg;
                                                    xml3.Save(Variables.XFile);
                                                }
                                                if (element3.SelectSingleNode("G41Args").InnerText == "0" && element3.SelectSingleNode("Game41").InnerText == "0" && !Variables.ArgsAdded)
                                                {
                                                    Variables.ArgsAdded = true;
                                                    element3.SelectSingleNode("G41Args").InnerText = arg;
                                                    xml3.Save(Variables.XFile);
                                                }
                                                if (element3.SelectSingleNode("G42Args").InnerText == "0" && element3.SelectSingleNode("Game42").InnerText == "0" && !Variables.ArgsAdded)
                                                {
                                                    Variables.ArgsAdded = true;
                                                    element3.SelectSingleNode("G42Args").InnerText = arg;
                                                    xml3.Save(Variables.XFile);
                                                }
                                                if (element3.SelectSingleNode("G43Args").InnerText == "0" && element3.SelectSingleNode("Game43").InnerText == "0" && !Variables.ArgsAdded)
                                                {
                                                    Variables.ArgsAdded = true;
                                                    element3.SelectSingleNode("G43Args").InnerText = arg;
                                                    xml3.Save(Variables.XFile);
                                                }
                                                if (element3.SelectSingleNode("G44Args").InnerText == "0" && element3.SelectSingleNode("Game44").InnerText == "0" && !Variables.ArgsAdded)
                                                {
                                                    Variables.ArgsAdded = true;
                                                    element3.SelectSingleNode("G44Args").InnerText = arg;
                                                    xml3.Save(Variables.XFile);
                                                }
                                                if (element3.SelectSingleNode("G45Args").InnerText == "0" && element3.SelectSingleNode("Game45").InnerText == "0" && !Variables.ArgsAdded)
                                                {
                                                    Variables.ArgsAdded = true;
                                                    element3.SelectSingleNode("G45Args").InnerText = arg;
                                                    xml3.Save(Variables.XFile);
                                                }
                                                if (element3.SelectSingleNode("G46Args").InnerText == "0" && element3.SelectSingleNode("Game46").InnerText == "0" && !Variables.ArgsAdded)
                                                {
                                                    Variables.ArgsAdded = true;
                                                    element3.SelectSingleNode("G46Args").InnerText = arg;
                                                    xml3.Save(Variables.XFile);
                                                }
                                                if (element3.SelectSingleNode("G47Args").InnerText == "0" && element3.SelectSingleNode("Game47").InnerText == "0" && !Variables.ArgsAdded)
                                                {
                                                    Variables.ArgsAdded = true;
                                                    element3.SelectSingleNode("G47Args").InnerText = arg;
                                                    xml3.Save(Variables.XFile);
                                                }
                                                if (element3.SelectSingleNode("G48Args").InnerText == "0" && element3.SelectSingleNode("Game48").InnerText == "0" && !Variables.ArgsAdded)
                                                {
                                                    Variables.ArgsAdded = true;
                                                    element3.SelectSingleNode("G48Args").InnerText = arg;
                                                    xml3.Save(Variables.XFile);
                                                }
                                                if (element3.SelectSingleNode("G49Args").InnerText == "0" && element3.SelectSingleNode("Game49").InnerText == "0" && !Variables.ArgsAdded)
                                                {
                                                    Variables.ArgsAdded = true;
                                                    element3.SelectSingleNode("G49Args").InnerText = arg;
                                                    xml3.Save(Variables.XFile);
                                                }
                                                if (element3.SelectSingleNode("G50Args").InnerText == "0" && element3.SelectSingleNode("Game50").InnerText == "0" && !Variables.ArgsAdded)
                                                {
                                                    Variables.ArgsAdded = true;
                                                    element3.SelectSingleNode("G50Args").InnerText = arg;
                                                    xml3.Save(Variables.XFile);
                                                }
                                                if (element3.SelectSingleNode("G51Args").InnerText == "0" && element3.SelectSingleNode("Game51").InnerText == "0" && !Variables.ArgsAdded)
                                                {
                                                    Variables.ArgsAdded = true;
                                                    element3.SelectSingleNode("G51Args").InnerText = arg;
                                                    xml3.Save(Variables.XFile);
                                                }
                                                if (element3.SelectSingleNode("G52Args").InnerText == "0" && element3.SelectSingleNode("Game52").InnerText == "0" && !Variables.ArgsAdded)
                                                {
                                                    Variables.ArgsAdded = true;
                                                    element3.SelectSingleNode("G52Args").InnerText = arg;
                                                    xml3.Save(Variables.XFile);
                                                }
                                                if (element3.SelectSingleNode("G53Args").InnerText == "0" && element3.SelectSingleNode("Game53").InnerText == "0" && !Variables.ArgsAdded)
                                                {
                                                    Variables.ArgsAdded = true;
                                                    element3.SelectSingleNode("G53Args").InnerText = arg;
                                                    xml3.Save(Variables.XFile);
                                                }
                                                if (element3.SelectSingleNode("G54Args").InnerText == "0" && element3.SelectSingleNode("Game54").InnerText == "0" && !Variables.ArgsAdded)
                                                {
                                                    Variables.ArgsAdded = true;
                                                    element3.SelectSingleNode("G54Args").InnerText = arg;
                                                    xml3.Save(Variables.XFile);
                                                }
                                                if (element3.SelectSingleNode("G55Args").InnerText == "0" && element3.SelectSingleNode("Game55").InnerText == "0" && !Variables.ArgsAdded)
                                                {
                                                    Variables.ArgsAdded = true;
                                                    element3.SelectSingleNode("G55Args").InnerText = arg;
                                                    xml3.Save(Variables.XFile);
                                                }
                                                if (element3.SelectSingleNode("G56Args").InnerText == "0" && element3.SelectSingleNode("Game56").InnerText == "0" && !Variables.ArgsAdded)
                                                {
                                                    Variables.ArgsAdded = true;
                                                    element3.SelectSingleNode("G56Args").InnerText = arg;
                                                    xml3.Save(Variables.XFile);
                                                }
                                                if (element3.SelectSingleNode("G57Args").InnerText == "0" && element3.SelectSingleNode("Game57").InnerText == "0" && !Variables.ArgsAdded)
                                                {
                                                    Variables.ArgsAdded = true;
                                                    element3.SelectSingleNode("G57Args").InnerText = arg;
                                                    xml3.Save(Variables.XFile);
                                                }
                                                if (element3.SelectSingleNode("G58Args").InnerText == "0" && element3.SelectSingleNode("Game58").InnerText == "0" && !Variables.ArgsAdded)
                                                {
                                                    Variables.ArgsAdded = true;
                                                    element3.SelectSingleNode("G58Args").InnerText = arg;
                                                    xml3.Save(Variables.XFile);
                                                }
                                                if (element3.SelectSingleNode("G59Args").InnerText == "0" && element3.SelectSingleNode("Game59").InnerText == "0" && !Variables.ArgsAdded)
                                                {
                                                    Variables.ArgsAdded = true;
                                                    element3.SelectSingleNode("G59Args").InnerText = arg;
                                                    xml3.Save(Variables.XFile);
                                                }
                                                if (element3.SelectSingleNode("G60Args").InnerText == "0" && element3.SelectSingleNode("Game60").InnerText == "0" && !Variables.ArgsAdded)
                                                {
                                                    Variables.ArgsAdded = true;
                                                    element3.SelectSingleNode("G60Args").InnerText = arg;
                                                    xml3.Save(Variables.XFile);
                                                }
                                            }
                                        }
                                        // Add Games
                                        var newline = line.Replace("URL=steam://", "steam://");
                                        XmlDocument xml1 = new XmlDocument();
                                        xml1.Load(Variables.XFile);
                                        XmlNodeList nodes = xml1.SelectNodes("//Games");
                                        foreach (XmlElement element in nodes)
                                        {
                                            if (element.SelectSingleNode("Game1").InnerText == "0" && !Variables.Added)
                                            {
                                                Variables.Added = true;
                                                element.SelectSingleNode("Game1").InnerText = newline;
                                                xml1.Save(Variables.XFile);
                                            }
                                            if (element.SelectSingleNode("Game2").InnerText == "0" && !Variables.Added)
                                            {
                                                Variables.Added = true;
                                                element.SelectSingleNode("Game2").InnerText = newline;
                                                xml1.Save(Variables.XFile);
                                            }
                                            if (element.SelectSingleNode("Game3").InnerText == "0" && !Variables.Added)
                                            {
                                                Variables.Added = true;
                                                element.SelectSingleNode("Game3").InnerText = newline;
                                                xml1.Save(Variables.XFile);
                                            }
                                            if (element.SelectSingleNode("Game4").InnerText == "0" && !Variables.Added)
                                            {
                                                Variables.Added = true;
                                                element.SelectSingleNode("Game4").InnerText = newline;
                                                xml1.Save(Variables.XFile);
                                            }
                                            if (element.SelectSingleNode("Game5").InnerText == "0" && !Variables.Added)
                                            {
                                                Variables.Added = true;
                                                element.SelectSingleNode("Game5").InnerText = newline;
                                                xml1.Save(Variables.XFile);
                                            }
                                            if (element.SelectSingleNode("Game6").InnerText == "0" && !Variables.Added)
                                            {
                                                Variables.Added = true;
                                                element.SelectSingleNode("Game6").InnerText = newline;
                                                xml1.Save(Variables.XFile);
                                            }
                                            if (element.SelectSingleNode("Game7").InnerText == "0" && !Variables.Added)
                                            {
                                                Variables.Added = true;
                                                element.SelectSingleNode("Game7").InnerText = newline;
                                                xml1.Save(Variables.XFile);
                                            }
                                            if (element.SelectSingleNode("Game8").InnerText == "0" && !Variables.Added)
                                            {
                                                Variables.Added = true;
                                                element.SelectSingleNode("Game8").InnerText = newline;
                                                xml1.Save(Variables.XFile);
                                            }
                                            if (element.SelectSingleNode("Game9").InnerText == "0" && !Variables.Added)
                                            {
                                                Variables.Added = true;
                                                element.SelectSingleNode("Game9").InnerText = newline;
                                                xml1.Save(Variables.XFile);
                                            }
                                            if (element.SelectSingleNode("Game10").InnerText == "0" && !Variables.Added)
                                            {
                                                Variables.Added = true;
                                                element.SelectSingleNode("Game10").InnerText = newline;
                                                xml1.Save(Variables.XFile);
                                            }
                                            if (element.SelectSingleNode("Game11").InnerText == "0" && !Variables.Added)
                                            {
                                                Variables.Added = true;
                                                element.SelectSingleNode("Game11").InnerText = newline;
                                                xml1.Save(Variables.XFile);
                                            }
                                            if (element.SelectSingleNode("Game12").InnerText == "0" && !Variables.Added)
                                            {
                                                Variables.Added = true;
                                                element.SelectSingleNode("Game12").InnerText = newline;
                                                xml1.Save(Variables.XFile);
                                            }
                                            if (element.SelectSingleNode("Game13").InnerText == "0" && !Variables.Added)
                                            {
                                                Variables.Added = true;
                                                element.SelectSingleNode("Game13").InnerText = newline;
                                                xml1.Save(Variables.XFile);
                                            }
                                            if (element.SelectSingleNode("Game14").InnerText == "0" && !Variables.Added)
                                            {
                                                Variables.Added = true;
                                                element.SelectSingleNode("Game14").InnerText = newline;
                                                xml1.Save(Variables.XFile);
                                            }
                                            if (element.SelectSingleNode("Game15").InnerText == "0" && !Variables.Added)
                                            {
                                                Variables.Added = true;
                                                element.SelectSingleNode("Game15").InnerText = newline;
                                                xml1.Save(Variables.XFile);
                                            }
                                            if (element.SelectSingleNode("Game16").InnerText == "0" && !Variables.Added)
                                            {
                                                Variables.Added = true;
                                                element.SelectSingleNode("Game16").InnerText = newline;
                                                xml1.Save(Variables.XFile);
                                            }
                                            if (element.SelectSingleNode("Game17").InnerText == "0" && !Variables.Added)
                                            {
                                                Variables.Added = true;
                                                element.SelectSingleNode("Game17").InnerText = newline;
                                                xml1.Save(Variables.XFile);
                                            }
                                            if (element.SelectSingleNode("Game18").InnerText == "0" && !Variables.Added)
                                            {
                                                Variables.Added = true;
                                                element.SelectSingleNode("Game18").InnerText = newline;
                                                xml1.Save(Variables.XFile);
                                            }
                                            if (element.SelectSingleNode("Game19").InnerText == "0" && !Variables.Added)
                                            {
                                                Variables.Added = true;
                                                element.SelectSingleNode("Game19").InnerText = newline;
                                                xml1.Save(Variables.XFile);
                                            }
                                            if (element.SelectSingleNode("Game20").InnerText == "0" && !Variables.Added)
                                            {
                                                Variables.Added = true;
                                                element.SelectSingleNode("Game20").InnerText = newline;
                                                xml1.Save(Variables.XFile);
                                            }
                                            if (element.SelectSingleNode("Game21").InnerText == "0" && !Variables.Added)
                                            {
                                                Variables.Added = true;
                                                element.SelectSingleNode("Game21").InnerText = newline;
                                                xml1.Save(Variables.XFile);
                                            }
                                            if (element.SelectSingleNode("Game22").InnerText == "0" && !Variables.Added)
                                            {
                                                Variables.Added = true;
                                                element.SelectSingleNode("Game22").InnerText = newline;
                                                xml1.Save(Variables.XFile);
                                            }
                                            if (element.SelectSingleNode("Game23").InnerText == "0" && !Variables.Added)
                                            {
                                                Variables.Added = true;
                                                element.SelectSingleNode("Game23").InnerText = newline;
                                                xml1.Save(Variables.XFile);
                                            }
                                            if (element.SelectSingleNode("Game24").InnerText == "0" && !Variables.Added)
                                            {
                                                Variables.Added = true;
                                                element.SelectSingleNode("Game24").InnerText = newline;
                                                xml1.Save(Variables.XFile);
                                            }
                                            if (element.SelectSingleNode("Game25").InnerText == "0" && !Variables.Added)
                                            {
                                                Variables.Added = true;
                                                element.SelectSingleNode("Game25").InnerText = newline;
                                                xml1.Save(Variables.XFile);
                                            }
                                            if (element.SelectSingleNode("Game26").InnerText == "0" && !Variables.Added)
                                            {
                                                Variables.Added = true;
                                                element.SelectSingleNode("Game26").InnerText = newline;
                                                xml1.Save(Variables.XFile);
                                            }
                                            if (element.SelectSingleNode("Game27").InnerText == "0" && !Variables.Added)
                                            {
                                                Variables.Added = true;
                                                element.SelectSingleNode("Game27").InnerText = newline;
                                                xml1.Save(Variables.XFile);
                                            }
                                            if (element.SelectSingleNode("Game28").InnerText == "0" && !Variables.Added)
                                            {
                                                Variables.Added = true;
                                                element.SelectSingleNode("Game28").InnerText = newline;
                                                xml1.Save(Variables.XFile);
                                            }
                                            if (element.SelectSingleNode("Game29").InnerText == "0" && !Variables.Added)
                                            {
                                                Variables.Added = true;
                                                element.SelectSingleNode("Game29").InnerText = newline;
                                                xml1.Save(Variables.XFile);
                                            }
                                            if (element.SelectSingleNode("Game30").InnerText == "0" && !Variables.Added)
                                            {
                                                Variables.Added = true;
                                                element.SelectSingleNode("Game30").InnerText = newline;
                                                xml1.Save(Variables.XFile);
                                            }
                                            if (element.SelectSingleNode("Game31").InnerText == "0" && !Variables.Added)
                                            {
                                                Variables.Added = true;
                                                element.SelectSingleNode("Game31").InnerText = newline;
                                                xml1.Save(Variables.XFile);
                                            }
                                            if (element.SelectSingleNode("Game32").InnerText == "0" && !Variables.Added)
                                            {
                                                Variables.Added = true;
                                                element.SelectSingleNode("Game32").InnerText = newline;
                                                xml1.Save(Variables.XFile);
                                            }
                                            if (element.SelectSingleNode("Game33").InnerText == "0" && !Variables.Added)
                                            {
                                                Variables.Added = true;
                                                element.SelectSingleNode("Game33").InnerText = newline;
                                                xml1.Save(Variables.XFile);
                                            }
                                            if (element.SelectSingleNode("Game34").InnerText == "0" && !Variables.Added)
                                            {
                                                Variables.Added = true;
                                                element.SelectSingleNode("Game34").InnerText = newline;
                                                xml1.Save(Variables.XFile);
                                            }
                                            if (element.SelectSingleNode("Game35").InnerText == "0" && !Variables.Added)
                                            {
                                                Variables.Added = true;
                                                element.SelectSingleNode("Game35").InnerText = newline;
                                                xml1.Save(Variables.XFile);
                                            }
                                            if (element.SelectSingleNode("Game36").InnerText == "0" && !Variables.Added)
                                            {
                                                Variables.Added = true;
                                                element.SelectSingleNode("Game36").InnerText = newline;
                                                xml1.Save(Variables.XFile);
                                            }
                                            if (element.SelectSingleNode("Game37").InnerText == "0" && !Variables.Added)
                                            {
                                                Variables.Added = true;
                                                element.SelectSingleNode("Game37").InnerText = newline;
                                                xml1.Save(Variables.XFile);
                                            }
                                            if (element.SelectSingleNode("Game38").InnerText == "0" && !Variables.Added)
                                            {
                                                Variables.Added = true;
                                                element.SelectSingleNode("Game38").InnerText = newline;
                                                xml1.Save(Variables.XFile);
                                            }
                                            if (element.SelectSingleNode("Game39").InnerText == "0" && !Variables.Added)
                                            {
                                                Variables.Added = true;
                                                element.SelectSingleNode("Game39").InnerText = newline;
                                                xml1.Save(Variables.XFile);
                                            }
                                            if (element.SelectSingleNode("Game40").InnerText == "0" && !Variables.Added)
                                            {
                                                Variables.Added = true;
                                                element.SelectSingleNode("Game40").InnerText = newline;
                                                xml1.Save(Variables.XFile);
                                            }
                                            if (element.SelectSingleNode("Game41").InnerText == "0" && !Variables.Added)
                                            {
                                                Variables.Added = true;
                                                element.SelectSingleNode("Game41").InnerText = newline;
                                                xml1.Save(Variables.XFile);
                                            }
                                            if (element.SelectSingleNode("Game42").InnerText == "0" && !Variables.Added)
                                            {
                                                Variables.Added = true;
                                                element.SelectSingleNode("Game42").InnerText = newline;
                                                xml1.Save(Variables.XFile);
                                            }
                                            if (element.SelectSingleNode("Game43").InnerText == "0" && !Variables.Added)
                                            {
                                                Variables.Added = true;
                                                element.SelectSingleNode("Game43").InnerText = newline;
                                                xml1.Save(Variables.XFile);
                                            }
                                            if (element.SelectSingleNode("Game44").InnerText == "0" && !Variables.Added)
                                            {
                                                Variables.Added = true;
                                                element.SelectSingleNode("Game44").InnerText = newline;
                                                xml1.Save(Variables.XFile);
                                            }
                                            if (element.SelectSingleNode("Game45").InnerText == "0" && !Variables.Added)
                                            {
                                                Variables.Added = true;
                                                element.SelectSingleNode("Game45").InnerText = newline;
                                                xml1.Save(Variables.XFile);
                                            }
                                            if (element.SelectSingleNode("Game46").InnerText == "0" && !Variables.Added)
                                            {
                                                Variables.Added = true;
                                                element.SelectSingleNode("Game46").InnerText = newline;
                                                xml1.Save(Variables.XFile);
                                            }
                                            if (element.SelectSingleNode("Game47").InnerText == "0" && !Variables.Added)
                                            {
                                                Variables.Added = true;
                                                element.SelectSingleNode("Game47").InnerText = newline;
                                                xml1.Save(Variables.XFile);
                                            }
                                            if (element.SelectSingleNode("Game48").InnerText == "0" && !Variables.Added)
                                            {
                                                Variables.Added = true;
                                                element.SelectSingleNode("Game48").InnerText = newline;
                                                xml1.Save(Variables.XFile);
                                            }
                                            if (element.SelectSingleNode("Game49").InnerText == "0" && !Variables.Added)
                                            {
                                                Variables.Added = true;
                                                element.SelectSingleNode("Game49").InnerText = newline;
                                                xml1.Save(Variables.XFile);
                                            }
                                            if (element.SelectSingleNode("Game50").InnerText == "0" && !Variables.Added)
                                            {
                                                Variables.Added = true;
                                                element.SelectSingleNode("Game50").InnerText = newline;
                                                xml1.Save(Variables.XFile);
                                            }
                                            if (element.SelectSingleNode("Game51").InnerText == "0" && !Variables.Added)
                                            {
                                                Variables.Added = true;
                                                element.SelectSingleNode("Game51").InnerText = newline;
                                                xml1.Save(Variables.XFile);
                                            }
                                            if (element.SelectSingleNode("Game52").InnerText == "0" && !Variables.Added)
                                            {
                                                Variables.Added = true;
                                                element.SelectSingleNode("Game52").InnerText = newline;
                                                xml1.Save(Variables.XFile);
                                            }
                                            if (element.SelectSingleNode("Game53").InnerText == "0" && !Variables.Added)
                                            {
                                                Variables.Added = true;
                                                element.SelectSingleNode("Game53").InnerText = newline;
                                                xml1.Save(Variables.XFile);
                                            }
                                            if (element.SelectSingleNode("Game54").InnerText == "0" && !Variables.Added)
                                            {
                                                Variables.Added = true;
                                                element.SelectSingleNode("Game54").InnerText = newline;
                                                xml1.Save(Variables.XFile);
                                            }
                                            if (element.SelectSingleNode("Game55").InnerText == "0" && !Variables.Added)
                                            {
                                                Variables.Added = true;
                                                element.SelectSingleNode("Game55").InnerText = newline;
                                                xml1.Save(Variables.XFile);
                                            }
                                            if (element.SelectSingleNode("Game56").InnerText == "0" && !Variables.Added)
                                            {
                                                Variables.Added = true;
                                                element.SelectSingleNode("Game56").InnerText = newline;
                                                xml1.Save(Variables.XFile);
                                            }
                                            if (element.SelectSingleNode("Game57").InnerText == "0" && !Variables.Added)
                                            {
                                                Variables.Added = true;
                                                element.SelectSingleNode("Game57").InnerText = newline;
                                                xml1.Save(Variables.XFile);
                                            }
                                            if (element.SelectSingleNode("Game58").InnerText == "0" && !Variables.Added)
                                            {
                                                Variables.Added = true;
                                                element.SelectSingleNode("Game58").InnerText = newline;
                                                xml1.Save(Variables.XFile);
                                            }
                                            if (element.SelectSingleNode("Game59").InnerText == "0" && !Variables.Added)
                                            {
                                                Variables.Added = true;
                                                element.SelectSingleNode("Game59").InnerText = newline;
                                                xml1.Save(Variables.XFile);
                                            }
                                            if (element.SelectSingleNode("Game60").InnerText == "0" && !Variables.Added)
                                            {
                                                Variables.Added = true;
                                                element.SelectSingleNode("Game60").InnerText = newline;
                                                xml1.Save(Variables.XFile);
                                            }
                                        }
                                    }
                                }
                                if (Variables.Added != true && Variables.ImageAdded != true)
                                {
                                    if (Message)
                                    {
                                        Message = false;
                                        DialogNoBox.Show(Properties.Resources.ResourceManager.GetString("ActionCancelled"));
                                    }
                                    Variables.Added = false;
                                    Variables.ImageAdded = false;
                                    Variables.ArgsAdded = false;
                                }
                                if (Files == 1)
                                {
                                    Application.Exit();
                                }
                            }
                        }
                        Variables.Added = false;
                        Variables.ImageAdded = false;
                        Variables.ArgsAdded = false;
                    }
                }
                if (Path.GetExtension(file.ToString()).Contains("lnk") || Path.GetExtension(file.ToString()).Contains("exe"))
                {
                    var NewFile = ShortcutUtils.ResolveShortcut(file.ToString());
                    OpenFileDialog openFileDialog2 = new OpenFileDialog();
                    if (Path.GetExtension(file.ToString()).Contains("lnk"))
                    {
                        openFileDialog2.Title = NewFile;
                    }
                    if (Path.GetExtension(file.ToString()).Contains("exe"))
                    {
                        openFileDialog2.Title = file.ToString();
                    }
                    openFileDialog2.CheckFileExists = true;
                    openFileDialog2.ReadOnlyChecked = true;
                    openFileDialog2.InitialDirectory = Variables.previousPath;
                    openFileDialog2.Filter = "Image files (*.png;*.jpeg;*.jpg;*.bmp)|*.png;*.jpeg;*.jpg;*.bmp|jpeg files (*.jpeg)|*.jpeg|jpg files (*.jpg)|*.jpg|bmp files (*.bmp)|*.bmp|png files (*.png)|*.png|All files (*.*)|*.*";
                    openFileDialog2.FilterIndex = 1;
                    DialogResult result2 = openFileDialog2.ShowDialog();
                    if (result2 == DialogResult.OK)
                    {
                        Variables.previousPath = Path.GetDirectoryName(openFileDialog2.FileName);
                        Variables.bitmap = new Bitmap(openFileDialog2.FileName);
                        var newBitmap = Path.GetFileNameWithoutExtension(openFileDialog2.SafeFileName);
                        if (!File.Exists(Variables.ImagePath + newBitmap + ".jpg"))
                        {
                            Save(Variables.bitmap, 460, 215, 100, Variables.ImagePath + newBitmap + ".jpg");
                        }
                        XmlDocument xml2 = new XmlDocument();
                        xml2.Load(Variables.XFile);
                        XmlNodeList nodes2 = xml2.SelectNodes("//Games");
                        foreach (XmlElement element2 in nodes2)
                        {
                            if (element2.SelectSingleNode("GameImage1").InnerText == "0" && element2.SelectSingleNode("Game1").InnerText == "0" && !Variables.ImageAdded)
                            {
                                Variables.ImageAdded = true;
                                element2.SelectSingleNode("GameImage1").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                xml2.Save(Variables.XFile);
                            }
                            if (element2.SelectSingleNode("GameImage2").InnerText == "0" && element2.SelectSingleNode("Game2").InnerText == "0" && !Variables.ImageAdded)
                            {
                                Variables.ImageAdded = true;
                                element2.SelectSingleNode("GameImage2").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                xml2.Save(Variables.XFile);
                            }
                            if (element2.SelectSingleNode("GameImage3").InnerText == "0" && element2.SelectSingleNode("Game3").InnerText == "0" && !Variables.ImageAdded)
                            {
                                Variables.ImageAdded = true;
                                element2.SelectSingleNode("GameImage3").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                xml2.Save(Variables.XFile);
                            }
                            if (element2.SelectSingleNode("GameImage4").InnerText == "0" && element2.SelectSingleNode("Game4").InnerText == "0" && !Variables.ImageAdded)
                            {
                                Variables.ImageAdded = true;
                                element2.SelectSingleNode("GameImage4").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                xml2.Save(Variables.XFile);
                            }
                            if (element2.SelectSingleNode("GameImage5").InnerText == "0" && element2.SelectSingleNode("Game5").InnerText == "0" && !Variables.ImageAdded)
                            {
                                Variables.ImageAdded = true;
                                element2.SelectSingleNode("GameImage5").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                xml2.Save(Variables.XFile);
                            }
                            if (element2.SelectSingleNode("GameImage6").InnerText == "0" && element2.SelectSingleNode("Game6").InnerText == "0" && !Variables.ImageAdded)
                            {
                                Variables.ImageAdded = true;
                                element2.SelectSingleNode("GameImage6").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                xml2.Save(Variables.XFile);
                            }
                            if (element2.SelectSingleNode("GameImage7").InnerText == "0" && element2.SelectSingleNode("Game7").InnerText == "0" && !Variables.ImageAdded)
                            {
                                Variables.ImageAdded = true;
                                element2.SelectSingleNode("GameImage7").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                xml2.Save(Variables.XFile);
                            }
                            if (element2.SelectSingleNode("GameImage8").InnerText == "0" && element2.SelectSingleNode("Game8").InnerText == "0" && !Variables.ImageAdded)
                            {
                                Variables.ImageAdded = true;
                                element2.SelectSingleNode("GameImage8").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                xml2.Save(Variables.XFile);
                            }
                            if (element2.SelectSingleNode("GameImage9").InnerText == "0" && element2.SelectSingleNode("Game9").InnerText == "0" && !Variables.ImageAdded)
                            {
                                Variables.ImageAdded = true;
                                element2.SelectSingleNode("GameImage9").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                xml2.Save(Variables.XFile);
                            }
                            if (element2.SelectSingleNode("GameImage10").InnerText == "0" && element2.SelectSingleNode("Game10").InnerText == "0" && !Variables.ImageAdded)
                            {
                                Variables.ImageAdded = true;
                                element2.SelectSingleNode("GameImage10").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                xml2.Save(Variables.XFile);
                            }
                            if (element2.SelectSingleNode("GameImage11").InnerText == "0" && element2.SelectSingleNode("Game11").InnerText == "0" && !Variables.ImageAdded)
                            {
                                Variables.ImageAdded = true;
                                element2.SelectSingleNode("GameImage11").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                xml2.Save(Variables.XFile);
                            }
                            if (element2.SelectSingleNode("GameImage12").InnerText == "0" && element2.SelectSingleNode("Game12").InnerText == "0" && !Variables.ImageAdded)
                            {
                                Variables.ImageAdded = true;
                                element2.SelectSingleNode("GameImage12").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                xml2.Save(Variables.XFile);
                            }
                            if (element2.SelectSingleNode("GameImage13").InnerText == "0" && element2.SelectSingleNode("Game13").InnerText == "0" && !Variables.ImageAdded)
                            {
                                Variables.ImageAdded = true;
                                element2.SelectSingleNode("GameImage13").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                xml2.Save(Variables.XFile);
                            }
                            if (element2.SelectSingleNode("GameImage14").InnerText == "0" && element2.SelectSingleNode("Game14").InnerText == "0" && !Variables.ImageAdded)
                            {
                                Variables.ImageAdded = true;
                                element2.SelectSingleNode("GameImage14").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                xml2.Save(Variables.XFile);
                            }
                            if (element2.SelectSingleNode("GameImage15").InnerText == "0" && element2.SelectSingleNode("Game15").InnerText == "0" && !Variables.ImageAdded)
                            {
                                Variables.ImageAdded = true;
                                element2.SelectSingleNode("GameImage15").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                xml2.Save(Variables.XFile);
                            }
                            if (element2.SelectSingleNode("GameImage16").InnerText == "0" && element2.SelectSingleNode("Game16").InnerText == "0" && !Variables.ImageAdded)
                            {
                                Variables.ImageAdded = true;
                                element2.SelectSingleNode("GameImage16").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                xml2.Save(Variables.XFile);
                            }
                            if (element2.SelectSingleNode("GameImage17").InnerText == "0" && element2.SelectSingleNode("Game17").InnerText == "0" && !Variables.ImageAdded)
                            {
                                Variables.ImageAdded = true;
                                element2.SelectSingleNode("GameImage17").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                xml2.Save(Variables.XFile);
                            }
                            if (element2.SelectSingleNode("GameImage18").InnerText == "0" && element2.SelectSingleNode("Game18").InnerText == "0" && !Variables.ImageAdded)
                            {
                                Variables.ImageAdded = true;
                                element2.SelectSingleNode("GameImage18").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                xml2.Save(Variables.XFile);
                            }
                            if (element2.SelectSingleNode("GameImage19").InnerText == "0" && element2.SelectSingleNode("Game19").InnerText == "0" && !Variables.ImageAdded)
                            {
                                Variables.ImageAdded = true;
                                element2.SelectSingleNode("GameImage19").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                xml2.Save(Variables.XFile);
                            }
                            if (element2.SelectSingleNode("GameImage20").InnerText == "0" && element2.SelectSingleNode("Game20").InnerText == "0" && !Variables.ImageAdded)
                            {
                                Variables.ImageAdded = true;
                                element2.SelectSingleNode("GameImage20").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                xml2.Save(Variables.XFile);
                            }
                            if (element2.SelectSingleNode("GameImage21").InnerText == "0" && element2.SelectSingleNode("Game21").InnerText == "0" && !Variables.ImageAdded)
                            {
                                Variables.ImageAdded = true;
                                element2.SelectSingleNode("GameImage21").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                xml2.Save(Variables.XFile);
                            }
                            if (element2.SelectSingleNode("GameImage22").InnerText == "0" && element2.SelectSingleNode("Game22").InnerText == "0" && !Variables.ImageAdded)
                            {
                                Variables.ImageAdded = true;
                                element2.SelectSingleNode("GameImage22").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                xml2.Save(Variables.XFile);
                            }
                            if (element2.SelectSingleNode("GameImage23").InnerText == "0" && element2.SelectSingleNode("Game23").InnerText == "0" && !Variables.ImageAdded)
                            {
                                Variables.ImageAdded = true;
                                element2.SelectSingleNode("GameImage23").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                xml2.Save(Variables.XFile);
                            }
                            if (element2.SelectSingleNode("GameImage24").InnerText == "0" && element2.SelectSingleNode("Game24").InnerText == "0" && !Variables.ImageAdded)
                            {
                                Variables.ImageAdded = true;
                                element2.SelectSingleNode("GameImage24").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                xml2.Save(Variables.XFile);
                            }
                            if (element2.SelectSingleNode("GameImage25").InnerText == "0" && element2.SelectSingleNode("Game25").InnerText == "0" && !Variables.ImageAdded)
                            {
                                Variables.ImageAdded = true;
                                element2.SelectSingleNode("GameImage25").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                xml2.Save(Variables.XFile);
                            }
                            if (element2.SelectSingleNode("GameImage26").InnerText == "0" && element2.SelectSingleNode("Game26").InnerText == "0" && !Variables.ImageAdded)
                            {
                                Variables.ImageAdded = true;
                                element2.SelectSingleNode("GameImage26").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                xml2.Save(Variables.XFile);
                            }
                            if (element2.SelectSingleNode("GameImage27").InnerText == "0" && element2.SelectSingleNode("Game27").InnerText == "0" && !Variables.ImageAdded)
                            {
                                Variables.ImageAdded = true;
                                element2.SelectSingleNode("GameImage27").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                xml2.Save(Variables.XFile);
                            }
                            if (element2.SelectSingleNode("GameImage28").InnerText == "0" && element2.SelectSingleNode("Game28").InnerText == "0" && !Variables.ImageAdded)
                            {
                                Variables.ImageAdded = true;
                                element2.SelectSingleNode("GameImage28").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                xml2.Save(Variables.XFile);
                            }
                            if (element2.SelectSingleNode("GameImage29").InnerText == "0" && element2.SelectSingleNode("Game29").InnerText == "0" && !Variables.ImageAdded)
                            {
                                Variables.ImageAdded = true;
                                element2.SelectSingleNode("GameImage29").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                xml2.Save(Variables.XFile);
                            }
                            if (element2.SelectSingleNode("GameImage30").InnerText == "0" && element2.SelectSingleNode("Game30").InnerText == "0" && !Variables.ImageAdded)
                            {
                                Variables.ImageAdded = true;
                                element2.SelectSingleNode("GameImage30").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                xml2.Save(Variables.XFile);
                            }
                            if (element2.SelectSingleNode("GameImage31").InnerText == "0" && element2.SelectSingleNode("Game31").InnerText == "0" && !Variables.ImageAdded)
                            {
                                Variables.ImageAdded = true;
                                element2.SelectSingleNode("GameImage31").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                xml2.Save(Variables.XFile);
                            }
                            if (element2.SelectSingleNode("GameImage32").InnerText == "0" && element2.SelectSingleNode("Game32").InnerText == "0" && !Variables.ImageAdded)
                            {
                                Variables.ImageAdded = true;
                                element2.SelectSingleNode("GameImage32").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                xml2.Save(Variables.XFile);
                            }
                            if (element2.SelectSingleNode("GameImage33").InnerText == "0" && element2.SelectSingleNode("Game33").InnerText == "0" && !Variables.ImageAdded)
                            {
                                Variables.ImageAdded = true;
                                element2.SelectSingleNode("GameImage33").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                xml2.Save(Variables.XFile);
                            }
                            if (element2.SelectSingleNode("GameImage34").InnerText == "0" && element2.SelectSingleNode("Game34").InnerText == "0" && !Variables.ImageAdded)
                            {
                                Variables.ImageAdded = true;
                                element2.SelectSingleNode("GameImage34").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                xml2.Save(Variables.XFile);
                            }
                            if (element2.SelectSingleNode("GameImage35").InnerText == "0" && element2.SelectSingleNode("Game35").InnerText == "0" && !Variables.ImageAdded)
                            {
                                Variables.ImageAdded = true;
                                element2.SelectSingleNode("GameImage35").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                xml2.Save(Variables.XFile);
                            }
                            if (element2.SelectSingleNode("GameImage36").InnerText == "0" && element2.SelectSingleNode("Game36").InnerText == "0" && !Variables.ImageAdded)
                            {
                                Variables.ImageAdded = true;
                                element2.SelectSingleNode("GameImage36").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                xml2.Save(Variables.XFile);
                            }
                            if (element2.SelectSingleNode("GameImage37").InnerText == "0" && element2.SelectSingleNode("Game37").InnerText == "0" && !Variables.ImageAdded)
                            {
                                Variables.ImageAdded = true;
                                element2.SelectSingleNode("GameImage37").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                xml2.Save(Variables.XFile);
                            }
                            if (element2.SelectSingleNode("GameImage38").InnerText == "0" && element2.SelectSingleNode("Game38").InnerText == "0" && !Variables.ImageAdded)
                            {
                                Variables.ImageAdded = true;
                                element2.SelectSingleNode("GameImage38").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                xml2.Save(Variables.XFile);
                            }
                            if (element2.SelectSingleNode("GameImage39").InnerText == "0" && element2.SelectSingleNode("Game39").InnerText == "0" && !Variables.ImageAdded)
                            {
                                Variables.ImageAdded = true;
                                element2.SelectSingleNode("GameImage39").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                xml2.Save(Variables.XFile);
                            }
                            if (element2.SelectSingleNode("GameImage40").InnerText == "0" && element2.SelectSingleNode("Game40").InnerText == "0" && !Variables.ImageAdded)
                            {
                                Variables.ImageAdded = true;
                                element2.SelectSingleNode("GameImage40").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                xml2.Save(Variables.XFile);
                            }
                            if (element2.SelectSingleNode("GameImage41").InnerText == "0" && element2.SelectSingleNode("Game41").InnerText == "0" && !Variables.ImageAdded)
                            {
                                Variables.ImageAdded = true;
                                element2.SelectSingleNode("GameImage41").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                xml2.Save(Variables.XFile);
                            }
                            if (element2.SelectSingleNode("GameImage42").InnerText == "0" && element2.SelectSingleNode("Game42").InnerText == "0" && !Variables.ImageAdded)
                            {
                                Variables.ImageAdded = true;
                                element2.SelectSingleNode("GameImage42").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                xml2.Save(Variables.XFile);
                            }
                            if (element2.SelectSingleNode("GameImage43").InnerText == "0" && element2.SelectSingleNode("Game43").InnerText == "0" && !Variables.ImageAdded)
                            {
                                Variables.ImageAdded = true;
                                element2.SelectSingleNode("GameImage43").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                xml2.Save(Variables.XFile);
                            }
                            if (element2.SelectSingleNode("GameImage44").InnerText == "0" && element2.SelectSingleNode("Game44").InnerText == "0" && !Variables.ImageAdded)
                            {
                                Variables.ImageAdded = true;
                                element2.SelectSingleNode("GameImage44").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                xml2.Save(Variables.XFile);
                            }
                            if (element2.SelectSingleNode("GameImage45").InnerText == "0" && element2.SelectSingleNode("Game45").InnerText == "0" && !Variables.ImageAdded)
                            {
                                Variables.ImageAdded = true;
                                element2.SelectSingleNode("GameImage45").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                xml2.Save(Variables.XFile);
                            }
                            if (element2.SelectSingleNode("GameImage46").InnerText == "0" && element2.SelectSingleNode("Game46").InnerText == "0" && !Variables.ImageAdded)
                            {
                                Variables.ImageAdded = true;
                                element2.SelectSingleNode("GameImage46").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                xml2.Save(Variables.XFile);
                            }
                            if (element2.SelectSingleNode("GameImage47").InnerText == "0" && element2.SelectSingleNode("Game47").InnerText == "0" && !Variables.ImageAdded)
                            {
                                Variables.ImageAdded = true;
                                element2.SelectSingleNode("GameImage47").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                xml2.Save(Variables.XFile);
                            }
                            if (element2.SelectSingleNode("GameImage48").InnerText == "0" && element2.SelectSingleNode("Game48").InnerText == "0" && !Variables.ImageAdded)
                            {
                                Variables.ImageAdded = true;
                                element2.SelectSingleNode("GameImage48").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                xml2.Save(Variables.XFile);
                            }
                            if (element2.SelectSingleNode("GameImage49").InnerText == "0" && element2.SelectSingleNode("Game49").InnerText == "0" && !Variables.ImageAdded)
                            {
                                Variables.ImageAdded = true;
                                element2.SelectSingleNode("GameImage49").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                xml2.Save(Variables.XFile);
                            }
                            if (element2.SelectSingleNode("GameImage50").InnerText == "0" && element2.SelectSingleNode("Game50").InnerText == "0" && !Variables.ImageAdded)
                            {
                                Variables.ImageAdded = true;
                                element2.SelectSingleNode("GameImage50").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                xml2.Save(Variables.XFile);
                            }
                            if (element2.SelectSingleNode("GameImage51").InnerText == "0" && element2.SelectSingleNode("Game51").InnerText == "0" && !Variables.ImageAdded)
                            {
                                Variables.ImageAdded = true;
                                element2.SelectSingleNode("GameImage51").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                xml2.Save(Variables.XFile);
                            }
                            if (element2.SelectSingleNode("GameImage52").InnerText == "0" && element2.SelectSingleNode("Game52").InnerText == "0" && !Variables.ImageAdded)
                            {
                                Variables.ImageAdded = true;
                                element2.SelectSingleNode("GameImage52").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                xml2.Save(Variables.XFile);
                            }
                            if (element2.SelectSingleNode("GameImage53").InnerText == "0" && element2.SelectSingleNode("Game53").InnerText == "0" && !Variables.ImageAdded)
                            {
                                Variables.ImageAdded = true;
                                element2.SelectSingleNode("GameImage53").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                xml2.Save(Variables.XFile);
                            }
                            if (element2.SelectSingleNode("GameImage54").InnerText == "0" && element2.SelectSingleNode("Game54").InnerText == "0" && !Variables.ImageAdded)
                            {
                                Variables.ImageAdded = true;
                                element2.SelectSingleNode("GameImage54").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                xml2.Save(Variables.XFile);
                            }
                            if (element2.SelectSingleNode("GameImage55").InnerText == "0" && element2.SelectSingleNode("Game55").InnerText == "0" && !Variables.ImageAdded)
                            {
                                Variables.ImageAdded = true;
                                element2.SelectSingleNode("GameImage55").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                xml2.Save(Variables.XFile);
                            }
                            if (element2.SelectSingleNode("GameImage56").InnerText == "0" && element2.SelectSingleNode("Game56").InnerText == "0" && !Variables.ImageAdded)
                            {
                                Variables.ImageAdded = true;
                                element2.SelectSingleNode("GameImage56").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                xml2.Save(Variables.XFile);
                            }
                            if (element2.SelectSingleNode("GameImage57").InnerText == "0" && element2.SelectSingleNode("Game57").InnerText == "0" && !Variables.ImageAdded)
                            {
                                Variables.ImageAdded = true;
                                element2.SelectSingleNode("GameImage57").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                xml2.Save(Variables.XFile);
                            }
                            if (element2.SelectSingleNode("GameImage58").InnerText == "0" && element2.SelectSingleNode("Game58").InnerText == "0" && !Variables.ImageAdded)
                            {
                                Variables.ImageAdded = true;
                                element2.SelectSingleNode("GameImage58").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                xml2.Save(Variables.XFile);
                            }
                            if (element2.SelectSingleNode("GameImage59").InnerText == "0" && element2.SelectSingleNode("Game59").InnerText == "0" && !Variables.ImageAdded)
                            {
                                Variables.ImageAdded = true;
                                element2.SelectSingleNode("GameImage59").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                xml2.Save(Variables.XFile);
                            }
                            if (element2.SelectSingleNode("GameImage60").InnerText == "0" && element2.SelectSingleNode("Game60").InnerText == "0" && !Variables.ImageAdded)
                            {
                                Variables.ImageAdded = true;
                                element2.SelectSingleNode("GameImage60").InnerText = Variables.ImagePath + newBitmap + ".jpg";
                                xml2.Save(Variables.XFile);
                            }
                        }
                        string arg = Dialog.Show(Properties.Resources.ResourceManager.GetString("Arguments"));
                        XmlDocument xml3 = new XmlDocument();
                        xml3.Load(Variables.XFile);
                        XmlNodeList nodes3 = xml3.SelectNodes("//Games");
                        foreach (XmlElement element3 in nodes3)
                        {
                            if (!(string.IsNullOrEmpty(arg)))
                            {
                                if (element3.SelectSingleNode("G1Args").InnerText == "0" && element3.SelectSingleNode("Game1").InnerText == "0" && !Variables.ArgsAdded)
                                {
                                    Variables.ArgsAdded = true;
                                    element3.SelectSingleNode("G1Args").InnerText = arg;
                                    xml3.Save(Variables.XFile);
                                }
                                if (element3.SelectSingleNode("G2Args").InnerText == "0" && element3.SelectSingleNode("Game2").InnerText == "0" && !Variables.ArgsAdded)
                                {
                                    Variables.ArgsAdded = true;
                                    element3.SelectSingleNode("G2Args").InnerText = arg;
                                    xml3.Save(Variables.XFile);
                                }
                                if (element3.SelectSingleNode("G3Args").InnerText == "0" && element3.SelectSingleNode("Game3").InnerText == "0" && !Variables.ArgsAdded)
                                {
                                    Variables.ArgsAdded = true;
                                    element3.SelectSingleNode("G3Args").InnerText = arg;
                                    xml3.Save(Variables.XFile);
                                }
                                if (element3.SelectSingleNode("G4Args").InnerText == "0" && element3.SelectSingleNode("Game4").InnerText == "0" && !Variables.ArgsAdded)
                                {
                                    Variables.ArgsAdded = true;
                                    element3.SelectSingleNode("G4Args").InnerText = arg;
                                    xml3.Save(Variables.XFile);
                                }
                                if (element3.SelectSingleNode("G5Args").InnerText == "0" && element3.SelectSingleNode("Game5").InnerText == "0" && !Variables.ArgsAdded)
                                {
                                    Variables.ArgsAdded = true;
                                    element3.SelectSingleNode("G5Args").InnerText = arg;
                                    xml3.Save(Variables.XFile);
                                }
                                if (element3.SelectSingleNode("G6Args").InnerText == "0" && element3.SelectSingleNode("Game6").InnerText == "0" && !Variables.ArgsAdded)
                                {
                                    Variables.ArgsAdded = true;
                                    element3.SelectSingleNode("G6Args").InnerText = arg;
                                    xml3.Save(Variables.XFile);
                                }
                                if (element3.SelectSingleNode("G7Args").InnerText == "0" && element3.SelectSingleNode("Game7").InnerText == "0" && !Variables.ArgsAdded)
                                {
                                    Variables.ArgsAdded = true;
                                    element3.SelectSingleNode("G7Args").InnerText = arg;
                                    xml3.Save(Variables.XFile);
                                }
                                if (element3.SelectSingleNode("G8Args").InnerText == "0" && element3.SelectSingleNode("Game8").InnerText == "0" && !Variables.ArgsAdded)
                                {
                                    Variables.ArgsAdded = true;
                                    element3.SelectSingleNode("G8Args").InnerText = arg;
                                    xml3.Save(Variables.XFile);
                                }
                                if (element3.SelectSingleNode("G9Args").InnerText == "0" && element3.SelectSingleNode("Game9").InnerText == "0" && !Variables.ArgsAdded)
                                {
                                    Variables.ArgsAdded = true;
                                    element3.SelectSingleNode("G9Args").InnerText = arg;
                                    xml3.Save(Variables.XFile);
                                }
                                if (element3.SelectSingleNode("G10Args").InnerText == "0" && element3.SelectSingleNode("Game10").InnerText == "0" && !Variables.ArgsAdded)
                                {
                                    Variables.ArgsAdded = true;
                                    element3.SelectSingleNode("G10Args").InnerText = arg;
                                    xml3.Save(Variables.XFile);
                                }
                                if (element3.SelectSingleNode("G11Args").InnerText == "0" && element3.SelectSingleNode("Game11").InnerText == "0" && !Variables.ArgsAdded)
                                {
                                    Variables.ArgsAdded = true;
                                    element3.SelectSingleNode("G11Args").InnerText = arg;
                                    xml3.Save(Variables.XFile);
                                }
                                if (element3.SelectSingleNode("G12Args").InnerText == "0" && element3.SelectSingleNode("Game12").InnerText == "0" && !Variables.ArgsAdded)
                                {
                                    Variables.ArgsAdded = true;
                                    element3.SelectSingleNode("G12Args").InnerText = arg;
                                    xml3.Save(Variables.XFile);
                                }
                                if (element3.SelectSingleNode("G13Args").InnerText == "0" && element3.SelectSingleNode("Game13").InnerText == "0" && !Variables.ArgsAdded)
                                {
                                    Variables.ArgsAdded = true;
                                    element3.SelectSingleNode("G13Args").InnerText = arg;
                                    xml3.Save(Variables.XFile);
                                }
                                if (element3.SelectSingleNode("G14Args").InnerText == "0" && element3.SelectSingleNode("Game14").InnerText == "0" && !Variables.ArgsAdded)
                                {
                                    Variables.ArgsAdded = true;
                                    element3.SelectSingleNode("G14Args").InnerText = arg;
                                    xml3.Save(Variables.XFile);
                                }
                                if (element3.SelectSingleNode("G15Args").InnerText == "0" && element3.SelectSingleNode("Game15").InnerText == "0" && !Variables.ArgsAdded)
                                {
                                    Variables.ArgsAdded = true;
                                    element3.SelectSingleNode("G15Args").InnerText = arg;
                                    xml3.Save(Variables.XFile);
                                }
                                if (element3.SelectSingleNode("G16Args").InnerText == "0" && element3.SelectSingleNode("Game16").InnerText == "0" && !Variables.ArgsAdded)
                                {
                                    Variables.ArgsAdded = true;
                                    element3.SelectSingleNode("G16Args").InnerText = arg;
                                    xml3.Save(Variables.XFile);
                                }
                                if (element3.SelectSingleNode("G17Args").InnerText == "0" && element3.SelectSingleNode("Game17").InnerText == "0" && !Variables.ArgsAdded)
                                {
                                    Variables.ArgsAdded = true;
                                    element3.SelectSingleNode("G17Args").InnerText = arg;
                                    xml3.Save(Variables.XFile);
                                }
                                if (element3.SelectSingleNode("G18Args").InnerText == "0" && element3.SelectSingleNode("Game18").InnerText == "0" && !Variables.ArgsAdded)
                                {
                                    Variables.ArgsAdded = true;
                                    element3.SelectSingleNode("G18Args").InnerText = arg;
                                    xml3.Save(Variables.XFile);
                                }
                                if (element3.SelectSingleNode("G19Args").InnerText == "0" && element3.SelectSingleNode("Game19").InnerText == "0" && !Variables.ArgsAdded)
                                {
                                    Variables.ArgsAdded = true;
                                    element3.SelectSingleNode("G19Args").InnerText = arg;
                                    xml3.Save(Variables.XFile);
                                }
                                if (element3.SelectSingleNode("G20Args").InnerText == "0" && element3.SelectSingleNode("Game20").InnerText == "0" && !Variables.ArgsAdded)
                                {
                                    Variables.ArgsAdded = true;
                                    element3.SelectSingleNode("G20Args").InnerText = arg;
                                    xml3.Save(Variables.XFile);
                                }
                                if (element3.SelectSingleNode("G21Args").InnerText == "0" && element3.SelectSingleNode("Game21").InnerText == "0" && !Variables.ArgsAdded)
                                {
                                    Variables.ArgsAdded = true;
                                    element3.SelectSingleNode("G21Args").InnerText = arg;
                                    xml3.Save(Variables.XFile);
                                }
                                if (element3.SelectSingleNode("G22Args").InnerText == "0" && element3.SelectSingleNode("Game22").InnerText == "0" && !Variables.ArgsAdded)
                                {
                                    Variables.ArgsAdded = true;
                                    element3.SelectSingleNode("G22Args").InnerText = arg;
                                    xml3.Save(Variables.XFile);
                                }
                                if (element3.SelectSingleNode("G23Args").InnerText == "0" && element3.SelectSingleNode("Game23").InnerText == "0" && !Variables.ArgsAdded)
                                {
                                    Variables.ArgsAdded = true;
                                    element3.SelectSingleNode("G23Args").InnerText = arg;
                                    xml3.Save(Variables.XFile);
                                }
                                if (element3.SelectSingleNode("G24Args").InnerText == "0" && element3.SelectSingleNode("Game24").InnerText == "0" && !Variables.ArgsAdded)
                                {
                                    Variables.ArgsAdded = true;
                                    element3.SelectSingleNode("G24Args").InnerText = arg;
                                    xml3.Save(Variables.XFile);
                                }
                                if (element3.SelectSingleNode("G25Args").InnerText == "0" && element3.SelectSingleNode("Game25").InnerText == "0" && !Variables.ArgsAdded)
                                {
                                    Variables.ArgsAdded = true;
                                    element3.SelectSingleNode("G25Args").InnerText = arg;
                                    xml3.Save(Variables.XFile);
                                }
                                if (element3.SelectSingleNode("G26Args").InnerText == "0" && element3.SelectSingleNode("Game26").InnerText == "0" && !Variables.ArgsAdded)
                                {
                                    Variables.ArgsAdded = true;
                                    element3.SelectSingleNode("G26Args").InnerText = arg;
                                    xml3.Save(Variables.XFile);
                                }
                                if (element3.SelectSingleNode("G27Args").InnerText == "0" && element3.SelectSingleNode("Game27").InnerText == "0" && !Variables.ArgsAdded)
                                {
                                    Variables.ArgsAdded = true;
                                    element3.SelectSingleNode("G27Args").InnerText = arg;
                                    xml3.Save(Variables.XFile);
                                }
                                if (element3.SelectSingleNode("G28Args").InnerText == "0" && element3.SelectSingleNode("Game28").InnerText == "0" && !Variables.ArgsAdded)
                                {
                                    Variables.ArgsAdded = true;
                                    element3.SelectSingleNode("G28Args").InnerText = arg;
                                    xml3.Save(Variables.XFile);
                                }
                                if (element3.SelectSingleNode("G29Args").InnerText == "0" && element3.SelectSingleNode("Game29").InnerText == "0" && !Variables.ArgsAdded)
                                {
                                    Variables.ArgsAdded = true;
                                    element3.SelectSingleNode("G29Args").InnerText = arg;
                                    xml3.Save(Variables.XFile);
                                }
                                if (element3.SelectSingleNode("G30Args").InnerText == "0" && element3.SelectSingleNode("Game30").InnerText == "0" && !Variables.ArgsAdded)
                                {
                                    Variables.ArgsAdded = true;
                                    element3.SelectSingleNode("G30Args").InnerText = arg;
                                    xml3.Save(Variables.XFile);
                                }
                                if (element3.SelectSingleNode("G31Args").InnerText == "0" && element3.SelectSingleNode("Game31").InnerText == "0" && !Variables.ArgsAdded)
                                {
                                    Variables.ArgsAdded = true;
                                    element3.SelectSingleNode("G31Args").InnerText = arg;
                                    xml3.Save(Variables.XFile);
                                }
                                if (element3.SelectSingleNode("G32Args").InnerText == "0" && element3.SelectSingleNode("Game32").InnerText == "0" && !Variables.ArgsAdded)
                                {
                                    Variables.ArgsAdded = true;
                                    element3.SelectSingleNode("G32Args").InnerText = arg;
                                    xml3.Save(Variables.XFile);
                                }
                                if (element3.SelectSingleNode("G33Args").InnerText == "0" && element3.SelectSingleNode("Game33").InnerText == "0" && !Variables.ArgsAdded)
                                {
                                    Variables.ArgsAdded = true;
                                    element3.SelectSingleNode("G33Args").InnerText = arg;
                                    xml3.Save(Variables.XFile);
                                }
                                if (element3.SelectSingleNode("G34Args").InnerText == "0" && element3.SelectSingleNode("Game34").InnerText == "0" && !Variables.ArgsAdded)
                                {
                                    Variables.ArgsAdded = true;
                                    element3.SelectSingleNode("G34Args").InnerText = arg;
                                    xml3.Save(Variables.XFile);
                                }
                                if (element3.SelectSingleNode("G35Args").InnerText == "0" && element3.SelectSingleNode("Game35").InnerText == "0" && !Variables.ArgsAdded)
                                {
                                    Variables.ArgsAdded = true;
                                    element3.SelectSingleNode("G35Args").InnerText = arg;
                                    xml3.Save(Variables.XFile);
                                }
                                if (element3.SelectSingleNode("G36Args").InnerText == "0" && element3.SelectSingleNode("Game36").InnerText == "0" && !Variables.ArgsAdded)
                                {
                                    Variables.ArgsAdded = true;
                                    element3.SelectSingleNode("G36Args").InnerText = arg;
                                    xml3.Save(Variables.XFile);
                                }
                                if (element3.SelectSingleNode("G37Args").InnerText == "0" && element3.SelectSingleNode("Game37").InnerText == "0" && !Variables.ArgsAdded)
                                {
                                    Variables.ArgsAdded = true;
                                    element3.SelectSingleNode("G37Args").InnerText = arg;
                                    xml3.Save(Variables.XFile);
                                }
                                if (element3.SelectSingleNode("G38Args").InnerText == "0" && element3.SelectSingleNode("Game38").InnerText == "0" && !Variables.ArgsAdded)
                                {
                                    Variables.ArgsAdded = true;
                                    element3.SelectSingleNode("G38Args").InnerText = arg;
                                    xml3.Save(Variables.XFile);
                                }
                                if (element3.SelectSingleNode("G39Args").InnerText == "0" && element3.SelectSingleNode("Game39").InnerText == "0" && !Variables.ArgsAdded)
                                {
                                    Variables.ArgsAdded = true;
                                    element3.SelectSingleNode("G39Args").InnerText = arg;
                                    xml3.Save(Variables.XFile);
                                }
                                if (element3.SelectSingleNode("G40Args").InnerText == "0" && element3.SelectSingleNode("Game40").InnerText == "0" && !Variables.ArgsAdded)
                                {
                                    Variables.ArgsAdded = true;
                                    element3.SelectSingleNode("G40Args").InnerText = arg;
                                    xml3.Save(Variables.XFile);
                                }
                                if (element3.SelectSingleNode("G41Args").InnerText == "0" && element3.SelectSingleNode("Game41").InnerText == "0" && !Variables.ArgsAdded)
                                {
                                    Variables.ArgsAdded = true;
                                    element3.SelectSingleNode("G41Args").InnerText = arg;
                                    xml3.Save(Variables.XFile);
                                }
                                if (element3.SelectSingleNode("G42Args").InnerText == "0" && element3.SelectSingleNode("Game42").InnerText == "0" && !Variables.ArgsAdded)
                                {
                                    Variables.ArgsAdded = true;
                                    element3.SelectSingleNode("G42Args").InnerText = arg;
                                    xml3.Save(Variables.XFile);
                                }
                                if (element3.SelectSingleNode("G43Args").InnerText == "0" && element3.SelectSingleNode("Game43").InnerText == "0" && !Variables.ArgsAdded)
                                {
                                    Variables.ArgsAdded = true;
                                    element3.SelectSingleNode("G43Args").InnerText = arg;
                                    xml3.Save(Variables.XFile);
                                }
                                if (element3.SelectSingleNode("G44Args").InnerText == "0" && element3.SelectSingleNode("Game44").InnerText == "0" && !Variables.ArgsAdded)
                                {
                                    Variables.ArgsAdded = true;
                                    element3.SelectSingleNode("G44Args").InnerText = arg;
                                    xml3.Save(Variables.XFile);
                                }
                                if (element3.SelectSingleNode("G45Args").InnerText == "0" && element3.SelectSingleNode("Game45").InnerText == "0" && !Variables.ArgsAdded)
                                {
                                    Variables.ArgsAdded = true;
                                    element3.SelectSingleNode("G45Args").InnerText = arg;
                                    xml3.Save(Variables.XFile);
                                }
                                if (element3.SelectSingleNode("G46Args").InnerText == "0" && element3.SelectSingleNode("Game46").InnerText == "0" && !Variables.ArgsAdded)
                                {
                                    Variables.ArgsAdded = true;
                                    element3.SelectSingleNode("G46Args").InnerText = arg;
                                    xml3.Save(Variables.XFile);
                                }
                                if (element3.SelectSingleNode("G47Args").InnerText == "0" && element3.SelectSingleNode("Game47").InnerText == "0" && !Variables.ArgsAdded)
                                {
                                    Variables.ArgsAdded = true;
                                    element3.SelectSingleNode("G47Args").InnerText = arg;
                                    xml3.Save(Variables.XFile);
                                }
                                if (element3.SelectSingleNode("G48Args").InnerText == "0" && element3.SelectSingleNode("Game48").InnerText == "0" && !Variables.ArgsAdded)
                                {
                                    Variables.ArgsAdded = true;
                                    element3.SelectSingleNode("G48Args").InnerText = arg;
                                    xml3.Save(Variables.XFile);
                                }
                                if (element3.SelectSingleNode("G49Args").InnerText == "0" && element3.SelectSingleNode("Game49").InnerText == "0" && !Variables.ArgsAdded)
                                {
                                    Variables.ArgsAdded = true;
                                    element3.SelectSingleNode("G49Args").InnerText = arg;
                                    xml3.Save(Variables.XFile);
                                }
                                if (element3.SelectSingleNode("G50Args").InnerText == "0" && element3.SelectSingleNode("Game50").InnerText == "0" && !Variables.ArgsAdded)
                                {
                                    Variables.ArgsAdded = true;
                                    element3.SelectSingleNode("G50Args").InnerText = arg;
                                    xml3.Save(Variables.XFile);
                                }
                                if (element3.SelectSingleNode("G51Args").InnerText == "0" && element3.SelectSingleNode("Game51").InnerText == "0" && !Variables.ArgsAdded)
                                {
                                    Variables.ArgsAdded = true;
                                    element3.SelectSingleNode("G51Args").InnerText = arg;
                                    xml3.Save(Variables.XFile);
                                }
                                if (element3.SelectSingleNode("G52Args").InnerText == "0" && element3.SelectSingleNode("Game52").InnerText == "0" && !Variables.ArgsAdded)
                                {
                                    Variables.ArgsAdded = true;
                                    element3.SelectSingleNode("G52Args").InnerText = arg;
                                    xml3.Save(Variables.XFile);
                                }
                                if (element3.SelectSingleNode("G53Args").InnerText == "0" && element3.SelectSingleNode("Game53").InnerText == "0" && !Variables.ArgsAdded)
                                {
                                    Variables.ArgsAdded = true;
                                    element3.SelectSingleNode("G53Args").InnerText = arg;
                                    xml3.Save(Variables.XFile);
                                }
                                if (element3.SelectSingleNode("G54Args").InnerText == "0" && element3.SelectSingleNode("Game54").InnerText == "0" && !Variables.ArgsAdded)
                                {
                                    Variables.ArgsAdded = true;
                                    element3.SelectSingleNode("G54Args").InnerText = arg;
                                    xml3.Save(Variables.XFile);
                                }
                                if (element3.SelectSingleNode("G55Args").InnerText == "0" && element3.SelectSingleNode("Game55").InnerText == "0" && !Variables.ArgsAdded)
                                {
                                    Variables.ArgsAdded = true;
                                    element3.SelectSingleNode("G55Args").InnerText = arg;
                                    xml3.Save(Variables.XFile);
                                }
                                if (element3.SelectSingleNode("G56Args").InnerText == "0" && element3.SelectSingleNode("Game56").InnerText == "0" && !Variables.ArgsAdded)
                                {
                                    Variables.ArgsAdded = true;
                                    element3.SelectSingleNode("G56Args").InnerText = arg;
                                    xml3.Save(Variables.XFile);
                                }
                                if (element3.SelectSingleNode("G57Args").InnerText == "0" && element3.SelectSingleNode("Game57").InnerText == "0" && !Variables.ArgsAdded)
                                {
                                    Variables.ArgsAdded = true;
                                    element3.SelectSingleNode("G57Args").InnerText = arg;
                                    xml3.Save(Variables.XFile);
                                }
                                if (element3.SelectSingleNode("G58Args").InnerText == "0" && element3.SelectSingleNode("Game58").InnerText == "0" && !Variables.ArgsAdded)
                                {
                                    Variables.ArgsAdded = true;
                                    element3.SelectSingleNode("G58Args").InnerText = arg;
                                    xml3.Save(Variables.XFile);
                                }
                                if (element3.SelectSingleNode("G59Args").InnerText == "0" && element3.SelectSingleNode("Game59").InnerText == "0" && !Variables.ArgsAdded)
                                {
                                    Variables.ArgsAdded = true;
                                    element3.SelectSingleNode("G59Args").InnerText = arg;
                                    xml3.Save(Variables.XFile);
                                }
                                if (element3.SelectSingleNode("G60Args").InnerText == "0" && element3.SelectSingleNode("Game60").InnerText == "0" && !Variables.ArgsAdded)
                                {
                                    Variables.ArgsAdded = true;
                                    element3.SelectSingleNode("G60Args").InnerText = arg;
                                    xml3.Save(Variables.XFile);
                                }
                            }
                        }
                        // Add Games
                        if (Path.GetExtension(file.ToString()).Contains("exe"))
                        {
                            XmlDocument xml1 = new XmlDocument();
                            xml1.Load(Variables.XFile);
                            XmlNodeList nodes = xml1.SelectNodes("//Games");
                            foreach (XmlElement element in nodes)
                            {
                                if (element.SelectSingleNode("Game1").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game1").InnerText = file.ToString();
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game2").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game2").InnerText = file.ToString();
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game3").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game3").InnerText = file.ToString();
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game4").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game4").InnerText = file.ToString();
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game5").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game5").InnerText = file.ToString();
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game6").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game6").InnerText = file.ToString();
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game7").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game7").InnerText = file.ToString();
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game8").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game8").InnerText = file.ToString();
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game9").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game9").InnerText = file.ToString();
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game10").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game10").InnerText = file.ToString();
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game11").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game11").InnerText = file.ToString();
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game12").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game12").InnerText = file.ToString();
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game13").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game13").InnerText = file.ToString();
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game14").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game14").InnerText = file.ToString();
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game15").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game15").InnerText = file.ToString();
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game16").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game16").InnerText = file.ToString();
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game17").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game17").InnerText = file.ToString();
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game18").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game18").InnerText = file.ToString();
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game19").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game19").InnerText = file.ToString();
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game20").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game20").InnerText = file.ToString();
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game21").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game21").InnerText = file.ToString();
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game22").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game22").InnerText = file.ToString();
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game23").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game23").InnerText = file.ToString();
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game24").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game24").InnerText = file.ToString();
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game25").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game25").InnerText = file.ToString();
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game26").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game26").InnerText = file.ToString();
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game27").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game27").InnerText = file.ToString();
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game28").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game28").InnerText = file.ToString();
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game29").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game29").InnerText = file.ToString();
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game30").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game30").InnerText = file.ToString();
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game31").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game31").InnerText = file.ToString();
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game32").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game32").InnerText = file.ToString();
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game33").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game33").InnerText = file.ToString();
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game34").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game34").InnerText = file.ToString();
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game35").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game35").InnerText = file.ToString();
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game36").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game36").InnerText = file.ToString();
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game37").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game37").InnerText = file.ToString();
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game38").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game38").InnerText = file.ToString();
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game39").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game39").InnerText = file.ToString();
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game40").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game40").InnerText = file.ToString();
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game41").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game41").InnerText = file.ToString();
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game42").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game42").InnerText = file.ToString();
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game43").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game43").InnerText = file.ToString();
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game44").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game44").InnerText = file.ToString();
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game45").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game45").InnerText = file.ToString();
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game46").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game46").InnerText = file.ToString();
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game47").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game47").InnerText = file.ToString();
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game48").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game48").InnerText = file.ToString();
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game49").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game49").InnerText = file.ToString();
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game50").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game50").InnerText = file.ToString();
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game51").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game51").InnerText = file.ToString();
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game52").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game52").InnerText = file.ToString();
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game53").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game53").InnerText = file.ToString();
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game54").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game54").InnerText = file.ToString();
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game55").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game55").InnerText = file.ToString();
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game56").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game56").InnerText = file.ToString();
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game57").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game57").InnerText = file.ToString();
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game58").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game58").InnerText = file.ToString();
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game59").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game59").InnerText = file.ToString();
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game60").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game60").InnerText = file.ToString();
                                    xml1.Save(Variables.XFile);
                                }
                            }
                        }
                        if (Path.GetExtension(file.ToString()).Contains("lnk"))
                        {
                            XmlDocument xml1 = new XmlDocument();
                            xml1.Load(Variables.XFile);
                            XmlNodeList nodes = xml1.SelectNodes("//Games");
                            foreach (XmlElement element in nodes)
                            {
                                if (element.SelectSingleNode("Game1").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game1").InnerText = NewFile;
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game2").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game2").InnerText = NewFile;
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game3").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game3").InnerText = NewFile;
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game4").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game4").InnerText = NewFile;
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game5").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game5").InnerText = NewFile;
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game6").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game6").InnerText = NewFile;
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game7").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game7").InnerText = NewFile;
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game8").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game8").InnerText = NewFile;
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game9").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game9").InnerText = NewFile;
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game10").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game10").InnerText = NewFile;
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game11").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game11").InnerText = NewFile;
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game12").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game12").InnerText = NewFile;
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game13").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game13").InnerText = NewFile;
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game14").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game14").InnerText = NewFile;
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game15").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game15").InnerText = NewFile;
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game16").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game16").InnerText = NewFile;
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game17").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game17").InnerText = NewFile;
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game18").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game18").InnerText = NewFile;
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game19").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game19").InnerText = NewFile;
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game20").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game20").InnerText = NewFile;
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game21").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game21").InnerText = NewFile;
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game22").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game22").InnerText = NewFile;
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game23").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game23").InnerText = NewFile;
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game24").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game24").InnerText = NewFile;
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game25").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game25").InnerText = NewFile;
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game26").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game26").InnerText = NewFile;
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game27").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game27").InnerText = NewFile;
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game28").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game28").InnerText = NewFile;
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game29").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game29").InnerText = NewFile;
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game30").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game30").InnerText = NewFile;
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game31").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game31").InnerText = NewFile;
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game32").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game32").InnerText = NewFile;
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game33").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game33").InnerText = NewFile;
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game34").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game34").InnerText = NewFile;
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game35").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game35").InnerText = NewFile;
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game36").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game36").InnerText = NewFile;
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game37").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game37").InnerText = NewFile;
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game38").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game38").InnerText = NewFile;
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game39").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game39").InnerText = NewFile;
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game40").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game40").InnerText = NewFile;
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game41").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game41").InnerText = NewFile;
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game42").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game42").InnerText = NewFile;
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game43").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game43").InnerText = NewFile;
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game44").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game44").InnerText = NewFile;
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game45").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game45").InnerText = NewFile;
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game46").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game46").InnerText = NewFile;
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game47").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game47").InnerText = NewFile;
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game48").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game48").InnerText = NewFile;
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game49").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game49").InnerText = NewFile;
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game50").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game50").InnerText = NewFile;
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game51").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game51").InnerText = NewFile;
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game52").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game52").InnerText = NewFile;
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game53").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game53").InnerText = NewFile;
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game54").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game54").InnerText = NewFile;
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game55").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game55").InnerText = NewFile;
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game56").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game56").InnerText = NewFile;
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game57").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game57").InnerText = NewFile;
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game58").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game58").InnerText = NewFile;
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game59").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game59").InnerText = NewFile;
                                    xml1.Save(Variables.XFile);
                                }
                                if (element.SelectSingleNode("Game60").InnerText == "0" && !Variables.Added)
                                {
                                    Variables.Added = true;
                                    element.SelectSingleNode("Game60").InnerText = NewFile;
                                    xml1.Save(Variables.XFile);
                                }
                            }
                        }
                        if (Variables.Added != true && Variables.ImageAdded != true)
                        {
                            if (Message)
                            {
                                Message = false;
                                DialogNoBox.Show(Properties.Resources.ResourceManager.GetString("ActionCancelled"));
                            }
                            Variables.Added = false;
                            Variables.ImageAdded = false;
                            Variables.ArgsAdded = false;
                        }
                        if (Files == 1)
                        {
                            Application.Exit();
                        }
                    }
                }
                Variables.Added = false;
                Variables.ImageAdded = false;
                Variables.ArgsAdded = false;
            }
        }
        static bool itemsChecked = false;
        private void button3_Click(object sender, EventArgs e)
        {
            if (itemsChecked)
            {
                for (int i = 0; i < Variables.listBox1.Items.Count; i++)
                {
                    Variables.listBox1.SetItemChecked(i, false);
                    Variables.button3.Text = "Select All";
                    itemsChecked = false;
                }
            }
            else
            {
                for (int i = 0; i < Variables.listBox1.Items.Count; i++)
                {
                    Variables.listBox1.SetItemChecked(i, true);
                    Variables.button3.Text = "Select None";
                    itemsChecked = true;
                }
            }
        }
    }
}
