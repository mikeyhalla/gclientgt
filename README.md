GClient GT
=========
*(Game Client Gaming Turbo Edition)*
This project was created for those of us that just want to sit back on the couch and play video games without all the fuss.
Documentation is in the start menu folder as well as a shortcut to Game Banners downloads...

***Now with Steam support***

Just plug in your Xbox controller or use the guide button to open to the GClient GT and navigate your games with the analog stick...

***[GClient GT Main Page](http://mikeyhalla.github.io/GClient/)***

![ScreenShot1](https://raw.githubusercontent.com/mikeyhalla/GClient/master/images/GClientGT.jpg)
![ScreenShot2](https://raw.githubusercontent.com/mikeyhalla/GClient/master/images/Screenshot2.jpg)
![ScreenShot3](https://raw.githubusercontent.com/mikeyhalla/GClient/master/images/Screenshot3.jpg)

_____________________________________________________________________________________ 

If something is not working please report it...
[Report Issues Here!](https://github.com/mikeyhalla/GClient/issues)


*IMPORTANT:*
--------------
This program runs with administrative rights for the purpose of many games needing it. While installing, a scheduled task is created to start GClient at logon.
The service is automatic and used only for optimizations.
There are plenty of settings and I advise you to be very careful with them especially fanboost. 

_____________________________________________________________________________________ 

*Program Locations:*
Main executable path; where you installed it - ideally "C:\Program Files (x86)\mikeyhalla\GClientGT"
Program Data; "C:\ProgramData\GClientGT"
Shortcut in user start menu programs folder "GClientGT"
A scheduled task is also created to start the program at logon.

_____________________________________________________________________________________ 

 Fixed: (In order from latest to oldest)
 Everything as far as I can tell...
 Not detecting fullscreen
 File Check - Auto update is now downloading correct file
 Arguments affecting some games like borderlands 2 maps
 GClient not starting //added XNA Framework to setup package
 
 If you are having problems:
 1. Uninstall and cleanup the program locations
 2. Re-download and reinstall
 3. Make sure XNA Framework and .NetFramework 4.5 are installed
 4. You are running Windows Vista or later
 5. Let me know the problem

_____________________________________________________________________________________ 

*DO NOT ENABLE FAN BOOST UNLESS YOU ARE 100% SURE THAT YOUR LAPTOP IS SUPPORTED OTHERWISE YOUR LAPTOP MAY FRY!*

*If you want to use Fan Boost:*
Check: [GClient Main Page](http://mikeyhalla.github.io/GClient/) for any updates on models supported.

The drivers are located in the Program Files\GClientGT Folder and you will need to install them only if supported.
As of now Fan boost only supports Acer V3-571, V3-571G, 5750G, 5742G, 5741G Laptops and not guaranteed to work on even these models.
*This is said to be risky because it is writing data to your BIOS and can brick your computer, especially if you try on a model not listed.*

I recommend that you find some type of fan boosting application for your computer if your model is not supported as it boosts the performance a great deal.

I personally have been able to raise the graphics level quite a bit due to this.

_____________________________________________________________________________________