﻿using System;
using System.Globalization;
using System.ServiceProcess;
using System.Threading;

namespace Boost
{
    static class Program
    {
        [STAThread]
        static void Main()
        {
#if DEBUG
            Service1 service = null;
            try
            {
                Thread.CurrentThread.CurrentUICulture = CultureInfo.CurrentCulture;
                service = new Service1();
                service.OnDebug();
                System.Threading.Thread.Sleep(System.Threading.Timeout.Infinite);
            }
            finally
            {
                if (service != null)
                {
                    service.Dispose();
                }
            }
#else
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CurrentCulture;
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new Service1()
            };
            ServiceBase.Run(ServicesToRun);
#endif
        }
    }
}
