﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.ServiceProcess;
using System.Threading;

[assembly: CLSCompliant(true)]
namespace Boost
{
    class FileWatcher : IDisposable
    {
        private FileSystemWatcher _fileWatcher;
        static string SysPath = (Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) + @"\GClientGT");
        static string LogPath = SysPath + @"\LOGS";
        static string SysFile = SysPath + @"\GClientGT.ini";
        static ServiceController[] services = ServiceController.GetServices();
        public FileWatcher()
        {
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CurrentCulture;
            _fileWatcher = new FileSystemWatcher(PathLocation());
            _fileWatcher.Created += new FileSystemEventHandler(_fileWatcher_Created);
            _fileWatcher.Deleted += new FileSystemEventHandler(_fileWatcher_Deleted);
            _fileWatcher.EnableRaisingEvents = true;
            _fileWatcher.Filter = "ServiceCom.com";
        }
        static string PathLocation()
        {
            return SysPath;
        }
        static bool AxInstSVON;
        static bool SensrSvcON;
        static bool ALGON;
        static bool AppMgmtON;
        static bool BDESVCON;
        static bool bthservON;
        static bool PeerDistSvcON;
        static bool CertPropSvcON;
        static bool BrowserON;
        static bool VaultSvcON;
        static bool DPSON;
        static bool WdiSystemHostON;
        static bool TrkWksON;
        static bool FaxON;
        static bool fdPHostON;
        static bool hidservON;
        static bool SharedAccessON;
        static bool iphlpsvcON;
        static bool KtmRmON;
        static bool lltdsvcON;
        static bool swprvON;
        static bool MSiSCSION;
        static bool NetTcpPortSharingON;
        static bool NetlogonON;
        static bool CscServiceON;
        static bool PNRPsvcON;
        static bool p2psvcON;
        static bool p2pimsvcON;
        static bool plaON;
        static bool PNRPAutoRegON;
        static bool WPDBusEnumON;
        static bool wercplsupportON;
        static bool PcaSvcON;
        static bool QWAVEON;
        static bool SessionEnvON;
        static bool RpcLocatorON;
        static bool RemoteRegistryON;
        static bool RemoteAccessON;
        static bool SCardSvrON;
        static bool SCPolicySvcON;
        static bool SNMPTRAPON;
        static bool SSDPSRVON;
        static bool StorSvcON;
        static bool TabletInputServiceON;
        static bool WebClientON;
        static bool WbioSrvcON;
        static bool WcsPlugInServiceON;
        static bool wcncsvcON;
        static bool WerSvcON;
        static bool WecsvcON;
        static bool StiSvcON;
        static bool WMPNetworkSvcON;
        static bool WinRMON;
        static bool W32TimeON;
        static bool SpoolerON;
        static bool ThemesON;
        static bool wuauservON;
        static bool MSDTCON;
        static bool RasAutoON;
        static bool RasManON;
        static bool TapiSrvON;
        static bool Log = false;
        static bool Boost = false;
        static bool Thread1 = false;
        static bool Thread2 = false;
        static bool Cleanup = true;
        void Logging()
        {
            try
            {
                if (Log == false)
                {
                    Log = true;
                    Thread thread = new Thread(Boosting);
                    thread.IsBackground = true;
                    thread.Start();
                    Directory.CreateDirectory(LogPath);
                    if (!File.Exists(LogPath + @"\GClientService.Log"))
                    {
                        var createLog = File.Create(LogPath + @"\GClientService.Log");
                        createLog.Dispose();
                    }
                    if (File.Exists(LogPath + @"\GClientService.Log"))
                    {
                        File.Delete(LogPath + @"\GClientService-Previous.log");
                    }
                    bool TempExist = true;
                    while (TempExist)
                    {
                        if (File.Exists(LogPath + @"\GClientService-Previous.log"))
                        {
                            Thread.Sleep(50);
                        }
                        else
                        {
                            File.Move(LogPath + @"\GClientService.Log", LogPath + @"\GClientService-Previous.log");
                            TempExist = false;
                        }
                    }
                    Trace.Listeners.Clear();
                    TextWriterTraceListener twtl = null;
                    try
                    {
                        twtl = new TextWriterTraceListener(LogPath + @"\GClientService.Log");
                        ConsoleTraceListener ctl = null;
                        try
                        {
                            ctl = new ConsoleTraceListener(false);
                            Trace.Listeners.Add(twtl);
                            Trace.Listeners.Add(ctl);
                            Trace.AutoFlush = true;
                        }
                        finally
                        {
                            if (ctl != null)
                            {
                                ctl.Dispose();
                            }
                        }
                    }
                    finally
                    {
                        if (twtl != null)
                        {
                            twtl.Dispose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
            }
        }
        void _fileWatcher_Created(object sender, FileSystemEventArgs e)
        {
            while(Thread1 || Thread2)
            {
                Thread.Sleep(50);
            }
            Boost = true;
            Thread logging = new Thread(Logging);
            logging.Start();
            Thread thread = new Thread(new ThreadStart(AxInstSV));
            thread.Start();
        }
        void _fileWatcher_Deleted(object sender, FileSystemEventArgs e)
        {
            while (Thread1 || Thread2)
            {
                Thread.Sleep(50);
            }
            Boost = false;
            Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("RestartingServices"));
            Thread thread = new Thread(new ThreadStart(StopBoost));
            thread.Start();
        }

        static void Boosting()
        {
            while (Cleanup)
            {
                if (Boost)
                {
                    Thread.Sleep(1000);
                }
                else if (!Boost)
                {
                    Thread.Sleep(7000);
                    if (!Boost)
                    {
                        if (AxInstSVON || SensrSvcON || ALGON || AppMgmtON || BDESVCON || bthservON || PeerDistSvcON || CertPropSvcON || BrowserON || VaultSvcON || DPSON || WdiSystemHostON || TrkWksON || FaxON || fdPHostON || hidservON || SharedAccessON || iphlpsvcON || KtmRmON || lltdsvcON || swprvON || MSiSCSION || NetTcpPortSharingON || NetlogonON || CscServiceON || PNRPsvcON || p2psvcON || p2pimsvcON || plaON || PNRPAutoRegON || WPDBusEnumON || wercplsupportON || PcaSvcON || QWAVEON || SessionEnvON || RpcLocatorON || RemoteRegistryON || RemoteAccessON || SCardSvrON || SCPolicySvcON || SNMPTRAPON || SSDPSRVON || StorSvcON || TabletInputServiceON || WebClientON || WbioSrvcON || WcsPlugInServiceON || wcncsvcON || WerSvcON || WecsvcON || StiSvcON || WMPNetworkSvcON || WinRMON || W32TimeON || SpoolerON || ThemesON || wuauservON || MSDTCON || RasAutoON || RasManON || TapiSrvON)
                        {
                            Thread thread = new Thread(new ThreadStart(StopBoost));
                            thread.Start();
                        }
                    }
                }
            }
        }
        //Begin Boost
        static void AxInstSV()
        {
            Thread1 = true;
            Stream stream = null;
            Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("Optimizing"));
            try
            {
                stream = new FileStream(SysFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                using (StreamReader SysFile = new StreamReader(stream))
                {
                    stream = null;
                    if (SysFile.ReadToEnd().Contains("AxInstSV=true"))
                    {
                        foreach (ServiceController service in services)
                        {
                            if (service.ServiceName == "AxInstSV")
                            {
                                using (ServiceController AxInstSV = new ServiceController("AxInstSV"))
                                {
                                    while (AxInstSV.Equals(ServiceControllerStatus.StopPending) || AxInstSV.Equals(ServiceControllerStatus.StartPending))
                                    {
                                        Thread.Sleep(20);
                                    }
                                    if (AxInstSV.Status.Equals(ServiceControllerStatus.Running))
                                    {
                                        AxInstSV.Stop();
                                        AxInstSV.WaitForStatus(ServiceControllerStatus.Stopped);
                                        AxInstSVON = true;
                                        Trace.WriteLine(DateTime.Now + " Stopping: " + service.ServiceName);
                                    }
                                }
                            }
                        }
                    }
                }
                SensrSvc();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
            }
            finally
            {
                if (stream != null)
                {
                    stream.Dispose();
                }
            }
        }
        static void SensrSvc()
        {
            Stream stream = null;
            try
            {
                stream = new FileStream(SysFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                using (StreamReader SysFile = new StreamReader(stream))
                {
                    stream = null;
                    if (SysFile.ReadToEnd().Contains("SensrSvc=true"))
                    {
                        foreach (ServiceController service in services)
                        {
                            if (service.ServiceName == "SensrSvc")
                            {
                                using (ServiceController SensrSvc = new ServiceController("SensrSvc"))
                                {
                                    while (SensrSvc.Equals(ServiceControllerStatus.StopPending) || SensrSvc.Equals(ServiceControllerStatus.StartPending))
                                    {
                                        Thread.Sleep(20);
                                    }
                                    if (SensrSvc.Status.Equals(ServiceControllerStatus.Running))
                                    {
                                        SensrSvc.Stop();
                                        SensrSvc.WaitForStatus(ServiceControllerStatus.Stopped);
                                        SensrSvcON = true;
                                        Trace.WriteLine(DateTime.Now + " Stopping: " + service.ServiceName);
                                    }
                                }
                            }
                        }
                    }
                }
                ALG();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
            }
            finally
            {
                if (stream != null)
                {
                    stream.Dispose();
                }
            }
        }
        static void ALG()
        {
            Stream stream = null;
            try
            {
                stream = new FileStream(SysFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                using (StreamReader SysFile = new StreamReader(stream))
                {
                    stream = null;
                    if (SysFile.ReadToEnd().Contains("ALG=true"))
                    {
                        foreach (ServiceController service in services)
                        {
                            if (service.ServiceName == "ALG")
                            {
                                using (ServiceController ALG = new ServiceController("ALG"))
                                {
                                    while (ALG.Equals(ServiceControllerStatus.StopPending) || ALG.Equals(ServiceControllerStatus.StartPending))
                                    {
                                        Thread.Sleep(20);
                                    }
                                    if (ALG.Status.Equals(ServiceControllerStatus.Running))
                                    {
                                        ALG.Stop();
                                        ALG.WaitForStatus(ServiceControllerStatus.Stopped);
                                        ALGON = true;
                                        Trace.WriteLine(DateTime.Now + " Stopping: " + service.ServiceName);
                                    }
                                }
                            }
                        }
                    }
                }
                AppMgmt();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
            }
            finally
            {
                if (stream != null)
                {
                    stream.Dispose();
                }
            }
        }
        static void AppMgmt()
        {
            Stream stream = null;
            try
            {
                stream = new FileStream(SysFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                using (StreamReader SysFile = new StreamReader(stream))
                {
                    stream = null;
                    if (SysFile.ReadToEnd().Contains("AppMgmt=true"))
                    {
                        foreach (ServiceController service in services)
                        {
                            if (service.ServiceName == "AppMgmt")
                            {
                                using (ServiceController AppMgmt = new ServiceController("AppMgmt"))
                                {
                                    while (AppMgmt.Equals(ServiceControllerStatus.StopPending) || AppMgmt.Equals(ServiceControllerStatus.StartPending))
                                    {
                                        Thread.Sleep(20);
                                    }
                                    if (AppMgmt.Status.Equals(ServiceControllerStatus.Running))
                                    {
                                        AppMgmt.Stop();
                                        AppMgmt.WaitForStatus(ServiceControllerStatus.Stopped);
                                        AppMgmtON = true;
                                        Trace.WriteLine(DateTime.Now + " Stopping: " + service.ServiceName);
                                    }
                                }
                            }
                        }
                    }
                }
                BDESVC();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
            }
            finally
            {
                if (stream != null)
                {
                    stream.Dispose();
                }
            }
        }
        static void BDESVC()
        {
            Stream stream = null;
            try
            {
                stream = new FileStream(SysFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                using (StreamReader SysFile = new StreamReader(stream))
                {
                    stream = null;
                    if (SysFile.ReadToEnd().Contains("BDESVC=true"))
                    {
                        foreach (ServiceController service in services)
                        {
                            if (service.ServiceName == "BDESVC")
                            {
                                using (ServiceController BDESVC = new ServiceController("BDESVC"))
                                {
                                    while (BDESVC.Equals(ServiceControllerStatus.StopPending) || BDESVC.Equals(ServiceControllerStatus.StartPending))
                                    {
                                        Thread.Sleep(20);
                                    }
                                    if (BDESVC.Status.Equals(ServiceControllerStatus.Running))
                                    {
                                        BDESVC.Stop();
                                        BDESVC.WaitForStatus(ServiceControllerStatus.Stopped);
                                        BDESVCON = true;
                                        Trace.WriteLine(DateTime.Now + " Stopping: " + service.ServiceName);
                                    }
                                }
                            }
                        }
                    }
                }
                bthserv();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
            }
            finally
            {
                if (stream != null)
                {
                    stream.Dispose();
                }
            }
        }
        static void bthserv()
        {
            Stream stream = null;
            try
            {
                stream = new FileStream(SysFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                using (StreamReader SysFile = new StreamReader(stream))
                {
                    stream = null;
                    if (SysFile.ReadToEnd().Contains("bthserv=true"))
                    {
                        foreach (ServiceController service in services)
                        {
                            if (service.ServiceName == "bthserv")
                            {
                                using (ServiceController bthserv = new ServiceController("bthserv"))
                                {
                                    while (bthserv.Equals(ServiceControllerStatus.StopPending) || bthserv.Equals(ServiceControllerStatus.StartPending))
                                    {
                                        Thread.Sleep(20);
                                    }
                                    if (bthserv.Status.Equals(ServiceControllerStatus.Running))
                                    {
                                        bthserv.Stop();
                                        bthserv.WaitForStatus(ServiceControllerStatus.Stopped);
                                        bthservON = true;
                                        Trace.WriteLine(DateTime.Now + " Stopping: " + service.ServiceName);
                                    }
                                }
                            }
                        }
                    }
                }
                PeerDistSvc();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
            }
            finally
            {
                if (stream != null)
                {
                    stream.Dispose();
                }
            }
        }
        static void PeerDistSvc()
        {
            Stream stream = null;
            try
            {
                stream = new FileStream(SysFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                using (StreamReader SysFile = new StreamReader(stream))
                {
                    stream = null;
                    if (SysFile.ReadToEnd().Contains("PeerDistSvc=true"))
                    {
                        foreach (ServiceController service in services)
                        {
                            if (service.ServiceName == "PeerDistSvc")
                            {
                                using (ServiceController PeerDistSvc = new ServiceController("PeerDistSvc"))
                                {
                                    while (PeerDistSvc.Equals(ServiceControllerStatus.StopPending) || PeerDistSvc.Equals(ServiceControllerStatus.StartPending))
                                    {
                                        Thread.Sleep(20);
                                    }
                                    if (PeerDistSvc.Status.Equals(ServiceControllerStatus.Running))
                                    {
                                        PeerDistSvc.Stop();
                                        PeerDistSvc.WaitForStatus(ServiceControllerStatus.Stopped);
                                        PeerDistSvcON = true;
                                        Trace.WriteLine(DateTime.Now + " Stopping: " + service.ServiceName);
                                    }
                                }
                            }
                        }
                    }
                }
                CertPropSvc();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
            }
            finally
            {
                if (stream != null)
                {
                    stream.Dispose();
                }
            }
        }
        static void CertPropSvc()
        {
            Stream stream = null;
            try
            {
                stream = new FileStream(SysFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                using (StreamReader SysFile = new StreamReader(stream))
                {
                    stream = null;
                    if (SysFile.ReadToEnd().Contains("CertPropSvc=true"))
                    {
                        foreach (ServiceController service in services)
                        {
                            if (service.ServiceName == "CertPropSvc")
                            {
                                using (ServiceController CertPropSvc = new ServiceController("CertPropSvc"))
                                {
                                    while (CertPropSvc.Equals(ServiceControllerStatus.StopPending) || CertPropSvc.Equals(ServiceControllerStatus.StartPending))
                                    {
                                        Thread.Sleep(20);
                                    }
                                    if (CertPropSvc.Status.Equals(ServiceControllerStatus.Running))
                                    {
                                        CertPropSvc.Stop();
                                        CertPropSvc.WaitForStatus(ServiceControllerStatus.Stopped);
                                        CertPropSvcON = true;
                                        Trace.WriteLine(DateTime.Now + " Stopping: " + service.ServiceName);
                                    }
                                }
                            }
                        }
                    }
                }
                Browser();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
            }
            finally
            {
                if (stream != null)
                {
                    stream.Dispose();
                }
            }
        }
        static void Browser()
        {
            Stream stream = null;
            try
            {
                stream = new FileStream(SysFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                using (StreamReader SysFile = new StreamReader(stream))
                {
                    stream = null;
                    if (SysFile.ReadToEnd().Contains("Browser=true"))
                    {
                        foreach (ServiceController service in services)
                        {
                            if (service.ServiceName == "Browser")
                            {
                                using (ServiceController Browser = new ServiceController("Browser"))
                                {
                                    while (Browser.Equals(ServiceControllerStatus.StopPending) || Browser.Equals(ServiceControllerStatus.StartPending))
                                    {
                                        Thread.Sleep(20);
                                    }
                                    if (Browser.Status.Equals(ServiceControllerStatus.Running))
                                    {
                                        Browser.Stop();
                                        Browser.WaitForStatus(ServiceControllerStatus.Stopped);
                                        BrowserON = true;
                                        Trace.WriteLine(DateTime.Now + " Stopping: " + service.ServiceName);
                                    }
                                }
                            }
                        }
                    }
                }
                VaultSvc();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
            }
            finally
            {
                if (stream != null)
                {
                    stream.Dispose();
                }
            }
        }
        static void VaultSvc()
        {
            Stream stream = null;
            try
            {
                stream = new FileStream(SysFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                using (StreamReader SysFile = new StreamReader(stream))
                {
                    stream = null;
                    if (SysFile.ReadToEnd().Contains("VaultSvc=true"))
                    {
                        foreach (ServiceController service in services)
                        {
                            if (service.ServiceName == "VaultSvc")
                            {
                                using (ServiceController VaultSvc = new ServiceController("VaultSvc"))
                                {
                                    while (VaultSvc.Equals(ServiceControllerStatus.StopPending) || VaultSvc.Equals(ServiceControllerStatus.StartPending))
                                    {
                                        Thread.Sleep(20);
                                    }
                                    if (VaultSvc.Status.Equals(ServiceControllerStatus.Running))
                                    {
                                        VaultSvc.Stop();
                                        VaultSvc.WaitForStatus(ServiceControllerStatus.Stopped);
                                        VaultSvcON = true;
                                        Trace.WriteLine(DateTime.Now + " Stopping: " + service.ServiceName);
                                    }
                                }
                            }
                        }
                    }
                }
                DPS();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
            }
            finally
            {
                if (stream != null)
                {
                    stream.Dispose();
                }
            }
        }
        static void DPS()
        {
            Stream stream = null;
            try
            {
                stream = new FileStream(SysFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                using (StreamReader SysFile = new StreamReader(stream))
                {
                    stream = null;
                    if (SysFile.ReadToEnd().Contains("DPS=true"))
                    {
                        foreach (ServiceController service in services)
                        {
                            if (service.ServiceName == "DPS")
                            {
                                using (ServiceController DPS = new ServiceController("DPS"))
                                {
                                    while (DPS.Equals(ServiceControllerStatus.StopPending) || DPS.Equals(ServiceControllerStatus.StartPending))
                                    {
                                        Thread.Sleep(20);
                                    }
                                    if (DPS.Status.Equals(ServiceControllerStatus.Running))
                                    {
                                        DPS.Stop();
                                        DPS.WaitForStatus(ServiceControllerStatus.Stopped);
                                        DPSON = true;
                                        Trace.WriteLine(DateTime.Now + " Stopping: " + service.ServiceName);
                                    }
                                }
                            }
                        }
                    }
                }
                WdiSystemHost();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
            }
            finally
            {
                if (stream != null)
                {
                    stream.Dispose();
                }
            }
        }
        static void WdiSystemHost()
        {
            Stream stream = null;
            try
            {
                stream = new FileStream(SysFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                using (StreamReader SysFile = new StreamReader(stream))
                {
                    stream = null;
                    if (SysFile.ReadToEnd().Contains("WdiSystemHost=true"))
                    {
                        foreach (ServiceController service in services)
                        {
                            if (service.ServiceName == "WdiSystemHost")
                            {
                                using (ServiceController WdiSystemHost = new ServiceController("WdiSystemHost"))
                                {
                                    while (WdiSystemHost.Equals(ServiceControllerStatus.StopPending) || WdiSystemHost.Equals(ServiceControllerStatus.StartPending))
                                    {
                                        Thread.Sleep(20);
                                    }
                                    if (WdiSystemHost.Status.Equals(ServiceControllerStatus.Running))
                                    {
                                        WdiSystemHost.Stop();
                                        WdiSystemHost.WaitForStatus(ServiceControllerStatus.Stopped);
                                        WdiSystemHostON = true;
                                        Trace.WriteLine(DateTime.Now + " Stopping: " + service.ServiceName);
                                    }
                                }
                            }
                        }
                    }
                }
                TrkWks();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
            }
            finally
            {
                if (stream != null)
                {
                    stream.Dispose();
                }
            }
        }
        static void TrkWks()
        {
            Stream stream = null;
            try
            {
                stream = new FileStream(SysFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                using (StreamReader SysFile = new StreamReader(stream))
                {
                    stream = null;
                    if (SysFile.ReadToEnd().Contains("TrkWks=true"))
                    {
                        foreach (ServiceController service in services)
                        {
                            if (service.ServiceName == "TrkWks")
                            {
                                using (ServiceController TrkWks = new ServiceController("TrkWks"))
                                {
                                    while (TrkWks.Equals(ServiceControllerStatus.StopPending) || TrkWks.Equals(ServiceControllerStatus.StartPending))
                                    {
                                        Thread.Sleep(20);
                                    }
                                    if (TrkWks.Status.Equals(ServiceControllerStatus.Running))
                                    {
                                        TrkWks.Stop();
                                        TrkWks.WaitForStatus(ServiceControllerStatus.Stopped);
                                        TrkWksON = true;
                                        Trace.WriteLine(DateTime.Now + " Stopping: " + service.ServiceName);
                                    }
                                }
                            }
                        }
                    }
                }
                Fax();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
            }
            finally
            {
                if (stream != null)
                {
                    stream.Dispose();
                }
            }
        }
        static void Fax()
        {
            Stream stream = null;
            try
            {
                stream = new FileStream(SysFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                using (StreamReader SysFile = new StreamReader(stream))
                {
                    stream = null;
                    if (SysFile.ReadToEnd().Contains("Fax=true"))
                    {
                        foreach (ServiceController service in services)
                        {
                            if (service.ServiceName == "Fax")
                            {
                                using (ServiceController Fax = new ServiceController("Fax"))
                                {
                                    while (Fax.Equals(ServiceControllerStatus.StopPending) || Fax.Equals(ServiceControllerStatus.StartPending))
                                    {
                                        Thread.Sleep(20);
                                    }
                                    if (Fax.Status.Equals(ServiceControllerStatus.Running))
                                    {
                                        Fax.Stop();
                                        Fax.WaitForStatus(ServiceControllerStatus.Stopped);
                                        FaxON = true;
                                        Trace.WriteLine(DateTime.Now + " Stopping: " + service.ServiceName);
                                    }
                                }
                            }
                        }
                    }
                }
                fdPHost();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
            }
            finally
            {
                if (stream != null)
                {
                    stream.Dispose();
                }
            }
        }
        static void fdPHost()
        {
            Stream stream = null;
            try
            {
                stream = new FileStream(SysFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                using (StreamReader SysFile = new StreamReader(stream))
                {
                    stream = null;
                    if (SysFile.ReadToEnd().Contains("fdPHost=true"))
                    {
                        foreach (ServiceController service in services)
                        {
                            if (service.ServiceName == "fdPHost")
                            {
                                using (ServiceController fdPHost = new ServiceController("fdPHost"))
                                {
                                    while (fdPHost.Equals(ServiceControllerStatus.StopPending) || fdPHost.Equals(ServiceControllerStatus.StartPending))
                                    {
                                        Thread.Sleep(20);
                                    }
                                    if (fdPHost.Status.Equals(ServiceControllerStatus.Running))
                                    {
                                        fdPHost.Stop();
                                        fdPHost.WaitForStatus(ServiceControllerStatus.Stopped);
                                        fdPHostON = true;
                                        Trace.WriteLine(DateTime.Now + " Stopping: " + service.ServiceName);
                                    }
                                }
                            }
                        }
                    }
                }
                hidserv();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
            }
            finally
            {
                if (stream != null)
                {
                    stream.Dispose();
                }
            }
        }
        static void hidserv()
        {
            Stream stream = null;
            try
            {
                stream = new FileStream(SysFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                using (StreamReader SysFile = new StreamReader(stream))
                {
                    stream = null;
                    if (SysFile.ReadToEnd().Contains("hidserv=true"))
                    {
                        foreach (ServiceController service in services)
                        {
                            if (service.ServiceName == "hidserv")
                            {
                                using (ServiceController hidserv = new ServiceController("hidserv"))
                                {
                                    while (hidserv.Equals(ServiceControllerStatus.StopPending) || hidserv.Equals(ServiceControllerStatus.StartPending))
                                    {
                                        Thread.Sleep(20);
                                    }
                                    if (hidserv.Status.Equals(ServiceControllerStatus.Running))
                                    {
                                        hidserv.Stop();
                                        hidserv.WaitForStatus(ServiceControllerStatus.Stopped);
                                        hidservON = true;
                                        Trace.WriteLine(DateTime.Now + " Stopping: " + service.ServiceName);
                                    }
                                }
                            }
                        }
                    }
                }
                SharedAccess();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
            }
            finally
            {
                if (stream != null)
                {
                    stream.Dispose();
                }
            }
        }
        static void SharedAccess()
        {
            Stream stream = null;
            try
            {
                stream = new FileStream(SysFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                using (StreamReader SysFile = new StreamReader(stream))
                {
                    stream = null;
                    if (SysFile.ReadToEnd().Contains("SharedAccess=true"))
                    {
                        foreach (ServiceController service in services)
                        {
                            if (service.ServiceName == "SharedAccess")
                            {
                                using (ServiceController SharedAccess = new ServiceController("SharedAccess"))
                                {
                                    while (SharedAccess.Equals(ServiceControllerStatus.StopPending) || SharedAccess.Equals(ServiceControllerStatus.StartPending))
                                    {
                                        Thread.Sleep(20);
                                    }
                                    if (SharedAccess.Status.Equals(ServiceControllerStatus.Running))
                                    {
                                        SharedAccess.Stop();
                                        SharedAccess.WaitForStatus(ServiceControllerStatus.Stopped);
                                        SharedAccessON = true;
                                        Trace.WriteLine(DateTime.Now + " Stopping: " + service.ServiceName);
                                    }
                                }
                            }
                        }
                    }
                }
                iphlpsvc();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
            }
            finally
            {
                if (stream != null)
                {
                    stream.Dispose();
                }
            }
        }
        static void iphlpsvc()
        {
            Stream stream = null;
            try
            {
                stream = new FileStream(SysFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                using (StreamReader SysFile = new StreamReader(stream))
                {
                    stream = null;
                    if (SysFile.ReadToEnd().Contains("iphlpsvc=true"))
                    {
                        foreach (ServiceController service in services)
                        {
                            if (service.ServiceName == "iphlpsvc")
                            {
                                using (ServiceController iphlpsvc = new ServiceController("iphlpsvc"))
                                {
                                    while (iphlpsvc.Equals(ServiceControllerStatus.StopPending) || iphlpsvc.Equals(ServiceControllerStatus.StartPending))
                                    {
                                        Thread.Sleep(20);
                                    }
                                    if (iphlpsvc.Status.Equals(ServiceControllerStatus.Running))
                                    {
                                        iphlpsvc.Stop();
                                        iphlpsvc.WaitForStatus(ServiceControllerStatus.Stopped);
                                        iphlpsvcON = true;
                                        Trace.WriteLine(DateTime.Now + " Stopping: " + service.ServiceName);
                                    }
                                }
                            }
                        }
                    }
                }
                KtmRm();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
            }
            finally
            {
                if (stream != null)
                {
                    stream.Dispose();
                }
            }
        }
        static void KtmRm()
        {
            Stream stream = null;
            try
            {
                stream = new FileStream(SysFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                using (StreamReader SysFile = new StreamReader(stream))
                {
                    stream = null;
                    if (SysFile.ReadToEnd().Contains("KtmRm=true"))
                    {
                        foreach (ServiceController service in services)
                        {
                            if (service.ServiceName == "KtmRm")
                            {
                                using (ServiceController KtmRm = new ServiceController("KtmRm"))
                                {
                                    while (KtmRm.Equals(ServiceControllerStatus.StopPending) || KtmRm.Equals(ServiceControllerStatus.StartPending))
                                    {
                                        Thread.Sleep(20);
                                    }
                                    if (KtmRm.Status.Equals(ServiceControllerStatus.Running))
                                    {
                                        KtmRm.Stop();
                                        KtmRm.WaitForStatus(ServiceControllerStatus.Stopped);
                                        KtmRmON = true;
                                        Trace.WriteLine(DateTime.Now + " Stopping: " + service.ServiceName);
                                    }
                                }
                            }
                        }
                    }
                }
                lltdsvc();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
            }
            finally
            {
                if (stream != null)
                {
                    stream.Dispose();
                }
            }
        }
        static void lltdsvc()
        {
            Stream stream = null;
            try
            {
                stream = new FileStream(SysFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                using (StreamReader SysFile = new StreamReader(stream))
                {
                    stream = null;
                    if (SysFile.ReadToEnd().Contains("lltdsvc=true"))
                    {
                        foreach (ServiceController service in services)
                        {
                            if (service.ServiceName == "lltdsvc")
                            {
                                using (ServiceController lltdsvc = new ServiceController("lltdsvc"))
                                {
                                    while (lltdsvc.Equals(ServiceControllerStatus.StopPending) || lltdsvc.Equals(ServiceControllerStatus.StartPending))
                                    {
                                        Thread.Sleep(20);
                                    }
                                    if (lltdsvc.Status.Equals(ServiceControllerStatus.Running))
                                    {
                                        lltdsvc.Stop();
                                        lltdsvc.WaitForStatus(ServiceControllerStatus.Stopped);
                                        lltdsvcON = true;
                                        Trace.WriteLine(DateTime.Now + " Stopping: " + service.ServiceName);
                                    }
                                }
                            }
                        }
                    }
                }
                swprv();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
            }
            finally
            {
                if (stream != null)
                {
                    stream.Dispose();
                }
            }
        }
        static void swprv()
        {
            Stream stream = null;
            try
            {
                stream = new FileStream(SysFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                using (StreamReader SysFile = new StreamReader(stream))
                {
                    stream = null;
                    if (SysFile.ReadToEnd().Contains("swprv=true"))
                    {
                        foreach (ServiceController service in services)
                        {
                            if (service.ServiceName == "swprv")
                            {
                                using (ServiceController swprv = new ServiceController("swprv"))
                                {
                                    while (swprv.Equals(ServiceControllerStatus.StopPending) || swprv.Equals(ServiceControllerStatus.StartPending))
                                    {
                                        Thread.Sleep(20);
                                    }
                                    if (swprv.Status.Equals(ServiceControllerStatus.Running))
                                    {
                                        swprv.Stop();
                                        swprv.WaitForStatus(ServiceControllerStatus.Stopped);
                                        swprvON = true;
                                        Trace.WriteLine(DateTime.Now + " Stopping: " + service.ServiceName);
                                    }
                                }
                            }
                        }
                    }
                }
                MSiSCSI();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
            }
            finally
            {
                if (stream != null)
                {
                    stream.Dispose();
                }
            }
        }
        static void MSiSCSI()
        {
            Stream stream = null;
            try
            {
                stream = new FileStream(SysFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                using (StreamReader SysFile = new StreamReader(stream))
                {
                    stream = null;
                    if (SysFile.ReadToEnd().Contains("MSiSCSI=true"))
                    {
                        foreach (ServiceController service in services)
                        {
                            if (service.ServiceName == "MSiSCSI")
                            {
                                using (ServiceController MSiSCSI = new ServiceController("MSiSCSI"))
                                {
                                    while (MSiSCSI.Equals(ServiceControllerStatus.StopPending) || MSiSCSI.Equals(ServiceControllerStatus.StartPending))
                                    {
                                        Thread.Sleep(20);
                                    }
                                    if (MSiSCSI.Status.Equals(ServiceControllerStatus.Running))
                                    {
                                        MSiSCSI.Stop();
                                        MSiSCSI.WaitForStatus(ServiceControllerStatus.Stopped);
                                        MSiSCSION = true;
                                        Trace.WriteLine(DateTime.Now + " Stopping: " + service.ServiceName);
                                    }
                                }
                            }
                        }
                    }
                }
                NetTcpPortSharing();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
            }
            finally
            {
                if (stream != null)
                {
                    stream.Dispose();
                }
            }
        }
        static void NetTcpPortSharing()
        {
            Stream stream = null;
            try
            {
                stream = new FileStream(SysFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                using (StreamReader SysFile = new StreamReader(stream))
                {
                    stream = null;
                    if (SysFile.ReadToEnd().Contains("NetTcpPortSharing=true"))
                    {
                        foreach (ServiceController service in services)
                        {
                            if (service.ServiceName == "NetTcpPortSharing")
                            {
                                using (ServiceController NetTcpPortSharing = new ServiceController("NetTcpPortSharing"))
                                {
                                    while (NetTcpPortSharing.Equals(ServiceControllerStatus.StopPending) || NetTcpPortSharing.Equals(ServiceControllerStatus.StartPending))
                                    {
                                        Thread.Sleep(20);
                                    }
                                    if (NetTcpPortSharing.Status.Equals(ServiceControllerStatus.Running))
                                    {
                                        NetTcpPortSharing.Stop();
                                        NetTcpPortSharing.WaitForStatus(ServiceControllerStatus.Stopped);
                                        NetTcpPortSharingON = true;
                                        Trace.WriteLine(DateTime.Now + " Stopping: " + service.ServiceName);
                                    }
                                }
                            }
                        }
                    }
                }
                Netlogon();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
            }
            finally
            {
                if (stream != null)
                {
                    stream.Dispose();
                }
            }
        }
        static void Netlogon()
        {
            Stream stream = null;
            try
            {
                stream = new FileStream(SysFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                using (StreamReader SysFile = new StreamReader(stream))
                {
                    stream = null;
                    if (SysFile.ReadToEnd().Contains("Netlogon=true"))
                    {
                        foreach (ServiceController service in services)
                        {
                            if (service.ServiceName == "Netlogon")
                            {
                                using (ServiceController Netlogon = new ServiceController("Netlogon"))
                                {
                                    while (Netlogon.Equals(ServiceControllerStatus.StopPending) || Netlogon.Equals(ServiceControllerStatus.StartPending))
                                    {
                                        Thread.Sleep(20);
                                    }
                                    if (Netlogon.Status.Equals(ServiceControllerStatus.Running))
                                    {
                                        Netlogon.Stop();
                                        Netlogon.WaitForStatus(ServiceControllerStatus.Stopped);
                                        NetlogonON = true;
                                        Trace.WriteLine(DateTime.Now + " Stopping: " + service.ServiceName);
                                    }
                                }
                            }
                        }
                    }
                }
                CscService();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
            }
            finally
            {
                if (stream != null)
                {
                    stream.Dispose();
                }
            }
        }
        static void CscService()
        {
            Stream stream = null;
            try
            {
                stream = new FileStream(SysFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                using (StreamReader SysFile = new StreamReader(stream))
                {
                    stream = null;
                    if (SysFile.ReadToEnd().Contains("CscService=true"))
                    {
                        foreach (ServiceController service in services)
                        {
                            if (service.ServiceName == "CscService")
                            {
                                using (ServiceController CscService = new ServiceController("CscService"))
                                {
                                    while (CscService.Equals(ServiceControllerStatus.StopPending) || CscService.Equals(ServiceControllerStatus.StartPending))
                                    {
                                        Thread.Sleep(20);
                                    }
                                    if (CscService.Status.Equals(ServiceControllerStatus.Running))
                                    {
                                        CscService.Stop();
                                        CscService.WaitForStatus(ServiceControllerStatus.Stopped);
                                        CscServiceON = true;
                                        Trace.WriteLine(DateTime.Now + " Stopping: " + service.ServiceName);
                                    }
                                }
                            }
                        }
                    }
                }
                PNRPsvc();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
            }
            finally
            {
                if (stream != null)
                {
                    stream.Dispose();
                }
            }
        }
        static void PNRPsvc()
        {
            Stream stream = null;
            try
            {
                stream = new FileStream(SysFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                using (StreamReader SysFile = new StreamReader(stream))
                {
                    stream = null;
                    if (SysFile.ReadToEnd().Contains("PNRPsvc=true"))
                    {
                        foreach (ServiceController service in services)
                        {
                            if (service.ServiceName == "PNRPsvc")
                            {
                                using (ServiceController PNRPsvc = new ServiceController("PNRPsvc"))
                                {
                                    while (PNRPsvc.Equals(ServiceControllerStatus.StopPending) || PNRPsvc.Equals(ServiceControllerStatus.StartPending))
                                    {
                                        Thread.Sleep(20);
                                    }
                                    if (PNRPsvc.Status.Equals(ServiceControllerStatus.Running))
                                    {
                                        PNRPsvc.Stop();
                                        PNRPsvc.WaitForStatus(ServiceControllerStatus.Stopped);
                                        PNRPsvcON = true;
                                        Trace.WriteLine(DateTime.Now + " Stopping: " + service.ServiceName);
                                    }
                                }
                            }
                        }
                    }
                }
                p2psvc();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
            }
            finally
            {
                if (stream != null)
                {
                    stream.Dispose();
                }
            }
        }
        static void p2psvc()
        {
            Stream stream = null;
            try
            {
                stream = new FileStream(SysFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                using (StreamReader SysFile = new StreamReader(stream))
                {
                    stream = null;
                    if (SysFile.ReadToEnd().Contains("p2psvc=true"))
                    {
                        foreach (ServiceController service in services)
                        {
                            if (service.ServiceName == "p2psvc")
                            {
                                using (ServiceController p2psvc = new ServiceController("p2psvc"))
                                {
                                    while (p2psvc.Equals(ServiceControllerStatus.StopPending) || p2psvc.Equals(ServiceControllerStatus.StartPending))
                                    {
                                        Thread.Sleep(20);
                                    }
                                    if (p2psvc.Status.Equals(ServiceControllerStatus.Running))
                                    {
                                        p2psvc.Stop();
                                        p2psvc.WaitForStatus(ServiceControllerStatus.Stopped);
                                        p2psvcON = true;
                                        Trace.WriteLine(DateTime.Now + " Stopping: " + service.ServiceName);
                                    }
                                }
                            }
                        }
                    }
                }
                p2pimsvc();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
            }
            finally
            {
                if (stream != null)
                {
                    stream.Dispose();
                }
            }
        }
        static void p2pimsvc()
        {
            Stream stream = null;
            try
            {
                stream = new FileStream(SysFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                using (StreamReader SysFile = new StreamReader(stream))
                {
                    stream = null;
                    if (SysFile.ReadToEnd().Contains("p2pimsvc=true"))
                    {
                        foreach (ServiceController service in services)
                        {
                            if (service.ServiceName == "p2pimsvc")
                            {
                                using (ServiceController p2pimsvc = new ServiceController("p2pimsvc"))
                                {
                                    while (p2pimsvc.Equals(ServiceControllerStatus.StopPending) || p2pimsvc.Equals(ServiceControllerStatus.StartPending))
                                    {
                                        Thread.Sleep(20);
                                    }
                                    if (p2pimsvc.Status.Equals(ServiceControllerStatus.Running))
                                    {
                                        p2pimsvc.Stop();
                                        p2pimsvc.WaitForStatus(ServiceControllerStatus.Stopped);
                                        p2pimsvcON = true;
                                        Trace.WriteLine(DateTime.Now + " Stopping: " + service.ServiceName);
                                    }
                                }
                            }
                        }
                    }
                }
                pla();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
            }
            finally
            {
                if (stream != null)
                {
                    stream.Dispose();
                }
            }
        }
        static void pla()
        {
            Stream stream = null;
            try
            {
                stream = new FileStream(SysFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                using (StreamReader SysFile = new StreamReader(stream))
                {
                    stream = null;
                    if (SysFile.ReadToEnd().Contains("pla=true"))
                    {
                        foreach (ServiceController service in services)
                        {
                            if (service.ServiceName == "pla")
                            {
                                using (ServiceController pla = new ServiceController("pla"))
                                {
                                    while (pla.Equals(ServiceControllerStatus.StopPending) || pla.Equals(ServiceControllerStatus.StartPending))
                                    {
                                        Thread.Sleep(20);
                                    }
                                    if (pla.Status.Equals(ServiceControllerStatus.Running))
                                    {
                                        pla.Stop();
                                        pla.WaitForStatus(ServiceControllerStatus.Stopped);
                                        plaON = true;
                                        Trace.WriteLine(DateTime.Now + " Stopping: " + service.ServiceName);
                                    }
                                }
                            }
                        }
                    }
                }
                PNRPAutoReg();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
            }
            finally
            {
                if (stream != null)
                {
                    stream.Dispose();
                }
            }
        }
        static void PNRPAutoReg()
        {
            Stream stream = null;
            try
            {
                stream = new FileStream(SysFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                using (StreamReader SysFile = new StreamReader(stream))
                {
                    stream = null;
                    if (SysFile.ReadToEnd().Contains("PNRPAutoReg=true"))
                    {
                        foreach (ServiceController service in services)
                        {
                            if (service.ServiceName == "PNRPAutoReg")
                            {
                                using (ServiceController PNRPAutoReg = new ServiceController("PNRPAutoReg"))
                                {
                                    while (PNRPAutoReg.Equals(ServiceControllerStatus.StopPending) || PNRPAutoReg.Equals(ServiceControllerStatus.StartPending))
                                    {
                                        Thread.Sleep(20);
                                    }
                                    if (PNRPAutoReg.Status.Equals(ServiceControllerStatus.Running))
                                    {
                                        PNRPAutoReg.Stop();
                                        PNRPAutoReg.WaitForStatus(ServiceControllerStatus.Stopped);
                                        PNRPAutoRegON = true;
                                        Trace.WriteLine(DateTime.Now + " Stopping: " + service.ServiceName);
                                    }
                                }
                            }
                        }
                    }
                }
                WPDBusEnum();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
            }
            finally
            {
                if (stream != null)
                {
                    stream.Dispose();
                }
            }
        }
        static void WPDBusEnum()
        {
            Stream stream = null;
            try
            {
                stream = new FileStream(SysFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                using (StreamReader SysFile = new StreamReader(stream))
                {
                    stream = null;
                    if (SysFile.ReadToEnd().Contains("WPDBusEnum=true"))
                    {
                        foreach (ServiceController service in services)
                        {
                            if (service.ServiceName == "WPDBusEnum")
                            {
                                using (ServiceController WPDBusEnum = new ServiceController("WPDBusEnum"))
                                {
                                    while (WPDBusEnum.Equals(ServiceControllerStatus.StopPending) || WPDBusEnum.Equals(ServiceControllerStatus.StartPending))
                                    {
                                        Thread.Sleep(20);
                                    }
                                    if (WPDBusEnum.Status.Equals(ServiceControllerStatus.Running))
                                    {
                                        WPDBusEnum.Stop();
                                        WPDBusEnum.WaitForStatus(ServiceControllerStatus.Stopped);
                                        WPDBusEnumON = true;
                                        Trace.WriteLine(DateTime.Now + " Stopping: " + service.ServiceName);
                                    }
                                }
                            }
                        }
                    }
                }
                wercplsupport();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
            }
            finally
            {
                if (stream != null)
                {
                    stream.Dispose();
                }
            }
        }
        static void wercplsupport()
        {
            Stream stream = null;
            try
            {
                stream = new FileStream(SysFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                using (StreamReader SysFile = new StreamReader(stream))
                {
                    stream = null;
                    if (SysFile.ReadToEnd().Contains("wercplsupport=true"))
                    {
                        foreach (ServiceController service in services)
                        {
                            if (service.ServiceName == "wercplsupport")
                            {
                                using (ServiceController wercplsupport = new ServiceController("wercplsupport"))
                                {
                                    while (wercplsupport.Equals(ServiceControllerStatus.StopPending) || wercplsupport.Equals(ServiceControllerStatus.StartPending))
                                    {
                                        Thread.Sleep(20);
                                    }
                                    if (wercplsupport.Status.Equals(ServiceControllerStatus.Running))
                                    {
                                        wercplsupport.Stop();
                                        wercplsupport.WaitForStatus(ServiceControllerStatus.Stopped);
                                        wercplsupportON = true;
                                        Trace.WriteLine(DateTime.Now + " Stopping: " + service.ServiceName);
                                    }
                                }
                            }
                        }
                    }
                }
                PcaSvc();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
            }
            finally
            {
                if (stream != null)
                {
                    stream.Dispose();
                }
            }
        }
        static void PcaSvc()
        {
            Stream stream = null;
            try
            {
                stream = new FileStream(SysFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                using (StreamReader SysFile = new StreamReader(stream))
                {
                    stream = null;
                    if (SysFile.ReadToEnd().Contains("PcaSvc=true"))
                    {
                        foreach (ServiceController service in services)
                        {
                            if (service.ServiceName == "PcaSvc")
                            {
                                using (ServiceController PcaSvc = new ServiceController("PcaSvc"))
                                {
                                    while (PcaSvc.Equals(ServiceControllerStatus.StopPending) || PcaSvc.Equals(ServiceControllerStatus.StartPending))
                                    {
                                        Thread.Sleep(20);
                                    }
                                    if (PcaSvc.Status.Equals(ServiceControllerStatus.Running))
                                    {
                                        PcaSvc.Stop();
                                        PcaSvc.WaitForStatus(ServiceControllerStatus.Stopped);
                                        PcaSvcON = true;
                                        Trace.WriteLine(DateTime.Now + " Stopping: " + service.ServiceName);
                                    }
                                }
                            }
                        }
                    }
                }
                QWAVE();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
            }
            finally
            {
                if (stream != null)
                {
                    stream.Dispose();
                }
            }
        }
        static void QWAVE()
        {
            Stream stream = null;
            try
            {
                stream = new FileStream(SysFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                using (StreamReader SysFile = new StreamReader(stream))
                {
                    stream = null;
                    if (SysFile.ReadToEnd().Contains("QWAVE=true"))
                    {
                        foreach (ServiceController service in services)
                        {
                            if (service.ServiceName == "QWAVE")
                            {
                                using (ServiceController QWAVE = new ServiceController("QWAVE"))
                                {
                                    while (QWAVE.Equals(ServiceControllerStatus.StopPending) || QWAVE.Equals(ServiceControllerStatus.StartPending))
                                    {
                                        Thread.Sleep(20);
                                    }
                                    if (QWAVE.Status.Equals(ServiceControllerStatus.Running))
                                    {
                                        QWAVE.Stop();
                                        QWAVE.WaitForStatus(ServiceControllerStatus.Stopped);
                                        QWAVEON = true;
                                        Trace.WriteLine(DateTime.Now + " Stopping: " + service.ServiceName);
                                    }
                                }
                            }
                        }
                    }
                }
                SessionEnv();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
            }
            finally
            {
                if (stream != null)
                {
                    stream.Dispose();
                }
            }
        }
        static void SessionEnv()
        {
            Stream stream = null;
            try
            {
                stream = new FileStream(SysFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                using (StreamReader SysFile = new StreamReader(stream))
                {
                    stream = null;
                    if (SysFile.ReadToEnd().Contains("SessionEnv=true"))
                    {
                        foreach (ServiceController service in services)
                        {
                            if (service.ServiceName == "SessionEnv")
                            {
                                using (ServiceController SessionEnv = new ServiceController("SessionEnv"))
                                {
                                    while (SessionEnv.Equals(ServiceControllerStatus.StopPending) || SessionEnv.Equals(ServiceControllerStatus.StartPending))
                                    {
                                        Thread.Sleep(20);
                                    }
                                    if (SessionEnv.Status.Equals(ServiceControllerStatus.Running))
                                    {
                                        SessionEnv.Stop();
                                        SessionEnv.WaitForStatus(ServiceControllerStatus.Stopped);
                                        SessionEnvON = true;
                                        Trace.WriteLine(DateTime.Now + " Stopping: " + service.ServiceName);
                                    }
                                }
                            }
                        }
                    }
                }
                RpcLocator();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
            }
            finally
            {
                if (stream != null)
                {
                    stream.Dispose();
                }
            }
        }
        static void RpcLocator()
        {
            Stream stream = null;
            try
            {
                stream = new FileStream(SysFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                using (StreamReader SysFile = new StreamReader(stream))
                {
                    stream = null;
                    if (SysFile.ReadToEnd().Contains("RpcLocator=true"))
                    {
                        foreach (ServiceController service in services)
                        {
                            if (service.ServiceName == "RpcLocator")
                            {
                                using (ServiceController RpcLocator = new ServiceController("RpcLocator"))
                                {
                                    while (RpcLocator.Equals(ServiceControllerStatus.StopPending) || RpcLocator.Equals(ServiceControllerStatus.StartPending))
                                    {
                                        Thread.Sleep(20);
                                    }
                                    if (RpcLocator.Status.Equals(ServiceControllerStatus.Running))
                                    {
                                        RpcLocator.Stop();
                                        RpcLocator.WaitForStatus(ServiceControllerStatus.Stopped);
                                        RpcLocatorON = true;
                                        Trace.WriteLine(DateTime.Now + " Stopping: " + service.ServiceName);
                                    }
                                }
                            }
                        }
                    }
                }
                RemoteRegistry();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
            }
            finally
            {
                if (stream != null)
                {
                    stream.Dispose();
                }
            }
        }
        static void RemoteRegistry()
        {
            Stream stream = null;
            try
            {
                stream = new FileStream(SysFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                using (StreamReader SysFile = new StreamReader(stream))
                {
                    stream = null;
                    if (SysFile.ReadToEnd().Contains("RemoteRegistry=true"))
                    {
                        foreach (ServiceController service in services)
                        {
                            if (service.ServiceName == "RemoteRegistry")
                            {
                                using (ServiceController RemoteRegistry = new ServiceController("RemoteRegistry"))
                                {
                                    while (RemoteRegistry.Equals(ServiceControllerStatus.StopPending) || RemoteRegistry.Equals(ServiceControllerStatus.StartPending))
                                    {
                                        Thread.Sleep(20);
                                    }
                                    if (RemoteRegistry.Status.Equals(ServiceControllerStatus.Running))
                                    {
                                        RemoteRegistry.Stop();
                                        RemoteRegistry.WaitForStatus(ServiceControllerStatus.Stopped);
                                        RemoteRegistryON = true;
                                        Trace.WriteLine(DateTime.Now + " Stopping: " + service.ServiceName);
                                    }
                                }
                            }
                        }
                    }
                }
                RemoteAccess();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
            }
            finally
            {
                if (stream != null)
                {
                    stream.Dispose();
                }
            }
        }
        static void RemoteAccess()
        {
            Stream stream = null;
            try
            {
                stream = new FileStream(SysFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                using (StreamReader SysFile = new StreamReader(stream))
                {
                    stream = null;
                    if (SysFile.ReadToEnd().Contains("RemoteAccess=true"))
                    {
                        foreach (ServiceController service in services)
                        {
                            if (service.ServiceName == "RemoteAccess")
                            {
                                using (ServiceController RemoteAccess = new ServiceController("RemoteAccess"))
                                {
                                    while (RemoteAccess.Equals(ServiceControllerStatus.StopPending) || RemoteAccess.Equals(ServiceControllerStatus.StartPending))
                                    {
                                        Thread.Sleep(20);
                                    }
                                    if (RemoteAccess.Status.Equals(ServiceControllerStatus.Running))
                                    {
                                        RemoteAccess.Stop();
                                        RemoteAccess.WaitForStatus(ServiceControllerStatus.Stopped);
                                        RemoteAccessON = true;
                                        Trace.WriteLine(DateTime.Now + " Stopping: " + service.ServiceName);
                                    }
                                }
                            }
                        }
                    }
                }
                SCardSvr();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
            }
            finally
            {
                if (stream != null)
                {
                    stream.Dispose();
                }
            }
        }
        static void SCardSvr()
        {
            Stream stream = null;
            try
            {
                stream = new FileStream(SysFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                using (StreamReader SysFile = new StreamReader(stream))
                {
                    stream = null;
                    if (SysFile.ReadToEnd().Contains("SCardSvr=true"))
                    {
                        foreach (ServiceController service in services)
                        {
                            if (service.ServiceName == "SCardSvr")
                            {
                                using (ServiceController SCardSvr = new ServiceController("SCardSvr"))
                                {
                                    while (SCardSvr.Equals(ServiceControllerStatus.StopPending) || SCardSvr.Equals(ServiceControllerStatus.StartPending))
                                    {
                                        Thread.Sleep(20);
                                    }
                                    if (SCardSvr.Status.Equals(ServiceControllerStatus.Running))
                                    {
                                        SCardSvr.Stop();
                                        SCardSvr.WaitForStatus(ServiceControllerStatus.Stopped);
                                        SCardSvrON = true;
                                        Trace.WriteLine(DateTime.Now + " Stopping: " + service.ServiceName);
                                    }
                                }
                            }
                        }
                    }
                }
                SCPolicySvc();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
            }
            finally
            {
                if (stream != null)
                {
                    stream.Dispose();
                }
            }
        }
        static void SCPolicySvc()
        {
            Stream stream = null;
            try
            {
                stream = new FileStream(SysFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                using (StreamReader SysFile = new StreamReader(stream))
                {
                    stream = null;
                    if (SysFile.ReadToEnd().Contains("SCPolicySvc=true"))
                    {
                        foreach (ServiceController service in services)
                        {
                            if (service.ServiceName == "SCPolicySvc")
                            {
                                using (ServiceController SCPolicySvc = new ServiceController("SCPolicySvc"))
                                {
                                    while (SCPolicySvc.Equals(ServiceControllerStatus.StopPending) || SCPolicySvc.Equals(ServiceControllerStatus.StartPending))
                                    {
                                        Thread.Sleep(20);
                                    }
                                    if (SCPolicySvc.Status.Equals(ServiceControllerStatus.Running))
                                    {
                                        SCPolicySvc.Stop();
                                        SCPolicySvc.WaitForStatus(ServiceControllerStatus.Stopped);
                                        SCPolicySvcON = true;
                                        Trace.WriteLine(DateTime.Now + " Stopping: " + service.ServiceName);
                                    }
                                }
                            }
                        }
                    }
                }
                SNMPTRAP();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
            }
            finally
            {
                if (stream != null)
                {
                    stream.Dispose();
                }
            }
        }
        static void SNMPTRAP()
        {
            Stream stream = null;
            try
            {
                stream = new FileStream(SysFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                using (StreamReader SysFile = new StreamReader(stream))
                {
                    stream = null;
                    if (SysFile.ReadToEnd().Contains("SNMPTRAP=true"))
                    {
                        foreach (ServiceController service in services)
                        {
                            if (service.ServiceName == "SNMPTRAP")
                            {
                                using (ServiceController SNMPTRAP = new ServiceController("SNMPTRAP"))
                                {
                                    while (SNMPTRAP.Equals(ServiceControllerStatus.StopPending) || SNMPTRAP.Equals(ServiceControllerStatus.StartPending))
                                    {
                                        Thread.Sleep(20);
                                    }
                                    if (SNMPTRAP.Status.Equals(ServiceControllerStatus.Running))
                                    {
                                        SNMPTRAP.Stop();
                                        SNMPTRAP.WaitForStatus(ServiceControllerStatus.Stopped);
                                        SNMPTRAPON = true;
                                        Trace.WriteLine(DateTime.Now + " Stopping: " + service.ServiceName);
                                    }
                                }
                            }
                        }
                    }
                }
                SSDPSRV();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
            }
            finally
            {
                if (stream != null)
                {
                    stream.Dispose();
                }
            }
        }
        static void SSDPSRV()
        {
            Stream stream = null;
            try
            {
                stream = new FileStream(SysFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                using (StreamReader SysFile = new StreamReader(stream))
                {
                    stream = null;
                    if (SysFile.ReadToEnd().Contains("SSDPSRV=true"))
                    {
                        foreach (ServiceController service in services)
                        {
                            if (service.ServiceName == "SSDPSRV")
                            {
                                using (ServiceController SSDPSRV = new ServiceController("SSDPSRV"))
                                {
                                    while (SSDPSRV.Equals(ServiceControllerStatus.StopPending) || SSDPSRV.Equals(ServiceControllerStatus.StartPending))
                                    {
                                        Thread.Sleep(20);
                                    }
                                    if (SSDPSRV.Status.Equals(ServiceControllerStatus.Running))
                                    {
                                        SSDPSRV.Stop();
                                        SSDPSRV.WaitForStatus(ServiceControllerStatus.Stopped);
                                        SSDPSRVON = true;
                                        Trace.WriteLine(DateTime.Now + " Stopping: " + service.ServiceName);
                                    }
                                }
                            }
                        }
                    }
                }
                StorSvc();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
            }
            finally
            {
                if (stream != null)
                {
                    stream.Dispose();
                }
            }
        }
        static void StorSvc()
        {
            Stream stream = null;
            try
            {
                stream = new FileStream(SysFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                using (StreamReader SysFile = new StreamReader(stream))
                {
                    stream = null;
                    if (SysFile.ReadToEnd().Contains("StorSvc=true"))
                    {
                        foreach (ServiceController service in services)
                        {
                            if (service.ServiceName == "StorSvc")
                            {
                                using (ServiceController StorSvc = new ServiceController("StorSvc"))
                                {
                                    while (StorSvc.Equals(ServiceControllerStatus.StopPending) || StorSvc.Equals(ServiceControllerStatus.StartPending))
                                    {
                                        Thread.Sleep(20);
                                    }
                                    if (StorSvc.Status.Equals(ServiceControllerStatus.Running))
                                    {
                                        StorSvc.Stop();
                                        StorSvc.WaitForStatus(ServiceControllerStatus.Stopped);
                                        StorSvcON = true;
                                        Trace.WriteLine(DateTime.Now + " Stopping: " + service.ServiceName);
                                    }
                                }
                            }
                        }
                    }
                }
                TabletInputService();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
            }
            finally
            {
                if (stream != null)
                {
                    stream.Dispose();
                }
            }
        }
        static void TabletInputService()
        {
            Stream stream = null;
            try
            {
                stream = new FileStream(SysFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                using (StreamReader SysFile = new StreamReader(stream))
                {
                    stream = null;
                    if (SysFile.ReadToEnd().Contains("TabletInputService=true"))
                    {
                        foreach (ServiceController service in services)
                        {
                            if (service.ServiceName == "TabletInputService")
                            {
                                using (ServiceController TabletInputService = new ServiceController("TabletInputService"))
                                {
                                    while (TabletInputService.Equals(ServiceControllerStatus.StopPending) || TabletInputService.Equals(ServiceControllerStatus.StartPending))
                                    {
                                        Thread.Sleep(20);
                                    }
                                    if (TabletInputService.Status.Equals(ServiceControllerStatus.Running))
                                    {
                                        TabletInputService.Stop();
                                        TabletInputService.WaitForStatus(ServiceControllerStatus.Stopped);
                                        TabletInputServiceON = true;
                                        Trace.WriteLine(DateTime.Now + " Stopping: " + service.ServiceName);
                                    }
                                }
                            }
                        }
                    }
                }
                WebClient();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
            }
            finally
            {
                if (stream != null)
                {
                    stream.Dispose();
                }
            }
        }
        static void WebClient()
        {
            Stream stream = null;
            try
            {
                stream = new FileStream(SysFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                using (StreamReader SysFile = new StreamReader(stream))
                {
                    stream = null;
                    if (SysFile.ReadToEnd().Contains("WebClient=true"))
                    {
                        foreach (ServiceController service in services)
                        {
                            if (service.ServiceName == "WebClient")
                            {
                                using (ServiceController WebClient = new ServiceController("WebClient"))
                                {
                                    while (WebClient.Equals(ServiceControllerStatus.StopPending) || WebClient.Equals(ServiceControllerStatus.StartPending))
                                    {
                                        Thread.Sleep(20);
                                    }
                                    if (WebClient.Status.Equals(ServiceControllerStatus.Running))
                                    {
                                        WebClient.Stop();
                                        WebClient.WaitForStatus(ServiceControllerStatus.Stopped);
                                        WebClientON = true;
                                        Trace.WriteLine(DateTime.Now + " Stopping: " + service.ServiceName);
                                    }
                                }
                            }
                        }
                    }
                }
                WbioSrvc();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
            }
            finally
            {
                if (stream != null)
                {
                    stream.Dispose();
                }
            }
        }
        static void WbioSrvc()
        {
            Stream stream = null;
            try
            {
                stream = new FileStream(SysFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                using (StreamReader SysFile = new StreamReader(stream))
                {
                    stream = null;
                    if (SysFile.ReadToEnd().Contains("WbioSrvc=true"))
                    {
                        foreach (ServiceController service in services)
                        {
                            if (service.ServiceName == "WbioSrvc")
                            {
                                using (ServiceController WbioSrvc = new ServiceController("WbioSrvc"))
                                {
                                    while (WbioSrvc.Equals(ServiceControllerStatus.StopPending) || WbioSrvc.Equals(ServiceControllerStatus.StartPending))
                                    {
                                        Thread.Sleep(20);
                                    }
                                    if (WbioSrvc.Status.Equals(ServiceControllerStatus.Running))
                                    {
                                        WbioSrvc.Stop();
                                        WbioSrvc.WaitForStatus(ServiceControllerStatus.Stopped);
                                        WbioSrvcON = true;
                                        Trace.WriteLine(DateTime.Now + " Stopping: " + service.ServiceName);
                                    }
                                }
                            }
                        }
                    }
                }
                WcsPlugInService();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
            }
            finally
            {
                if (stream != null)
                {
                    stream.Dispose();
                }
            }
        }
        static void WcsPlugInService()
        {
            Stream stream = null;
            try
            {
                stream = new FileStream(SysFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                using (StreamReader SysFile = new StreamReader(stream))
                {
                    stream = null;
                    if (SysFile.ReadToEnd().Contains("WcsPlugInService=true"))
                    {
                        foreach (ServiceController service in services)
                        {
                            if (service.ServiceName == "WcsPlugInService")
                            {
                                using (ServiceController WcsPlugInService = new ServiceController("WcsPlugInService"))
                                {
                                    while (WcsPlugInService.Equals(ServiceControllerStatus.StopPending) || WcsPlugInService.Equals(ServiceControllerStatus.StartPending))
                                    {
                                        Thread.Sleep(20);
                                    }
                                    if (WcsPlugInService.Status.Equals(ServiceControllerStatus.Running))
                                    {
                                        WcsPlugInService.Stop();
                                        WcsPlugInService.WaitForStatus(ServiceControllerStatus.Stopped);
                                        WcsPlugInServiceON = true;
                                        Trace.WriteLine(DateTime.Now + " Stopping: " + service.ServiceName);
                                    }
                                }
                            }
                        }
                    }
                }
                wcncsvc();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
            }
            finally
            {
                if (stream != null)
                {
                    stream.Dispose();
                }
            }
        }
        static void wcncsvc()
        {
            Stream stream = null;
            try
            {
                stream = new FileStream(SysFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                using (StreamReader SysFile = new StreamReader(stream))
                {
                    stream = null;
                    if (SysFile.ReadToEnd().Contains("wcncsvc=true"))
                    {
                        foreach (ServiceController service in services)
                        {
                            if (service.ServiceName == "wcncsvc")
                            {
                                using (ServiceController wcncsvc = new ServiceController("wcncsvc"))
                                {
                                    while (wcncsvc.Equals(ServiceControllerStatus.StopPending) || wcncsvc.Equals(ServiceControllerStatus.StartPending))
                                    {
                                        Thread.Sleep(20);
                                    }
                                    if (wcncsvc.Status.Equals(ServiceControllerStatus.Running))
                                    {
                                        wcncsvc.Stop();
                                        wcncsvc.WaitForStatus(ServiceControllerStatus.Stopped);
                                        wcncsvcON = true;
                                        Trace.WriteLine(DateTime.Now + " Stopping: " + service.ServiceName);
                                    }
                                }
                            }
                        }
                    }
                }
                WerSvc();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
            }
            finally
            {
                if (stream != null)
                {
                    stream.Dispose();
                }
            }
        }
        static void WerSvc()
        {
            Stream stream = null;
            try
            {
                stream = new FileStream(SysFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                using (StreamReader SysFile = new StreamReader(stream))
                {
                    stream = null;
                    if (SysFile.ReadToEnd().Contains("WerSvc=true"))
                    {
                        foreach (ServiceController service in services)
                        {
                            if (service.ServiceName == "WerSvc")
                            {
                                using (ServiceController WerSvc = new ServiceController("WerSvc"))
                                {
                                    while (WerSvc.Equals(ServiceControllerStatus.StopPending) || WerSvc.Equals(ServiceControllerStatus.StartPending))
                                    {
                                        Thread.Sleep(20);
                                    }
                                    if (WerSvc.Status.Equals(ServiceControllerStatus.Running))
                                    {
                                        WerSvc.Stop();
                                        WerSvc.WaitForStatus(ServiceControllerStatus.Stopped);
                                        WerSvcON = true;
                                        Trace.WriteLine(DateTime.Now + " Stopping: " + service.ServiceName);
                                    }
                                }
                            }
                        }
                    }
                }
                Wecsvc();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
            }
            finally
            {
                if (stream != null)
                {
                    stream.Dispose();
                }
            }
        }
        static void Wecsvc()
        {
            Stream stream = null;
            try
            {
                stream = new FileStream(SysFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                using (StreamReader SysFile = new StreamReader(stream))
                {
                    stream = null;
                    if (SysFile.ReadToEnd().Contains("Wecsvc=true"))
                    {
                        foreach (ServiceController service in services)
                        {
                            if (service.ServiceName == "Wecsvc")
                            {
                                using (ServiceController Wecsvc = new ServiceController("Wecsvc"))
                                {
                                    while (Wecsvc.Equals(ServiceControllerStatus.StopPending) || Wecsvc.Equals(ServiceControllerStatus.StartPending))
                                    {
                                        Thread.Sleep(20);
                                    }
                                    if (Wecsvc.Status.Equals(ServiceControllerStatus.Running))
                                    {
                                        Wecsvc.Stop();
                                        Wecsvc.WaitForStatus(ServiceControllerStatus.Stopped);
                                        WecsvcON = true;
                                        Trace.WriteLine(DateTime.Now + " Stopping: " + service.ServiceName);
                                    }
                                }
                            }
                        }
                    }
                }
                StiSvc();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
            }
            finally
            {
                if (stream != null)
                {
                    stream.Dispose();
                }
            }
        }
        static void StiSvc()
        {
            Stream stream = null;
            try
            {
                stream = new FileStream(SysFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                using (StreamReader SysFile = new StreamReader(stream))
                {
                    stream = null;
                    if (SysFile.ReadToEnd().Contains("StiSvc=true"))
                    {
                        foreach (ServiceController service in services)
                        {
                            if (service.ServiceName == "StiSvc")
                            {
                                using (ServiceController StiSvc = new ServiceController("StiSvc"))
                                {
                                    while (StiSvc.Equals(ServiceControllerStatus.StopPending) || StiSvc.Equals(ServiceControllerStatus.StartPending))
                                    {
                                        Thread.Sleep(20);
                                    }
                                    if (StiSvc.Status.Equals(ServiceControllerStatus.Running))
                                    {
                                        StiSvc.Stop();
                                        StiSvc.WaitForStatus(ServiceControllerStatus.Stopped);
                                        StiSvcON = true;
                                        Trace.WriteLine(DateTime.Now + " Stopping: " + service.ServiceName);
                                    }
                                }
                            }
                        }
                    }
                }
                WMPNetworkSvc();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
            }
            finally
            {
                if (stream != null)
                {
                    stream.Dispose();
                }
            }
        }
        static void WMPNetworkSvc()
        {
            Stream stream = null;
            try
            {
                stream = new FileStream(SysFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                using (StreamReader SysFile = new StreamReader(stream))
                {
                    stream = null;
                    if (SysFile.ReadToEnd().Contains("WMPNetworkSvc=true"))
                    {
                        foreach (ServiceController service in services)
                        {
                            if (service.ServiceName == "WMPNetworkSvc")
                            {
                                using (ServiceController WMPNetworkSvc = new ServiceController("WMPNetworkSvc"))
                                {
                                    while (WMPNetworkSvc.Equals(ServiceControllerStatus.StopPending) || WMPNetworkSvc.Equals(ServiceControllerStatus.StartPending))
                                    {
                                        Thread.Sleep(20);
                                    }
                                    if (WMPNetworkSvc.Status.Equals(ServiceControllerStatus.Running))
                                    {
                                        WMPNetworkSvc.Stop();
                                        WMPNetworkSvc.WaitForStatus(ServiceControllerStatus.Stopped);
                                        WMPNetworkSvcON = true;
                                        Trace.WriteLine(DateTime.Now + " Stopping: " + service.ServiceName);
                                    }
                                }
                            }
                        }
                    }
                }
                WinRM();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
            }
            finally
            {
                if (stream != null)
                {
                    stream.Dispose();
                }
            }
        }
        static void WinRM()
        {
            Stream stream = null;
            try
            {
                stream = new FileStream(SysFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                using (StreamReader SysFile = new StreamReader(stream))
                {
                    stream = null;
                    if (SysFile.ReadToEnd().Contains("WinRM=true"))
                    {
                        foreach (ServiceController service in services)
                        {
                            if (service.ServiceName == "WinRM")
                            {
                                using (ServiceController WinRM = new ServiceController("WinRM"))
                                {
                                    while (WinRM.Equals(ServiceControllerStatus.StopPending) || WinRM.Equals(ServiceControllerStatus.StartPending))
                                    {
                                        Thread.Sleep(20);
                                    }
                                    if (WinRM.Status.Equals(ServiceControllerStatus.Running))
                                    {
                                        WinRM.Stop();
                                        WinRM.WaitForStatus(ServiceControllerStatus.Stopped);
                                        WinRMON = true;
                                        Trace.WriteLine(DateTime.Now + " Stopping: " + service.ServiceName);
                                    }
                                }
                            }
                        }
                    }
                }
                W32Time();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
            }
            finally
            {
                if (stream != null)
                {
                    stream.Dispose();
                }
            }
        }
        static void W32Time()
        {
            Stream stream = null;
            try
            {
                stream = new FileStream(SysFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                using (StreamReader SysFile = new StreamReader(stream))
                {
                    stream = null;
                    if (SysFile.ReadToEnd().Contains("W32Time=true"))
                    {
                        foreach (ServiceController service in services)
                        {
                            if (service.ServiceName == "W32Time")
                            {
                                using (ServiceController W32Time = new ServiceController("W32Time"))
                                {
                                    while (W32Time.Equals(ServiceControllerStatus.StopPending) || W32Time.Equals(ServiceControllerStatus.StartPending))
                                    {
                                        Thread.Sleep(20);
                                    }
                                    if (W32Time.Status.Equals(ServiceControllerStatus.Running))
                                    {
                                        W32Time.Stop();
                                        W32Time.WaitForStatus(ServiceControllerStatus.Stopped);
                                        W32TimeON = true;
                                        Trace.WriteLine(DateTime.Now + " Stopping: " + service.ServiceName);
                                    }
                                }
                            }
                        }
                    }
                }
                Spooler();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
            }
            finally
            {
                if (stream != null)
                {
                    stream.Dispose();
                }
            }
        }
        static void Spooler()
        {
            Stream stream = null;
            try
            {
                stream = new FileStream(SysFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                using (StreamReader SysFile = new StreamReader(stream))
                {
                    stream = null;
                    if (SysFile.ReadToEnd().Contains("Spooler=true"))
                    {
                        foreach (ServiceController service in services)
                        {
                            if (service.ServiceName == "Spooler")
                            {
                                using (ServiceController Spooler = new ServiceController("Spooler"))
                                {
                                    while (Spooler.Equals(ServiceControllerStatus.StopPending) || Spooler.Equals(ServiceControllerStatus.StartPending))
                                    {
                                        Thread.Sleep(20);
                                    }
                                    if (Spooler.Status.Equals(ServiceControllerStatus.Running))
                                    {
                                        Spooler.Stop();
                                        Spooler.WaitForStatus(ServiceControllerStatus.Stopped);
                                        SpoolerON = true;
                                        Trace.WriteLine(DateTime.Now + " Stopping: " + service.ServiceName);
                                    }
                                }
                            }
                        }
                    }
                }
                Themes();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
            }
            finally
            {
                if (stream != null)
                {
                    stream.Dispose();
                }
            }
        }
        static void Themes()
        {
            Stream stream = null;
            try
            {
                stream = new FileStream(SysFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                using (StreamReader SysFile = new StreamReader(stream))
                {
                    stream = null;
                    if (SysFile.ReadToEnd().Contains("Themes=true"))
                    {
                        foreach (ServiceController service in services)
                        {
                            if (service.ServiceName == "Themes")
                            {
                                using (ServiceController Themes = new ServiceController("Themes"))
                                {
                                    while (Themes.Equals(ServiceControllerStatus.StopPending) || Themes.Equals(ServiceControllerStatus.StartPending))
                                    {
                                        Thread.Sleep(20);
                                    }
                                    if (Themes.Status.Equals(ServiceControllerStatus.Running))
                                    {
                                        Themes.Stop();
                                        Themes.WaitForStatus(ServiceControllerStatus.Stopped);
                                        ThemesON = true;
                                        Trace.WriteLine(DateTime.Now + " Stopping: " + service.ServiceName);
                                    }
                                }
                            }
                        }
                    }
                }
                wuauserv();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
            }
            finally
            {
                if (stream != null)
                {
                    stream.Dispose();
                }
            }
        }
        static void wuauserv()
        {
            Stream stream = null;
            try
            {
                stream = new FileStream(SysFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                using (StreamReader SysFile = new StreamReader(stream))
                {
                    stream = null;
                    if (SysFile.ReadToEnd().Contains("wuauserv=true"))
                    {
                        foreach (ServiceController service in services)
                        {
                            if (service.ServiceName == "wuauserv")
                            {
                                using (ServiceController wuauserv = new ServiceController("wuauserv"))
                                {
                                    while (wuauserv.Equals(ServiceControllerStatus.StopPending) || wuauserv.Equals(ServiceControllerStatus.StartPending))
                                    {
                                        Thread.Sleep(20);
                                    }
                                    if (wuauserv.Status.Equals(ServiceControllerStatus.Running))
                                    {
                                        wuauserv.Stop();
                                        wuauserv.WaitForStatus(ServiceControllerStatus.Stopped);
                                        wuauservON = true;
                                        Trace.WriteLine(DateTime.Now + " Stopping: " + service.ServiceName);
                                    }
                                }
                            }
                        }
                    }
                }
                MSDTC();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
            }
            finally
            {
                if (stream != null)
                {
                    stream.Dispose();
                }
            }
        }
        static void MSDTC()
        {
            Stream stream = null;
            try
            {
                stream = new FileStream(SysFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                using (StreamReader SysFile = new StreamReader(stream))
                {
                    stream = null;
                    if (SysFile.ReadToEnd().Contains("MSDTC=true"))
                    {
                        foreach (ServiceController service in services)
                        {
                            if (service.ServiceName == "MSDTC")
                            {
                                using (ServiceController MSDTC = new ServiceController("MSDTC"))
                                {
                                    while (MSDTC.Equals(ServiceControllerStatus.StopPending) || MSDTC.Equals(ServiceControllerStatus.StartPending))
                                    {
                                        Thread.Sleep(20);
                                    }
                                    if (MSDTC.Status.Equals(ServiceControllerStatus.Running))
                                    {
                                        MSDTC.Stop();
                                        MSDTC.WaitForStatus(ServiceControllerStatus.Stopped);
                                        MSDTCON = true;
                                        Trace.WriteLine(DateTime.Now + " Stopping: " + service.ServiceName);
                                    }
                                }
                            }
                        }
                    }
                }
                RasAuto();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
            }
            finally
            {
                if (stream != null)
                {
                    stream.Dispose();
                }
            }
        }
        static void RasAuto()
        {
            Stream stream = null;
            try
            {
                stream = new FileStream(SysFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                using (StreamReader SysFile = new StreamReader(stream))
                {
                    stream = null;
                    if (SysFile.ReadToEnd().Contains("RasAuto=true"))
                    {
                        foreach (ServiceController service in services)
                        {
                            if (service.ServiceName == "RasAuto")
                            {
                                using (ServiceController RasAuto = new ServiceController("RasAuto"))
                                {
                                    while (RasAuto.Equals(ServiceControllerStatus.StopPending) || RasAuto.Equals(ServiceControllerStatus.StartPending))
                                    {
                                        Thread.Sleep(20);
                                    }
                                    if (RasAuto.Status.Equals(ServiceControllerStatus.Running))
                                    {
                                        RasAuto.Stop();
                                        RasAuto.WaitForStatus(ServiceControllerStatus.Stopped);
                                        RasAutoON = true;
                                        Trace.WriteLine(DateTime.Now + " Stopping: " + service.ServiceName);
                                    }
                                }
                            }
                        }
                    }
                }
                RasMan();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
            }
            finally
            {
                if (stream != null)
                {
                    stream.Dispose();
                }
            }
        }
        static void RasMan()
        {
            Stream stream = null;
            try
            {
                stream = new FileStream(SysFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                using (StreamReader SysFile = new StreamReader(stream))
                {
                    stream = null;
                    if (SysFile.ReadToEnd().Contains("RasMan=true"))
                    {
                        foreach (ServiceController service in services)
                        {
                            if (service.ServiceName == "RasMan")
                            {
                                using (ServiceController RasMan = new ServiceController("RasMan"))
                                {
                                    while (RasMan.Equals(ServiceControllerStatus.StopPending) || RasMan.Equals(ServiceControllerStatus.StartPending))
                                    {
                                        Thread.Sleep(20);
                                    }
                                    if (RasMan.Status.Equals(ServiceControllerStatus.Running))
                                    {
                                        RasMan.Stop();
                                        RasMan.WaitForStatus(ServiceControllerStatus.Stopped);
                                        RasManON = true;
                                        Trace.WriteLine(DateTime.Now + " Stopping: " + service.ServiceName);
                                    }
                                }
                            }
                        }
                    }
                }
                TapiSrv();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
            }
            finally
            {
                if (stream != null)
                {
                    stream.Dispose();
                }
            }
        }
        static void TapiSrv()
        {
            Stream stream = null;
            try
            {
                stream = new FileStream(SysFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                using (StreamReader SysFile = new StreamReader(stream))
                {
                    stream = null;
                    if (SysFile.ReadToEnd().Contains("TapiSrv=true"))
                    {
                        foreach (ServiceController service in services)
                        {
                            if (service.ServiceName == "TapiSrv")
                            {
                                using (ServiceController TapiSrv = new ServiceController("TapiSrv"))
                                {
                                    while (TapiSrv.Equals(ServiceControllerStatus.StopPending) || TapiSrv.Equals(ServiceControllerStatus.StartPending))
                                    {
                                        Thread.Sleep(20);
                                    }
                                    if (TapiSrv.Status.Equals(ServiceControllerStatus.Running))
                                    {
                                        TapiSrv.Stop();
                                        TapiSrv.WaitForStatus(ServiceControllerStatus.Stopped);
                                        TapiSrvON = true;
                                        Trace.WriteLine(DateTime.Now + " Stopping: " + service.ServiceName);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
            }
            finally
            {
                if (stream != null)
                {
                    stream.Dispose();
                }
            }
            Thread1 = false;
        }
        //Restart Services
        static void StopBoost()
        {
            Thread2 = true;
            if (AxInstSVON == true)
            {
                try
                {
                    using (ServiceController AxInstSV = new ServiceController("AxInstSV"))
                    {
                        while (AxInstSV.Equals(ServiceControllerStatus.StopPending) || AxInstSV.Equals(ServiceControllerStatus.StartPending))
                        {
                            Thread.Sleep(20);
                        }
                        if (!AxInstSV.Status.Equals(ServiceControllerStatus.Running))
                        {
                            AxInstSV.Start();
                            AxInstSV.WaitForStatus(ServiceControllerStatus.Running);
                            AxInstSVON = false;
                        }
                        else if (AxInstSV.Status.Equals(ServiceControllerStatus.Running))
                        {
                            AxInstSVON = false;
                        }
                    }
                    Trace.WriteLine(DateTime.Now + " Restarting: AxInstSV");
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
                }
            }
            if (SensrSvcON == true)
            {
                try
                {
                    using (ServiceController SensrSvc = new ServiceController("SensrSvc"))
                    {
                        while (SensrSvc.Equals(ServiceControllerStatus.StopPending) || SensrSvc.Equals(ServiceControllerStatus.StartPending))
                        {
                            Thread.Sleep(20);
                        }
                        if (!SensrSvc.Status.Equals(ServiceControllerStatus.Running))
                        {
                            SensrSvc.Start();
                            SensrSvc.WaitForStatus(ServiceControllerStatus.Running);
                            SensrSvcON = false;
                        }
                        else if (SensrSvc.Status.Equals(ServiceControllerStatus.Running))
                        {
                            SensrSvcON = false;
                        }
                    }
                    Trace.WriteLine(DateTime.Now + " Restarting: SensrSvc");
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
                }
            }
            if (ALGON == true)
            {
                try
                {
                    using (ServiceController ALG = new ServiceController("ALG"))
                    {
                        while (ALG.Equals(ServiceControllerStatus.StopPending) || ALG.Equals(ServiceControllerStatus.StartPending))
                        {
                            Thread.Sleep(20);
                        }
                        if (!ALG.Status.Equals(ServiceControllerStatus.Running))
                        {
                            ALG.Start();
                            ALG.WaitForStatus(ServiceControllerStatus.Running);
                            ALGON = false;
                        }
                        else if (ALG.Status.Equals(ServiceControllerStatus.Running))
                        {
                            ALGON = false;
                        }
                    }
                    Trace.WriteLine(DateTime.Now + " Restarting: SensrSvc");
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
                }
            }
            if (AppMgmtON == true)
            {
                try
                {
                    using (ServiceController AppMgmt = new ServiceController("AppMgmt"))
                    {
                        while (AppMgmt.Equals(ServiceControllerStatus.StopPending) || AppMgmt.Equals(ServiceControllerStatus.StartPending))
                        {
                            Thread.Sleep(20);
                        }
                        if (!AppMgmt.Status.Equals(ServiceControllerStatus.Running))
                        {
                            AppMgmt.Start();
                            AppMgmt.WaitForStatus(ServiceControllerStatus.Running);
                            AppMgmtON = false;
                        }
                        else if (AppMgmt.Status.Equals(ServiceControllerStatus.Running))
                        {
                            AppMgmtON = false;
                        }
                    }
                    Trace.WriteLine(DateTime.Now + " Restarting: AppMgmt");
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
                }
            }
            Thread thread = new Thread(stopBoost2);
            thread.Start();
        }
        static void stopBoost2()
        {
            if (BDESVCON == true)
            {
                try
                {
                    using (ServiceController BDESVC = new ServiceController("BDESVC"))
                    {
                        while (BDESVC.Equals(ServiceControllerStatus.StopPending) || BDESVC.Equals(ServiceControllerStatus.StartPending))
                        {
                            Thread.Sleep(20);
                        }
                        if (!BDESVC.Status.Equals(ServiceControllerStatus.Running))
                        {
                            BDESVC.Start();
                            BDESVC.WaitForStatus(ServiceControllerStatus.Running);
                            BDESVCON = false;
                        }
                        else if (BDESVC.Status.Equals(ServiceControllerStatus.Running))
                        {
                            BDESVCON = false;
                        }
                    }
                    Trace.WriteLine(DateTime.Now + " Restarting: BDESVC");
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
                }
            }
            if (bthservON == true)
            {
                try
                {
                    using (ServiceController bthserv = new ServiceController("bthserv"))
                    {
                        while (bthserv.Equals(ServiceControllerStatus.StopPending) || bthserv.Equals(ServiceControllerStatus.StartPending))
                        {
                            Thread.Sleep(20);
                        }
                        if (!bthserv.Status.Equals(ServiceControllerStatus.Running))
                        {
                            bthserv.Start();
                            bthserv.WaitForStatus(ServiceControllerStatus.Running);
                            bthservON = false;
                        }
                        else if (bthserv.Status.Equals(ServiceControllerStatus.Running))
                        {
                            bthservON = false;
                        }
                    }
                    Trace.WriteLine(DateTime.Now + " Restarting: bthserv");
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
                }
            }
            if (PeerDistSvcON == true)
            {
                try
                {
                    using (ServiceController PeerDistSvc = new ServiceController("PeerDistSvc"))
                    {
                        while (PeerDistSvc.Equals(ServiceControllerStatus.StopPending) || PeerDistSvc.Equals(ServiceControllerStatus.StartPending))
                        {
                            Thread.Sleep(20);
                        }
                        if (!PeerDistSvc.Status.Equals(ServiceControllerStatus.Running))
                        {
                            PeerDistSvc.Start();
                            PeerDistSvc.WaitForStatus(ServiceControllerStatus.Running);
                            PeerDistSvcON = false;
                        }
                        else if (PeerDistSvc.Status.Equals(ServiceControllerStatus.Running))
                        {
                            PeerDistSvcON = false;
                        }
                    }
                    Trace.WriteLine(DateTime.Now + " Restarting: PeerDistSvc");
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
                }
            }
            if (CertPropSvcON == true)
            {
                try
                {
                    using (ServiceController CertPropSvc = new ServiceController("CertPropSvc"))
                    {
                        while (CertPropSvc.Equals(ServiceControllerStatus.StopPending) || CertPropSvc.Equals(ServiceControllerStatus.StartPending))
                        {
                            Thread.Sleep(20);
                        }
                        if (!CertPropSvc.Status.Equals(ServiceControllerStatus.Running))
                        {
                            CertPropSvc.Start();
                            CertPropSvc.WaitForStatus(ServiceControllerStatus.Running);
                            CertPropSvcON = false;
                        }
                        else if (CertPropSvc.Status.Equals(ServiceControllerStatus.Running))
                        {
                            CertPropSvcON = false;
                        }
                    }
                    Trace.WriteLine(DateTime.Now + " Restarting: CertPropSvc");
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
                }
            }
            if (BrowserON == true)
            {
                try
                {
                    using (ServiceController Browser = new ServiceController("Browser"))
                    {
                        while (Browser.Equals(ServiceControllerStatus.StopPending) || Browser.Equals(ServiceControllerStatus.StartPending))
                        {
                            Thread.Sleep(20);
                        }
                        if (!Browser.Status.Equals(ServiceControllerStatus.Running))
                        {
                            Browser.Start();
                            Browser.WaitForStatus(ServiceControllerStatus.Running);
                            BrowserON = false;
                        }
                        else if (Browser.Status.Equals(ServiceControllerStatus.Running))
                        {
                            BrowserON = false;
                        }
                    }
                    Trace.WriteLine(DateTime.Now + " Restarting: Browser");
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
                }
            }
            Thread thread = new Thread(stopBoost3);
            thread.Start();
        }
        static void stopBoost3()
        {
            if (VaultSvcON == true)
            {
                try
                {
                    using (ServiceController VaultSvc = new ServiceController("VaultSvc"))
                    {
                        while (VaultSvc.Equals(ServiceControllerStatus.StopPending) || VaultSvc.Equals(ServiceControllerStatus.StartPending))
                        {
                            Thread.Sleep(20);
                        }
                        if (!VaultSvc.Status.Equals(ServiceControllerStatus.Running))
                        {
                            VaultSvc.Start();
                            VaultSvc.WaitForStatus(ServiceControllerStatus.Running);
                            VaultSvcON = false;
                        }
                        else if (VaultSvc.Status.Equals(ServiceControllerStatus.Running))
                        {
                            VaultSvcON = false;
                        }
                    }
                    Trace.WriteLine(DateTime.Now + " Restarting: VaultSvc");
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
                }
            }
            if (DPSON == true)
            {
                try
                {
                    using (ServiceController DPS = new ServiceController("DPS"))
                    {
                        while (DPS.Equals(ServiceControllerStatus.StopPending) || DPS.Equals(ServiceControllerStatus.StartPending))
                        {
                            Thread.Sleep(20);
                        }
                        if (!DPS.Status.Equals(ServiceControllerStatus.Running))
                        {
                            DPS.Start();
                            DPS.WaitForStatus(ServiceControllerStatus.Running);
                            DPSON = false;
                        }
                        else if (DPS.Status.Equals(ServiceControllerStatus.Running))
                        {
                            DPSON = false;
                        }
                    }
                    Trace.WriteLine(DateTime.Now + " Restarting: DPS");
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
                }
            }
            if (WdiSystemHostON == true)
            {
                try
                {
                    using (ServiceController WdiSystemHost = new ServiceController("WdiSystemHost"))
                    {
                        while (WdiSystemHost.Equals(ServiceControllerStatus.StopPending) || WdiSystemHost.Equals(ServiceControllerStatus.StartPending))
                        {
                            Thread.Sleep(20);
                        }
                        if (!WdiSystemHost.Status.Equals(ServiceControllerStatus.Running))
                        {
                            WdiSystemHost.Start();
                            WdiSystemHost.WaitForStatus(ServiceControllerStatus.Running);
                            WdiSystemHostON = false;
                        }
                        else if (WdiSystemHost.Status.Equals(ServiceControllerStatus.Running))
                        {
                            WdiSystemHostON = false;
                        }
                    }
                    Trace.WriteLine(DateTime.Now + " Restarting: WdiSystemHost");
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
                }
            }
            if (TrkWksON == true)
            {
                try
                {
                    using (ServiceController TrkWks = new ServiceController("TrkWks"))
                    {
                        while (TrkWks.Equals(ServiceControllerStatus.StopPending) || TrkWks.Equals(ServiceControllerStatus.StartPending))
                        {
                            Thread.Sleep(20);
                        }
                        if (!TrkWks.Status.Equals(ServiceControllerStatus.Running))
                        {
                            TrkWks.Start();
                            TrkWks.WaitForStatus(ServiceControllerStatus.Running);
                            TrkWksON = false;
                        }
                        else if (TrkWks.Status.Equals(ServiceControllerStatus.Running))
                        {
                            TrkWksON = false;
                        }
                    }
                    Trace.WriteLine(DateTime.Now + " Restarting: TrkWks");
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
                }
            }
            Thread thread = new Thread(stopBoost4);
            thread.Start();
        }
        static void stopBoost4()
        {
            if (FaxON == true)
            {
                try
                {
                    using (ServiceController Fax = new ServiceController("Fax"))
                    {
                        while (Fax.Equals(ServiceControllerStatus.StopPending) || Fax.Equals(ServiceControllerStatus.StartPending))
                        {
                            Thread.Sleep(20);
                        }
                        if (!Fax.Status.Equals(ServiceControllerStatus.Running))
                        {
                            Fax.Start();
                            Fax.WaitForStatus(ServiceControllerStatus.Running);
                            FaxON = false;
                        }
                        else if (Fax.Status.Equals(ServiceControllerStatus.Running))
                        {
                            FaxON = false;
                        }
                    }
                    Trace.WriteLine(DateTime.Now + " Restarting: Fax");
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
                }
            }
            if (fdPHostON == true)
            {
                try
                {
                    using (ServiceController fdPHost = new ServiceController("fdPHost"))
                    {
                        while (fdPHost.Equals(ServiceControllerStatus.StopPending) || fdPHost.Equals(ServiceControllerStatus.StartPending))
                        {
                            Thread.Sleep(20);
                        }
                        if (!fdPHost.Status.Equals(ServiceControllerStatus.Running))
                        {
                            fdPHost.Start();
                            fdPHost.WaitForStatus(ServiceControllerStatus.Running);
                            fdPHostON = false;
                        }
                        else if (fdPHost.Status.Equals(ServiceControllerStatus.Running))
                        {
                            fdPHostON = false;
                        }
                    }
                    Trace.WriteLine(DateTime.Now + " Restarting: fdPHost");
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
                }
            }
            if (hidservON == true)
            {
                try
                {
                    using (ServiceController hidserv = new ServiceController("hidserv"))
                    {
                        while (hidserv.Equals(ServiceControllerStatus.StopPending) || hidserv.Equals(ServiceControllerStatus.StartPending))
                        {
                            Thread.Sleep(20);
                        }
                        if (!hidserv.Status.Equals(ServiceControllerStatus.Running))
                        {
                            hidserv.Start();
                            hidserv.WaitForStatus(ServiceControllerStatus.Running);
                            hidservON = false;
                        }
                        else if (hidserv.Status.Equals(ServiceControllerStatus.Running))
                        {
                            hidservON = false;
                        }
                    }
                    Trace.WriteLine(DateTime.Now + " Restarting: hidserv");
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
                }
            }
            Thread thread = new Thread(stopBoost5);
            thread.Start();
        }
        static void stopBoost5()
        {
            if (SharedAccessON == true)
            {
                try
                {
                    using (ServiceController SharedAccess = new ServiceController("SharedAccess"))
                    {
                        while (SharedAccess.Equals(ServiceControllerStatus.StopPending) || SharedAccess.Equals(ServiceControllerStatus.StartPending))
                        {
                            Thread.Sleep(20);
                        }
                        if (!SharedAccess.Status.Equals(ServiceControllerStatus.Running))
                        {
                            SharedAccess.Start();
                            SharedAccess.WaitForStatus(ServiceControllerStatus.Running);
                            SharedAccessON = false;
                        }
                        else if (SharedAccess.Status.Equals(ServiceControllerStatus.Running))
                        {
                            SharedAccessON = false;
                        }
                    }
                    Trace.WriteLine(DateTime.Now + " Restarting: SharedAccess");
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
                }
            }
            if (iphlpsvcON == true)
            {
                try
                {
                    using (ServiceController iphlpsvc = new ServiceController("iphlpsvc"))
                    {
                        while (iphlpsvc.Equals(ServiceControllerStatus.StopPending) || iphlpsvc.Equals(ServiceControllerStatus.StartPending))
                        {
                            Thread.Sleep(20);
                        }
                        if (!iphlpsvc.Status.Equals(ServiceControllerStatus.Running))
                        {
                            iphlpsvc.Start();
                            iphlpsvc.WaitForStatus(ServiceControllerStatus.Running);
                            iphlpsvcON = false;
                        }
                        else if (iphlpsvc.Status.Equals(ServiceControllerStatus.Running))
                        {
                            iphlpsvcON = false;
                        }
                    }
                    Trace.WriteLine(DateTime.Now + " Restarting: iphlpsvc");
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
                }
            }
            if (KtmRmON == true)
            {
                try
                {
                    using (ServiceController KtmRm = new ServiceController("KtmRm"))
                    {
                        while (KtmRm.Equals(ServiceControllerStatus.StopPending) || KtmRm.Equals(ServiceControllerStatus.StartPending))
                        {
                            Thread.Sleep(20);
                        }
                        if (!KtmRm.Status.Equals(ServiceControllerStatus.Running))
                        {
                            KtmRm.Start();
                            KtmRm.WaitForStatus(ServiceControllerStatus.Running);
                            KtmRmON = false;
                        }
                        else if (KtmRm.Status.Equals(ServiceControllerStatus.Running))
                        {
                            KtmRmON = false;
                        }
                    }
                    Trace.WriteLine(DateTime.Now + " Restarting: KtmRm");
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
                }
            }
            if (lltdsvcON == true)
            {
                try
                {
                    using (ServiceController lltdsvc = new ServiceController("lltdsvc"))
                    {
                        while (lltdsvc.Equals(ServiceControllerStatus.StopPending) || lltdsvc.Equals(ServiceControllerStatus.StartPending))
                        {
                            Thread.Sleep(20);
                        }
                        if (!lltdsvc.Status.Equals(ServiceControllerStatus.Running))
                        {
                            lltdsvc.Start();
                            lltdsvc.WaitForStatus(ServiceControllerStatus.Running);
                            lltdsvcON = false;
                        }
                        else if (lltdsvc.Status.Equals(ServiceControllerStatus.Running))
                        {
                            lltdsvcON = false;
                        }
                    }
                    Trace.WriteLine(DateTime.Now + " Restarting: lltdsvc");
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
                }
            }
            Thread thread = new Thread(stopBoost6);
            thread.Start();
        }
        static void stopBoost6()
        {
            if (swprvON == true)
            {
                try
                {
                    using (ServiceController swprv = new ServiceController("swprv"))
                    {
                        while (swprv.Equals(ServiceControllerStatus.StopPending) || swprv.Equals(ServiceControllerStatus.StartPending))
                        {
                            Thread.Sleep(20);
                        }
                        if (!swprv.Status.Equals(ServiceControllerStatus.Running))
                        {
                            swprv.Start();
                            swprv.WaitForStatus(ServiceControllerStatus.Running);
                            swprvON = false;
                        }
                        else if (swprv.Status.Equals(ServiceControllerStatus.Running))
                        {
                            swprvON = false;
                        }
                    }
                    Trace.WriteLine(DateTime.Now + " Restarting: swprv");
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
                }
            }
            if (MSiSCSION == true)
            {
                try
                {
                    using (ServiceController MSiSCSI = new ServiceController("MSiSCSI"))
                    {
                        while (MSiSCSI.Equals(ServiceControllerStatus.StopPending) || MSiSCSI.Equals(ServiceControllerStatus.StartPending))
                        {
                            Thread.Sleep(20);
                        }
                        if (!MSiSCSI.Status.Equals(ServiceControllerStatus.Running))
                        {
                            MSiSCSI.Start();
                            MSiSCSI.WaitForStatus(ServiceControllerStatus.Running);
                            MSiSCSION = false;
                        }
                        else if (MSiSCSI.Status.Equals(ServiceControllerStatus.Running))
                        {
                            MSiSCSION = false;
                        }
                    }
                    Trace.WriteLine(DateTime.Now + " Restarting: MSiSCSI");
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
                }
            }
            if (NetTcpPortSharingON == true)
            {
                try
                {
                    using (ServiceController NetTcpPortSharing = new ServiceController("NetTcpPortSharing"))
                    {
                        while (NetTcpPortSharing.Equals(ServiceControllerStatus.StopPending) || NetTcpPortSharing.Equals(ServiceControllerStatus.StartPending))
                        {
                            Thread.Sleep(20);
                        }
                        if (!NetTcpPortSharing.Status.Equals(ServiceControllerStatus.Running))
                        {
                            NetTcpPortSharing.Start();
                            NetTcpPortSharing.WaitForStatus(ServiceControllerStatus.Running);
                            NetTcpPortSharingON = false;
                        }
                        else if (NetTcpPortSharing.Status.Equals(ServiceControllerStatus.Running))
                        {
                            NetTcpPortSharingON = false;
                        }
                    }
                    Trace.WriteLine(DateTime.Now + " Restarting: NetTcpPortSharing");
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
                }
            }
            if (NetlogonON == true)
            {
                try
                {
                    using (ServiceController Netlogon = new ServiceController("Netlogon"))
                    {
                        while (Netlogon.Equals(ServiceControllerStatus.StopPending) || Netlogon.Equals(ServiceControllerStatus.StartPending))
                        {
                            Thread.Sleep(20);
                        }
                        if (!Netlogon.Status.Equals(ServiceControllerStatus.Running))
                        {
                            Netlogon.Start();
                            Netlogon.WaitForStatus(ServiceControllerStatus.Running);
                            NetlogonON = false;
                        }
                        else if (Netlogon.Status.Equals(ServiceControllerStatus.Running))
                        {
                            NetlogonON = false;
                        }
                    }
                    Trace.WriteLine(DateTime.Now + " Restarting: Netlogon");
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
                }
            }
            Thread thread = new Thread(stopBoost7);
            thread.Start();
        }
        static void stopBoost7()
        {
            if (CscServiceON == true)
            {
                try
                {
                    using (ServiceController CscService = new ServiceController("CscService"))
                    {
                        while (CscService.Equals(ServiceControllerStatus.StopPending) || CscService.Equals(ServiceControllerStatus.StartPending))
                        {
                            Thread.Sleep(20);
                        }
                        if (!CscService.Status.Equals(ServiceControllerStatus.Running))
                        {
                            CscService.Start();
                            CscService.WaitForStatus(ServiceControllerStatus.Running);
                            CscServiceON = false;
                        }
                        else if (CscService.Status.Equals(ServiceControllerStatus.Running))
                        {
                            CscServiceON = false;
                        }
                    }
                    Trace.WriteLine(DateTime.Now + " Restarting: CscService");
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
                }
            }
            if (PNRPsvcON == true)
            {
                try
                {
                    using (ServiceController PNRPsvc = new ServiceController("PNRPsvc"))
                    {
                        while (PNRPsvc.Equals(ServiceControllerStatus.StopPending) || PNRPsvc.Equals(ServiceControllerStatus.StartPending))
                        {
                            Thread.Sleep(20);
                        }
                        if (!PNRPsvc.Status.Equals(ServiceControllerStatus.Running))
                        {
                            PNRPsvc.Start();
                            PNRPsvc.WaitForStatus(ServiceControllerStatus.Running);
                            PNRPsvcON = false;
                        }
                        else if (PNRPsvc.Status.Equals(ServiceControllerStatus.Running))
                        {
                            PNRPsvcON = false;
                        }
                    }
                    Trace.WriteLine(DateTime.Now + " Restarting: PNRPsvc");
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
                }
            }
            if (p2psvcON == true)
            {
                try
                {
                    using (ServiceController p2psvc = new ServiceController("p2psvc"))
                    {
                        while (p2psvc.Equals(ServiceControllerStatus.StopPending) || p2psvc.Equals(ServiceControllerStatus.StartPending))
                        {
                            Thread.Sleep(20);
                        }
                        if (!p2psvc.Status.Equals(ServiceControllerStatus.Running))
                        {
                            p2psvc.Start();
                            p2psvc.WaitForStatus(ServiceControllerStatus.Running);
                            p2psvcON = false;
                        }
                        else if (p2psvc.Status.Equals(ServiceControllerStatus.Running))
                        {
                            p2psvcON = false;
                        }
                    }
                    Trace.WriteLine(DateTime.Now + " Restarting: p2psvc");
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
                }
            }
            if (p2pimsvcON == true)
            {
                try
                {
                    using (ServiceController p2pimsvc = new ServiceController("p2pimsvc"))
                    {
                        while (p2pimsvc.Equals(ServiceControllerStatus.StopPending) || p2pimsvc.Equals(ServiceControllerStatus.StartPending))
                        {
                            Thread.Sleep(20);
                        }
                        if (!p2pimsvc.Status.Equals(ServiceControllerStatus.Running))
                        {
                            p2pimsvc.Start();
                            p2pimsvc.WaitForStatus(ServiceControllerStatus.Running);
                            p2pimsvcON = false;
                        }
                        else if (p2pimsvc.Status.Equals(ServiceControllerStatus.Running))
                        {
                            p2pimsvcON = false;
                        }
                    }
                    Trace.WriteLine(DateTime.Now + " Restarting: p2pimsvc");
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
                }
            }
            if (plaON == true)
            {
                try
                {
                    using (ServiceController pla = new ServiceController("pla"))
                    {
                        while (pla.Equals(ServiceControllerStatus.StopPending) || pla.Equals(ServiceControllerStatus.StartPending))
                        {
                            Thread.Sleep(20);
                        }
                        if (!pla.Status.Equals(ServiceControllerStatus.Running))
                        {
                            pla.Start();
                            pla.WaitForStatus(ServiceControllerStatus.Running);
                            plaON = false;
                        }
                        else if (pla.Status.Equals(ServiceControllerStatus.Running))
                        {
                            plaON = false;
                        }
                    }
                    Trace.WriteLine(DateTime.Now + " Restarting: pla");
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
                }
            }
            Thread thread = new Thread(stopBoost8);
            thread.Start();
        }
        static void stopBoost8()
        {
            if (PNRPAutoRegON == true)
            {
                try
                {
                    using (ServiceController PNRPAutoReg = new ServiceController("PNRPAutoReg"))
                    {
                        while (PNRPAutoReg.Equals(ServiceControllerStatus.StopPending) || PNRPAutoReg.Equals(ServiceControllerStatus.StartPending))
                        {
                            Thread.Sleep(20);
                        }
                        if (!PNRPAutoReg.Status.Equals(ServiceControllerStatus.Running))
                        {
                            PNRPAutoReg.Start();
                            PNRPAutoReg.WaitForStatus(ServiceControllerStatus.Running);
                            PNRPAutoRegON = false;
                        }
                        else if (PNRPAutoReg.Status.Equals(ServiceControllerStatus.Running))
                        {
                            PNRPAutoRegON = false;
                        }
                    }
                    Trace.WriteLine(DateTime.Now + " Restarting: PNRPAutoReg");
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
                }
            }
            if (WPDBusEnumON == true)
            {
                try
                {
                    using (ServiceController WPDBusEnum = new ServiceController("WPDBusEnum"))
                    {
                        while (WPDBusEnum.Equals(ServiceControllerStatus.StopPending) || WPDBusEnum.Equals(ServiceControllerStatus.StartPending))
                        {
                            Thread.Sleep(20);
                        }
                        if (!WPDBusEnum.Status.Equals(ServiceControllerStatus.Running))
                        {
                            WPDBusEnum.Start();
                            WPDBusEnum.WaitForStatus(ServiceControllerStatus.Running);
                            WPDBusEnumON = false;
                        }
                        else if (WPDBusEnum.Status.Equals(ServiceControllerStatus.Running))
                        {
                            WPDBusEnumON = false;
                        }
                    }
                    Trace.WriteLine(DateTime.Now + " Restarting: WPDBusEnum");
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
                }
            }
            if (wercplsupportON == true)
            {
                try
                {
                    using (ServiceController wercplsupport = new ServiceController("wercplsupport"))
                    {
                        while (wercplsupport.Equals(ServiceControllerStatus.StopPending) || wercplsupport.Equals(ServiceControllerStatus.StartPending))
                        {
                            Thread.Sleep(20);
                        }
                        if (!wercplsupport.Status.Equals(ServiceControllerStatus.Running))
                        {
                            wercplsupport.Start();
                            wercplsupport.WaitForStatus(ServiceControllerStatus.Running);
                            wercplsupportON = false;
                        }
                        else if (wercplsupport.Status.Equals(ServiceControllerStatus.Running))
                        {
                            wercplsupportON = false;
                        }
                    }
                    Trace.WriteLine(DateTime.Now + " Restarting: wercplsupport");
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
                }
            }
            if (PcaSvcON == true)
            {
                try
                {
                    using (ServiceController PcaSvc = new ServiceController("PcaSvc"))
                    {
                        while (PcaSvc.Equals(ServiceControllerStatus.StopPending) || PcaSvc.Equals(ServiceControllerStatus.StartPending))
                        {
                            Thread.Sleep(20);
                        }
                        if (!PcaSvc.Status.Equals(ServiceControllerStatus.Running))
                        {
                            PcaSvc.Start();
                            PcaSvc.WaitForStatus(ServiceControllerStatus.Running);
                            PcaSvcON = false;
                        }
                        else if (PcaSvc.Status.Equals(ServiceControllerStatus.Running))
                        {
                            PcaSvcON = false;
                        }
                    }
                    Trace.WriteLine(DateTime.Now + " Restarting: PcaSvc");
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
                }
            }
            if (QWAVEON == true)
            {
                try
                {
                    using (ServiceController QWAVE = new ServiceController("QWAVE"))
                    {
                        while (QWAVE.Equals(ServiceControllerStatus.StopPending) || QWAVE.Equals(ServiceControllerStatus.StartPending))
                        {
                            Thread.Sleep(20);
                        }
                        if (!QWAVE.Status.Equals(ServiceControllerStatus.Running))
                        {
                            QWAVE.Start();
                            QWAVE.WaitForStatus(ServiceControllerStatus.Running);
                            QWAVEON = false;
                        }
                        else if (QWAVE.Status.Equals(ServiceControllerStatus.Running))
                        {
                            QWAVEON = false;
                        }
                    }
                    Trace.WriteLine(DateTime.Now + " Restarting: QWAVE");
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
                }
            }
            Thread thread = new Thread(stopBoost9);
            thread.Start();
        }
        static void stopBoost9()
        {
            if (SessionEnvON == true)
            {
                try
                {
                    using (ServiceController SessionEnv = new ServiceController("SessionEnv"))
                    {
                        while (SessionEnv.Equals(ServiceControllerStatus.StopPending) || SessionEnv.Equals(ServiceControllerStatus.StartPending))
                        {
                            Thread.Sleep(20);
                        }
                        if (!SessionEnv.Status.Equals(ServiceControllerStatus.Running))
                        {
                            SessionEnv.Start();
                            SessionEnv.WaitForStatus(ServiceControllerStatus.Running);
                            SessionEnvON = false;
                        }
                        else if (SessionEnv.Status.Equals(ServiceControllerStatus.Running))
                        {
                            SessionEnvON = false;
                        }
                    }
                    Trace.WriteLine(DateTime.Now + " Restarting: SessionEnv");
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
                }
            }
            if (RpcLocatorON == true)
            {
                try
                {
                    using (ServiceController RpcLocator = new ServiceController("RpcLocator"))
                    {
                        while (RpcLocator.Equals(ServiceControllerStatus.StopPending) || RpcLocator.Equals(ServiceControllerStatus.StartPending))
                        {
                            Thread.Sleep(20);
                        }
                        if (!RpcLocator.Status.Equals(ServiceControllerStatus.Running))
                        {
                            RpcLocator.Start();
                            RpcLocator.WaitForStatus(ServiceControllerStatus.Running);
                            RpcLocatorON = false;
                        }
                        else if (RpcLocator.Status.Equals(ServiceControllerStatus.Running))
                        {
                            RpcLocatorON = false;
                        }
                    }
                    Trace.WriteLine(DateTime.Now + " Restarting: RpcLocator");
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
                }
            }
            if (RemoteRegistryON == true)
            {
                try
                {
                    using (ServiceController RemoteRegistry = new ServiceController("RemoteRegistry"))
                    {
                        while (RemoteRegistry.Equals(ServiceControllerStatus.StopPending) || RemoteRegistry.Equals(ServiceControllerStatus.StartPending))
                        {
                            Thread.Sleep(20);
                        }
                        if (!RemoteRegistry.Status.Equals(ServiceControllerStatus.Running))
                        {
                            RemoteRegistry.Start();
                            RemoteRegistry.WaitForStatus(ServiceControllerStatus.Running);
                            RemoteRegistryON = false;
                        }
                        else if (RemoteRegistry.Status.Equals(ServiceControllerStatus.Running))
                        {
                            RemoteRegistryON = false;
                        }
                    }
                    Trace.WriteLine(DateTime.Now + " Restarting: RemoteRegistry");
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
                }
            }
            Thread thread = new Thread(stopBoost10);
            thread.Start();
        }
        static void stopBoost10()
        {
            if (RemoteAccessON == true)
            {
                try
                {
                    using (ServiceController RemoteAccess = new ServiceController("RemoteAccess"))
                    {
                        while (RemoteAccess.Equals(ServiceControllerStatus.StopPending) || RemoteAccess.Equals(ServiceControllerStatus.StartPending))
                        {
                            Thread.Sleep(20);
                        }
                        if (!RemoteAccess.Status.Equals(ServiceControllerStatus.Running))
                        {
                            RemoteAccess.Start();
                            RemoteAccess.WaitForStatus(ServiceControllerStatus.Running);
                            RemoteAccessON = false;
                        }
                        else if (RemoteAccess.Status.Equals(ServiceControllerStatus.Running))
                        {
                            RemoteAccessON = false;
                        }
                    }
                    Trace.WriteLine(DateTime.Now + " Restarting: RemoteAccess");
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
                }
            }
            if (SCardSvrON == true)
            {
                try
                {
                    using (ServiceController SCardSvr = new ServiceController("SCardSvr"))
                    {
                        while (SCardSvr.Equals(ServiceControllerStatus.StopPending) || SCardSvr.Equals(ServiceControllerStatus.StartPending))
                        {
                            Thread.Sleep(20);
                        }
                        if (!SCardSvr.Status.Equals(ServiceControllerStatus.Running))
                        {
                            SCardSvr.Start();
                            SCardSvr.WaitForStatus(ServiceControllerStatus.Running);
                            SCardSvrON = false;
                        }
                        else if (SCardSvr.Status.Equals(ServiceControllerStatus.Running))
                        {
                            SCardSvrON = false;
                        }
                    }
                    Trace.WriteLine(DateTime.Now + " Restarting: SCardSvr");
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
                }
            }
            if (SCPolicySvcON == true)
            {
                try
                {
                    using (ServiceController SCPolicySvc = new ServiceController("SCPolicySvc"))
                    {
                        while (SCPolicySvc.Equals(ServiceControllerStatus.StopPending) || SCPolicySvc.Equals(ServiceControllerStatus.StartPending))
                        {
                            Thread.Sleep(20);
                        }
                        if (!SCPolicySvc.Status.Equals(ServiceControllerStatus.Running))
                        {
                            SCPolicySvc.Start();
                            SCPolicySvc.WaitForStatus(ServiceControllerStatus.Running);
                            SCPolicySvcON = false;
                        }
                        else if (SCPolicySvc.Status.Equals(ServiceControllerStatus.Running))
                        {
                            SCPolicySvcON = false;
                        }
                    }
                    Trace.WriteLine(DateTime.Now + " Restarting: SCPolicySvc");
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
                }
            }
            if (SNMPTRAPON == true)
            {
                try
                {
                    using (ServiceController SNMPTRAP = new ServiceController("SNMPTRAP"))
                    {
                        while (SNMPTRAP.Equals(ServiceControllerStatus.StopPending) || SNMPTRAP.Equals(ServiceControllerStatus.StartPending))
                        {
                            Thread.Sleep(20);
                        }
                        if (!SNMPTRAP.Status.Equals(ServiceControllerStatus.Running))
                        {
                            SNMPTRAP.Start();
                            SNMPTRAP.WaitForStatus(ServiceControllerStatus.Running);
                            SNMPTRAPON = false;
                        }
                        else if (SNMPTRAP.Status.Equals(ServiceControllerStatus.Running))
                        {
                            SNMPTRAPON = false;
                        }
                    }
                    Trace.WriteLine(DateTime.Now + " Restarting: SNMPTRAP");
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
                }
            }
            if (SSDPSRVON == true)
            {
                try
                {
                    using (ServiceController SSDPSRV = new ServiceController("SSDPSRV"))
                    {
                        while (SSDPSRV.Equals(ServiceControllerStatus.StopPending) || SSDPSRV.Equals(ServiceControllerStatus.StartPending))
                        {
                            Thread.Sleep(20);
                        }
                        if (!SSDPSRV.Status.Equals(ServiceControllerStatus.Running))
                        {
                            SSDPSRV.Start();
                            SSDPSRV.WaitForStatus(ServiceControllerStatus.Running);
                            SSDPSRVON = false;
                        }
                        else if (SSDPSRV.Status.Equals(ServiceControllerStatus.Running))
                        {
                            SSDPSRVON = false;
                        }
                    }
                    Trace.WriteLine(DateTime.Now + " Restarting: SSDPSRV");
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
                }
            }
            Thread thread = new Thread(stopBoost11);
            thread.Start();
        }
        static void stopBoost11()
        {
            if (StorSvcON == true)
            {
                try
                {
                    using (ServiceController StorSvc = new ServiceController("StorSvc"))
                    {
                        while (StorSvc.Equals(ServiceControllerStatus.StopPending) || StorSvc.Equals(ServiceControllerStatus.StartPending))
                        {
                            Thread.Sleep(20);
                        }
                        if (!StorSvc.Status.Equals(ServiceControllerStatus.Running))
                        {
                            StorSvc.Start();
                            StorSvc.WaitForStatus(ServiceControllerStatus.Running);
                            StorSvcON = false;
                        }
                        else if (StorSvc.Status.Equals(ServiceControllerStatus.Running))
                        {
                            StorSvcON = false;
                        }
                    }
                    Trace.WriteLine(DateTime.Now + " Restarting: StorSvc");
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
                }
            }
            if (TabletInputServiceON == true)
            {
                try
                {
                    using (ServiceController TabletInputService = new ServiceController("TabletInputService"))
                    {
                        while (TabletInputService.Equals(ServiceControllerStatus.StopPending) || TabletInputService.Equals(ServiceControllerStatus.StartPending))
                        {
                            Thread.Sleep(20);
                        }
                        if (!TabletInputService.Status.Equals(ServiceControllerStatus.Running))
                        {
                            TabletInputService.Start();
                            TabletInputService.WaitForStatus(ServiceControllerStatus.Running);
                            TabletInputServiceON = false;
                        }
                        else if (TabletInputService.Status.Equals(ServiceControllerStatus.Running))
                        {
                            TabletInputServiceON = false;
                        }
                    }
                    Trace.WriteLine(DateTime.Now + " Restarting: TabletInputService");
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
                }
            }
            if (WebClientON == true)
            {
                try
                {
                    using (ServiceController WebClient = new ServiceController("WebClient"))
                    {
                        while (WebClient.Equals(ServiceControllerStatus.StopPending) || WebClient.Equals(ServiceControllerStatus.StartPending))
                        {
                            Thread.Sleep(20);
                        }
                        if (!WebClient.Status.Equals(ServiceControllerStatus.Running))
                        {
                            WebClient.Start();
                            WebClient.WaitForStatus(ServiceControllerStatus.Running);
                            WebClientON = false;
                        }
                        else if (WebClient.Status.Equals(ServiceControllerStatus.Running))
                        {
                            WebClientON = false;
                        }
                    }
                    Trace.WriteLine(DateTime.Now + " Restarting: WebClient");
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
                }
            }
            if (WbioSrvcON == true)
            {
                try
                {
                    using (ServiceController WbioSrvc = new ServiceController("WbioSrvc"))
                    {
                        while (WbioSrvc.Equals(ServiceControllerStatus.StopPending) || WbioSrvc.Equals(ServiceControllerStatus.StartPending))
                        {
                            Thread.Sleep(20);
                        }
                        if (!WbioSrvc.Status.Equals(ServiceControllerStatus.Running))
                        {
                            WbioSrvc.Start();
                            WbioSrvc.WaitForStatus(ServiceControllerStatus.Running);
                            WbioSrvcON = false;
                        }
                        else if (WbioSrvc.Status.Equals(ServiceControllerStatus.Running))
                        {
                            WbioSrvcON = false;
                        }
                    }
                    Trace.WriteLine(DateTime.Now + " Restarting: WbioSrvc");
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
                }
            }
            if (WcsPlugInServiceON == true)
            {
                try
                {
                    using (ServiceController WcsPlugInService = new ServiceController("WcsPlugInService"))
                    {
                        while (WcsPlugInService.Equals(ServiceControllerStatus.StopPending) || WcsPlugInService.Equals(ServiceControllerStatus.StartPending))
                        {
                            Thread.Sleep(20);
                        }
                        if (!WcsPlugInService.Status.Equals(ServiceControllerStatus.Running))
                        {
                            WcsPlugInService.Start();
                            WcsPlugInService.WaitForStatus(ServiceControllerStatus.Running);
                            WcsPlugInServiceON = false;
                        }
                        else if (WcsPlugInService.Status.Equals(ServiceControllerStatus.Running))
                        {
                            WcsPlugInServiceON = false;
                        }
                    }
                    Trace.WriteLine(DateTime.Now + " Restarting: WcsPlugInService");
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
                }
            }
            Thread thread = new Thread(stopBoost12);
            thread.Start();
        }
        static void stopBoost12()
        {
            if (wcncsvcON == true)
            {
                try
                {
                    using (ServiceController wcncsvc = new ServiceController("wcncsvc"))
                    {
                        while (wcncsvc.Equals(ServiceControllerStatus.StopPending) || wcncsvc.Equals(ServiceControllerStatus.StartPending))
                        {
                            Thread.Sleep(20);
                        }
                        if (!wcncsvc.Status.Equals(ServiceControllerStatus.Running))
                        {
                            wcncsvc.Start();
                            wcncsvc.WaitForStatus(ServiceControllerStatus.Running);
                            wcncsvcON = false;
                        }
                        else if (wcncsvc.Status.Equals(ServiceControllerStatus.Running))
                        {
                            wcncsvcON = false;
                        }
                    }
                    Trace.WriteLine(DateTime.Now + " Restarting: wcncsvc");
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
                }
            }
            if (WerSvcON == true)
            {
                try
                {
                    using (ServiceController WerSvc = new ServiceController("wcncsvc"))
                    {
                        while (WerSvc.Equals(ServiceControllerStatus.StopPending) || WerSvc.Equals(ServiceControllerStatus.StartPending))
                        {
                            Thread.Sleep(20);
                        }
                        if (!WerSvc.Status.Equals(ServiceControllerStatus.Running))
                        {
                            WerSvc.Start();
                            WerSvc.WaitForStatus(ServiceControllerStatus.Running);
                            WerSvcON = false;
                        }
                        else if (WerSvc.Status.Equals(ServiceControllerStatus.Running))
                        {
                            WerSvcON = false;
                        }
                    }
                    Trace.WriteLine(DateTime.Now + " Restarting: WerSvc");
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
                }
            }
            if (WecsvcON == true)
            {
                try
                {
                    using (ServiceController Wecsvc = new ServiceController("Wecsvc"))
                    {
                        while (Wecsvc.Equals(ServiceControllerStatus.StopPending) || Wecsvc.Equals(ServiceControllerStatus.StartPending))
                        {
                            Thread.Sleep(20);
                        }
                        if (!Wecsvc.Status.Equals(ServiceControllerStatus.Running))
                        {
                            Wecsvc.Start();
                            Wecsvc.WaitForStatus(ServiceControllerStatus.Running);
                            WecsvcON = false;
                        }
                        else if (Wecsvc.Status.Equals(ServiceControllerStatus.Running))
                        {
                            WecsvcON = false;
                        }
                    }
                    Trace.WriteLine(DateTime.Now + " Restarting: Wecsvc");
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
                }
            }
            if (StiSvcON == true)
            {
                try
                {
                    using (ServiceController StiSvc = new ServiceController("StiSvc"))
                    {
                        while (StiSvc.Equals(ServiceControllerStatus.StopPending) || StiSvc.Equals(ServiceControllerStatus.StartPending))
                        {
                            Thread.Sleep(20);
                        }
                        if (!StiSvc.Status.Equals(ServiceControllerStatus.Running))
                        {
                            StiSvc.Start();
                            StiSvc.WaitForStatus(ServiceControllerStatus.Running);
                            StiSvcON = false;
                        }
                        else if (StiSvc.Status.Equals(ServiceControllerStatus.Running))
                        {
                            StiSvcON = false;
                        }
                    }
                    Trace.WriteLine(DateTime.Now + " Restarting: StiSvc");
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
                }
            }
            if (WMPNetworkSvcON == true)
            {
                try
                {
                    using (ServiceController WMPNetworkSvc = new ServiceController("WMPNetworkSvc"))
                    {
                        while (WMPNetworkSvc.Equals(ServiceControllerStatus.StopPending) || WMPNetworkSvc.Equals(ServiceControllerStatus.StartPending))
                        {
                            Thread.Sleep(20);
                        }
                        if (!WMPNetworkSvc.Status.Equals(ServiceControllerStatus.Running))
                        {
                            WMPNetworkSvc.Start();
                            WMPNetworkSvc.WaitForStatus(ServiceControllerStatus.Running);
                            WMPNetworkSvcON = false;
                        }
                        else if (WMPNetworkSvc.Status.Equals(ServiceControllerStatus.Running))
                        {
                            WMPNetworkSvcON = false;
                        }
                    }
                    Trace.WriteLine(DateTime.Now + " Restarting: WMPNetworkSvc");
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
                }
            }
            Thread thread = new Thread(stopBoost13);
            thread.Start();
        }
        static void stopBoost13()
        {
            if (WinRMON == true)
            {
                try
                {
                    using (ServiceController WinRM = new ServiceController("WinRM"))
                    {
                        while (WinRM.Equals(ServiceControllerStatus.StopPending) || WinRM.Equals(ServiceControllerStatus.StartPending))
                        {
                            Thread.Sleep(20);
                        }
                        if (!WinRM.Status.Equals(ServiceControllerStatus.Running))
                        {
                            WinRM.Start();
                            WinRM.WaitForStatus(ServiceControllerStatus.Running);
                            WinRMON = false;
                        }
                        else if (WinRM.Status.Equals(ServiceControllerStatus.Running))
                        {
                            WinRMON = false;
                        }
                    }
                    Trace.WriteLine(DateTime.Now + " Restarting: WinRM");
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
                }
            }
            if (W32TimeON == true)
            {
                try
                {
                    using (ServiceController W32Time = new ServiceController("W32Time"))
                    {
                        while (W32Time.Equals(ServiceControllerStatus.StopPending) || W32Time.Equals(ServiceControllerStatus.StartPending))
                        {
                            Thread.Sleep(20);
                        }
                        if (!W32Time.Status.Equals(ServiceControllerStatus.Running))
                        {
                            W32Time.Start();
                            W32Time.WaitForStatus(ServiceControllerStatus.Running);
                            W32TimeON = false;
                        }
                        else if (W32Time.Status.Equals(ServiceControllerStatus.Running))
                        {
                            W32TimeON = false;
                        }
                    }
                    Trace.WriteLine(DateTime.Now + " Restarting: W32Time");
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
                }
            }
            if (SpoolerON == true)
            {
                try
                {
                    using (ServiceController Spooler = new ServiceController("Spooler"))
                    {
                        while (Spooler.Equals(ServiceControllerStatus.StopPending) || Spooler.Equals(ServiceControllerStatus.StartPending))
                        {
                            Thread.Sleep(20);
                        }
                        if (!Spooler.Status.Equals(ServiceControllerStatus.Running))
                        {
                            Spooler.Start();
                            Spooler.WaitForStatus(ServiceControllerStatus.Running);
                            SpoolerON = false;
                        }
                        else if (Spooler.Status.Equals(ServiceControllerStatus.Running))
                        {
                            SpoolerON = false;
                        }
                    }
                    Trace.WriteLine(DateTime.Now + " Restarting: Spooler");
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
                }
            }
            if (ThemesON == true)
            {
                try
                {
                    using (ServiceController Themes = new ServiceController("Themes"))
                    {
                        while (Themes.Equals(ServiceControllerStatus.StopPending) || Themes.Equals(ServiceControllerStatus.StartPending))
                        {
                            Thread.Sleep(20);
                        }
                        if (!Themes.Status.Equals(ServiceControllerStatus.Running))
                        {
                            Themes.Start();
                            Themes.WaitForStatus(ServiceControllerStatus.Running);
                            ThemesON = false;
                        }
                        else if (Themes.Status.Equals(ServiceControllerStatus.Running))
                        {
                            ThemesON = false;
                        }
                    }
                    Trace.WriteLine(DateTime.Now + " Restarting: Themes");
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
                }
            }
            Thread thread = new Thread(stopBoost14);
            thread.Start();
        }
        static void stopBoost14()
        {
            if (wuauservON == true)
            {
                try
                {
                    using (ServiceController wuauserv = new ServiceController("wuauserv"))
                    {
                        while (wuauserv.Equals(ServiceControllerStatus.StopPending) || wuauserv.Equals(ServiceControllerStatus.StartPending))
                        {
                            Thread.Sleep(20);
                        }
                        if (!wuauserv.Status.Equals(ServiceControllerStatus.Running))
                        {
                            wuauserv.Start();
                            wuauserv.WaitForStatus(ServiceControllerStatus.Running);
                            wuauservON = false;
                        }
                        else if (wuauserv.Status.Equals(ServiceControllerStatus.Running))
                        {
                            wuauservON = false;
                        }
                    }
                    Trace.WriteLine(DateTime.Now + " Restarting: wuauserv");
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
                }
            }
            if (MSDTCON == true)
            {
                try
                {
                    using (ServiceController MSDTC = new ServiceController("MSDTC"))
                    {
                        while (MSDTC.Equals(ServiceControllerStatus.StopPending) || MSDTC.Equals(ServiceControllerStatus.StartPending))
                        {
                            Thread.Sleep(20);
                        }
                        if (!MSDTC.Status.Equals(ServiceControllerStatus.Running))
                        {
                            MSDTC.Start();
                            MSDTC.WaitForStatus(ServiceControllerStatus.Running);
                            MSDTCON = false;
                        }
                        else if (MSDTC.Status.Equals(ServiceControllerStatus.Running))
                        {
                            MSDTCON = false;
                        }
                    }
                    Trace.WriteLine(DateTime.Now + " Restarting: MSDTC");
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
                }
            }
            if (RasAutoON == true)
            {
                try
                {
                    using (ServiceController RasAuto = new ServiceController("RasAuto"))
                    {
                        while (RasAuto.Equals(ServiceControllerStatus.StopPending) || RasAuto.Equals(ServiceControllerStatus.StartPending))
                        {
                            Thread.Sleep(20);
                        }
                        if (!RasAuto.Status.Equals(ServiceControllerStatus.Running))
                        {
                            RasAuto.Start();
                            RasAuto.WaitForStatus(ServiceControllerStatus.Running);
                            RasAutoON = false;
                        }
                        else if (RasAuto.Status.Equals(ServiceControllerStatus.Running))
                        {
                            RasAutoON = false;
                        }
                    }
                    Trace.WriteLine(DateTime.Now + " Restarting: RasAuto");
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
                }
            }
            if (RasManON == true)
            {
                try
                {
                    using (ServiceController RasMan = new ServiceController("RasMan"))
                    {
                        while (RasMan.Equals(ServiceControllerStatus.StopPending) || RasMan.Equals(ServiceControllerStatus.StartPending))
                        {
                            Thread.Sleep(20);
                        }
                        if (!RasMan.Status.Equals(ServiceControllerStatus.Running))
                        {
                            RasMan.Start();
                            RasMan.WaitForStatus(ServiceControllerStatus.Running);
                            RasManON = false;
                        }
                        else if (RasMan.Status.Equals(ServiceControllerStatus.Running))
                        {
                            RasManON = false;
                        }
                    }
                    Trace.WriteLine(DateTime.Now + " Restarting: RasMan");
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
                }
            }
            if (TapiSrvON == true)
            {
                try
                {
                    using (ServiceController TapiSrv = new ServiceController("TapiSrv"))
                    {
                        while (TapiSrv.Equals(ServiceControllerStatus.StopPending) || TapiSrv.Equals(ServiceControllerStatus.StartPending))
                        {
                            Thread.Sleep(20);
                        }
                        if (!TapiSrv.Status.Equals(ServiceControllerStatus.Running))
                        {
                            TapiSrv.Start();
                            TapiSrv.WaitForStatus(ServiceControllerStatus.Running);
                            TapiSrvON = false;
                        }
                        else if (TapiSrv.Status.Equals(ServiceControllerStatus.Running))
                        {
                            TapiSrvON = false;
                        }
                    }
                    Trace.WriteLine(DateTime.Now + " Restarting: TapiSrv");
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(DateTime.Now + Properties.Resources.ResourceManager.GetString("FailedOperationMessage") + "\n" + "          " + ex.Message);
                }
            }
            Thread2 = false;
        }
        public void Dispose()
        {
            ((IDisposable)_fileWatcher).Dispose();
        }
        protected virtual void OnStop()
        {
            Cleanup = false;
            ((IDisposable)_fileWatcher).Dispose();
            Environment.Exit(0);
        }
    }
}
