﻿using System.Globalization;
using System.ServiceProcess;
using System.Threading;

namespace Boost
{
    public partial class Service1 : ServiceBase
    {
        public Service1()
        {
            InitializeComponent();
        }
        public void OnDebug()
        {
            OnStart(null);
        }
        static FileWatcher fileWatcher = null;
        protected override void OnStart(string[] args)
        {
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CurrentCulture;
            fileWatcher = new FileWatcher();
        }
        protected override void OnStop()
        {
            if (fileWatcher != null)
            {
                fileWatcher.Dispose();
            }
            this.ExitCode = 0;
            this.Dispose();
            base.OnStop();
        }
    }
}
