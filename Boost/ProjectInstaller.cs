﻿using Boost.Properties;
using IWshRuntimeLibrary;
using Microsoft.Win32.TaskScheduler;
using System;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Management;
using System.Reflection;
using System.ServiceProcess;
using System.Threading;

namespace Boost
{
    [RunInstaller(true)]
    public partial class ProjectInstaller : System.Configuration.Install.Installer
    {
        public ProjectInstaller()
        {
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CurrentCulture;
            InitializeComponent();
        }
        static string SysPath = (Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) + @"\GClientGT");
        public override void Commit(IDictionary savedState)
        {
            base.Commit(savedState);
            Directory.SetCurrentDirectory(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));
            Directory.CreateDirectory(SysPath);
            Directory.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.CommonPrograms) + @"\GClient GT");
            Process myProcess = null;
            try
            {
                using (myProcess = new Process())
                {
                    myProcess.StartInfo.Arguments = "/q";
                    myProcess.StartInfo.FileName = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + @"\Redistributable\xnafx40_redist.msi";
                    myProcess.Start();
                    do
                    {
                        if (!myProcess.HasExited)
                        {
                            myProcess.Refresh();
                            if (myProcess.Responding)
                            {
                                Console.WriteLine(Resources.ResourceManager.GetString("InstallingXNA"));
                            }
                            else
                            {
                                Console.WriteLine(Resources.ResourceManager.GetString("XNAResponse"));
                            }
                        }
                    }
                    while (!myProcess.WaitForExit(1000));
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            bool AcerLaptop = false;
            bool AcerLaptopModel = false;

            using (ManagementClass mc = new ManagementClass("Win32_ComputerSystem"))
            {
                ManagementObjectCollection moc = mc.GetInstances();
                if (moc.Count != 0)
                {
                    foreach (ManagementObject mo in mc.GetInstances())
                    {
                        var factory = (mo["Manufacturer"].ToString() + " " + mo["Model"].ToString());
                        var model = (mo["Model"].ToString());
                        if (factory.Contains("Acer"))
                        {
                            AcerLaptop = true;
                        }
                        if (model.Contains("V3-571") || model.Contains("V3-571G") || model.Contains("5750G") || model.Contains("5742G") || model.Contains("5741G"))
                        {
                            AcerLaptopModel = true;
                        }
                    }
                }
            }
            string XNAFrame = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + @"\Redistributable\xnafx40_redist.msi";
            string FanDriver = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + @"\Redistributable\Fan Controller Driver.zip";
            string FanOff = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + @"\FanControl\FanOff.exe";
            string FanOn = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + @"\FanControl\FanOn.exe";
            string NO_USE_FanOff = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + @"\FanControl\NO_USE_FanOff.exe";
            string NO_USE_FanOn = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + @"\FanControl\NO_USE_FanOn.exe";
            if (!System.IO.File.Exists(FanOff) && !System.IO.File.Exists(FanOn) && AcerLaptopModel == true && AcerLaptop == true)
            {
                System.Windows.Forms.MessageBox.Show(Resources.ResourceManager.GetString("SupportForFan") + "\n" + Resources.ResourceManager.GetString("Documentation"));
            }
            if (System.IO.File.Exists(FanOff) && System.IO.File.Exists(FanOn))
            {
                try
                {
                    System.IO.File.Delete(NO_USE_FanOff);
                    System.IO.File.Delete(NO_USE_FanOn);
                    System.IO.File.Delete(FanDriver);
                }
                catch (Exception)
                {
                    throw;
                }
            }
            if (!AcerLaptop || !AcerLaptopModel)
            {
                try
                {
                    System.IO.File.Delete(NO_USE_FanOff);
                    System.IO.File.Delete(NO_USE_FanOn);
                    System.IO.File.Delete(FanDriver);
                }
                catch
                {
                    Console.WriteLine(Resources.ResourceManager.GetString("CannotDeleteFan"));
                }
            }
            try
            {
                System.IO.File.Delete(XNAFrame);
                if (!System.IO.File.Exists(FanDriver))
                {
                    Directory.Delete(Path.GetDirectoryName(XNAFrame));
                }
            }
            catch
            {
                Console.WriteLine(Resources.ResourceManager.GetString("CannotDeleteXNA"));
            }
            using (ServiceController controller = new ServiceController("GClientBoost"))
            {
                if (controller.Status == ServiceControllerStatus.Stopped)
                {
                    controller.Start();
                }
            }
            taskSchedule();
        }
        static void taskSchedule()
        {
            Directory.SetCurrentDirectory(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));
            string user = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            using (TaskService task = new TaskService())
            {
                Version version = task.HighestSupportedVersion;
                bool newVer = (version >= new Version(1, 2));
                Console.WriteLine(Resources.ResourceManager.GetString("HighestVersion" + version));
                Console.WriteLine(Resources.ResourceManager.GetString("Target"));
                Console.WriteLine(Resources.ResourceManager.GetString("RunTasks"));
                foreach (RunningTask runningTasks in task.GetRunningTasks(true))
                {
                    if (runningTasks != null)
                    {
                        Console.WriteLine(Resources.ResourceManager.GetString("RunPath" + runningTasks.Name + runningTasks.Path + runningTasks.State));
                        if (version.Minor > 0)
                            Console.WriteLine(Resources.ResourceManager.GetString("CurrentAction" + runningTasks.CurrentAction));
                    }
                }
                TaskFolder taskFolder = task.RootFolder;
                Console.WriteLine(Resources.ResourceManager.GetString("TaskCount" + taskFolder.Tasks.Count));
                foreach (Task tasks in taskFolder.Tasks)
                {
                    try
                    {
                        Console.WriteLine(Resources.ResourceManager.GetString("TaskDefinition" + tasks.Name + tasks.Definition.RegistrationInfo.Author + tasks.State));
                        foreach (Trigger trigger in tasks.Definition.Triggers)
                            Console.WriteLine(Resources.ResourceManager.GetString("TaskTrigger" + trigger));
                        foreach (Microsoft.Win32.TaskScheduler.Action action in tasks.Definition.Actions)
                            Console.WriteLine(Resources.ResourceManager.GetString("TaskAction" + action));
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }
                Console.WriteLine(Resources.ResourceManager.GetString("CheckFolder"));
                TaskFolderCollection taskFolderCollection = taskFolder.SubFolders;
                if (taskFolderCollection.Count > 0)
                {
                    Console.WriteLine(Resources.ResourceManager.GetString("SubFolders"));
                    try
                    {
                        foreach (TaskFolder standardFolder in taskFolderCollection)
                            Console.WriteLine(Resources.ResourceManager.GetString("FolderStandard" + standardFolder.Path));
                    }
                    catch (Exception err)
                    {
                        Console.WriteLine(err.ToString());
                        throw;
                    }
                }
                if (newVer)
                {
                    Console.WriteLine(Resources.ResourceManager.GetString("RetrieveFolder"));
                    try
                    {
                        TaskFolder sub = taskFolder.SubFolders["Microsoft"];
                        Console.WriteLine(Resources.ResourceManager.GetString("SubFolderPath" + sub.Path));
                    }
                    catch (NotSupportedException) { }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.ToString());
                        throw;
                    }
                }
                Console.WriteLine(Resources.ResourceManager.GetString("CheckTask"));
                try
                {
                    TaskDefinition td = task.NewTask();
                    td.Principal.UserId = user;
                    td.Principal.LogonType = TaskLogonType.InteractiveToken;
                    td.RegistrationInfo.Author = "mikeyhalla";
                    td.RegistrationInfo.Description = "Launch GClient GT Edition with Administrative rights...";
                    td.Settings.Enabled = true;
                    td.Settings.Priority = ProcessPriorityClass.Normal;
                    td.Principal.RunLevel = TaskRunLevel.Highest;
                    td.RegistrationInfo.Source = "GClientGT";
                    td.RegistrationInfo.Version = new Version(0, 9);
                    td.Settings.AllowDemandStart = true;
                    td.Settings.Compatibility = TaskCompatibility.V2;
                    LogonTrigger lTrigger = (LogonTrigger)td.Triggers.Add(new LogonTrigger());
                    if (newVer)
                    {
                        lTrigger.UserId = user;
                    }
                    // Program to execute
                    td.Actions.Add(new ExecAction("\"" + Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + @"\Launcher.exe" + "\"", null, Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)));
                    taskFolder.RegisterTaskDefinition("GClientGT", td, TaskCreation.CreateOrUpdate, null, null,
                       TaskLogonType.InteractiveToken, null);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                    throw;
                }
                Task runningTask = taskFolder.Tasks["GClientGT"];
                Console.WriteLine(Resources.ResourceManager.GetString("NextRun" + runningTask.NextRunTime));
                Console.WriteLine(Resources.ResourceManager.GetString("RunningDefinitionAction"));
                for (int i = 0; i < runningTask.Definition.Triggers.Count; i++)
                    Console.WriteLine(Resources.ResourceManager.GetString("RunningDefinitionTrigger" + i + runningTask.Definition.Triggers[i]));
                Console.WriteLine(Resources.ResourceManager.GetString("NewAction"));
                for (int i = 0; i < runningTask.Definition.Actions.Count; i++)
                    Console.WriteLine(Resources.ResourceManager.GetString("RunningDefinitionAction" + i + runningTask.Definition.Actions[i]));
            }
            finalizer();
        }
        static void finalizer()
        {
            Process.Start(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + @"\Launcher.exe");
            Directory.SetCurrentDirectory(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));
            //Create Shortcut
            WshShell shell = new WshShell();
            if (!System.IO.File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.CommonPrograms) + @"\GClient GT\Game Banners.url"))
            {
                System.IO.File.Copy(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + @"\Game Banners.url", Environment.GetFolderPath(Environment.SpecialFolder.CommonPrograms) + @"\GClient GT\Game Banners.url");
            }
            string shortcutAddress = Environment.GetFolderPath(Environment.SpecialFolder.CommonPrograms) + @"\GClient GT\GClient GT.lnk";
            string shortcutAddress2 = Environment.GetFolderPath(Environment.SpecialFolder.CommonPrograms) + @"\GClient GT\Documentation.lnk";
            IWshShortcut shortcut = (IWshShortcut)shell.CreateShortcut(shortcutAddress);
            IWshShortcut shortcut2 = (IWshShortcut)shell.CreateShortcut(shortcutAddress2);
            shortcut.Description = "GClient GT Launcher";
            shortcut2.Description = "Documentation";
            shortcut.WorkingDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            shortcut2.WorkingDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            shortcut.TargetPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + @"\Launcher.exe";
            shortcut2.TargetPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + @"\Documentation.txt";
            shortcut.Save();
            shortcut2.Save();
        }
    }
}