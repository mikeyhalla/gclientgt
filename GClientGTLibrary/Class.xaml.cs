﻿using System;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Security.Permissions;
using System.Security;
using System.Runtime.ConstrainedExecution;
using System.Text;
using System.IO;
using System.Linq;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.ComponentModel;
using System.Globalization;
using System.Threading;

namespace GClientGTLibrary
{
    public static class ShortcutUtils
    {
        public static string ResolveShortcut(string filePath)
        {
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CurrentCulture;
            IWshRuntimeLibrary.WshShell shell = new IWshRuntimeLibrary.WshShell();
            try
            {
                IWshRuntimeLibrary.IWshShortcut shortcut = (IWshRuntimeLibrary.IWshShortcut)shell.CreateShortcut(filePath);
                return shortcut.TargetPath;
            }
            catch (COMException)
            {
                return null;
            }
        }

        private class NativeMethods
        {
            [DllImport("msi.dll", CharSet = CharSet.Unicode)]
            public static extern uint MsiGetShortcutTarget(string targetFile, StringBuilder productCode, StringBuilder featureID, StringBuilder componentCode);

            [DllImport("msi.dll", CharSet = CharSet.Unicode)]
            public static extern InstallState MsiGetComponentPath(string productCode, string componentCode, StringBuilder componentPath, ref int componentPathBufferSize);

            public const int MaxFeatureLength = 38;
            public const int MaxGuidLength = 38;
            public const int MaxPathLength = 1024;

            public enum InstallState
            {
                NotUsed = -7,
                BadConfig = -6,
                Incomplete = -5,
                SourceAbsent = -4,
                MoreData = -3,
                InvalidArg = -2,
                Unknown = -1,
                Broken = 0,
                Advertised = 1,
                Removed = 1,
                Absent = 2,
                Local = 3,
                Source = 4,
                Default = 5
            }
        }
    }
    public partial class Actions
    {
        public static Bitmap newImage = null;
        public static EncoderParameters encoderParameters = null;
        public static Bitmap bitmap = null;
        public static void Save(Image image, int maxWidth, int maxHeight, int quality, string filePath)
        {
            if (image != null || !image.Equals(0))
            {
                int originalWidth = image.Width;
                int originalHeight = image.Height;
                float ratioX = maxWidth / (float)originalWidth;
                float ratioY = maxHeight / (float)originalHeight;
                float ratio = Math.Min(ratioX, ratioY);
                int newWidth = (int)(originalWidth * ratio);
                int newHeight = (int)(originalHeight * ratio);
                newImage = new Bitmap(newWidth, newHeight, PixelFormat.Format24bppRgb);
                using (Graphics graphics = Graphics.FromImage(newImage))
                {
                    graphics.CompositingQuality = CompositingQuality.HighQuality;
                    graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    graphics.SmoothingMode = SmoothingMode.HighQuality;
                    graphics.DrawImage(image, 0, 0, newWidth, newHeight);
                }
                ImageCodecInfo imageCodecInfo = GetEncoderInfo(ImageFormat.Jpeg);
                System.Drawing.Imaging.Encoder encoder = System.Drawing.Imaging.Encoder.Quality;
                encoderParameters = new EncoderParameters(1);
                EncoderParameter encoderParameter = new EncoderParameter(encoder, quality);
                encoderParameters.Param[0] = encoderParameter;
                newImage.Save(filePath, imageCodecInfo, encoderParameters);
            }
        }
        public static ImageCodecInfo GetEncoderInfo(ImageFormat format)
        {
            return ImageCodecInfo.GetImageDecoders().SingleOrDefault(c => c.FormatID == format.Guid);
        }
    }
    public static class NativeMethods
    {
        [DllImport("user32.dll")]
        static extern IntPtr GetForegroundWindow();
        [DllImport("user32.dll", CharSet = CharSet.Unicode)]
        static extern int GetWindowText(IntPtr hWnd, StringBuilder text, int count);
        public static string GetActiveWindowTitle()
        {
            const int nChars = 256;
            StringBuilder Buff = new StringBuilder(nChars);
            IntPtr handle = GetForegroundWindow();
            if (GetWindowText(handle, Buff, nChars) > 0)
            {
                return Buff.ToString();
            }
            return null;
        }
        [SecurityCritical]
        [SecurityPermission(SecurityAction.Demand, UnmanagedCode = true)]
        public abstract class CriticalHandle : CriticalFinalizerObject
        {
            [StructLayout(LayoutKind.Sequential)]
            struct Rect
            {
                public int Left;
                public int Top;
                public int Right;
                public int Bottom;
            }
            [SecurityCritical]
            [SecurityPermission(SecurityAction.InheritanceDemand, UnmanagedCode = true)]
            public static class ScreenDetection
            {
                [DllImport("user32.dll", CharSet = CharSet.Unicode)]
                static extern IntPtr GetForegroundWindow();
                [DllImport("user32.dll", CharSet = CharSet.Unicode)]
                static extern IntPtr GetDesktopWindow();
                [DllImport("user32.dll", CharSet = CharSet.Unicode)]
                static extern IntPtr GetShellWindow();
                [DllImport("user32.dll", CharSet = CharSet.Unicode)]
                static extern uint GetWindowRect(IntPtr hwnd, out Rect rc);
                [DllImport("user32.dll", CharSet = CharSet.Unicode)]
                static extern int GetClassName(IntPtr hWnd, StringBuilder lpClassName, int nMaxCount);
                public static bool AreApplicationFullScreen()
                {
                    const int maxChars = 256;
                    StringBuilder className = new StringBuilder(maxChars);
                    IntPtr handle = GetForegroundWindow();
                    bool runningFullScreen = false;
                    Rect appBounds;
                    Rectangle screenBounds;
                    IntPtr hWnd;
                    hWnd = GetForegroundWindow();
                    if (!hWnd.Equals(null) && !hWnd.Equals(IntPtr.Zero))
                    {
                        if (!(hWnd.Equals(GetDesktopWindow()) || hWnd.Equals(GetShellWindow())))
                        {
                            if (GetClassName(handle, className, maxChars) > 0)
                            {
                                string cName = className.ToString();
                                var Handle = GetWindowRect(hWnd, out appBounds);
                                if (!Handle.Equals(null) && cName != "Progman" && cName != "WorkerW")
                                {
                                    screenBounds = Screen.FromHandle(hWnd).Bounds;
                                    if ((appBounds.Bottom - appBounds.Top) == screenBounds.Height
                                        && (appBounds.Right - appBounds.Left) == screenBounds.Width)
                                    {
                                        runningFullScreen = true;
                                    }
                                }
                            }
                        }
                    }
                    return runningFullScreen;
                }
            }
            [DllImport("user32.dll")]
            [return: MarshalAs(UnmanagedType.Bool)]
            static extern bool SetForegroundWindow(UIntPtr hWnd);
            [DllImport("xinput1_3.dll", EntryPoint = "#100", CharSet = CharSet.Unicode)]
            static extern int gamepadGuide(int playerIndex, out XINPUT_Guide struc);
            [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
            static extern UIntPtr FindWindow(string lpClassName, string lpWindowName);
            [return: MarshalAs(UnmanagedType.Bool)]
            [DllImport("user32.dll", CharSet = CharSet.Unicode)]
            static extern bool ShowWindow(UIntPtr hWnd, int nCmdShow);
            public static void BringToFront(string className, string caption)
            {
                SetForegroundWindow(FindWindow(className, caption));
            }
            public struct XINPUT_Guide
            {
                public UInt32 eventCount;
                public ushort wButtons;
                public Byte bLeftTrigger;
                public Byte bRightTrigger;
                public short sThumbLX;
                public short sThumbLY;
                public short sThumbRX;
                public short sThumbRY;
            }
            public static int guide(int playerIndex, out XINPUT_Guide struc)
            {
                return gamepadGuide(playerIndex, out struc);
            }
            public static UIntPtr Find(string className, string windowName)
            {
                return FindWindow(className, windowName);
            }
            public static bool Show(UIntPtr hWnd, int nCmdShow)
            {
                return ShowWindow(hWnd, nCmdShow);
            }
        }
    }
    public class Variables
    {
        //Guide Button
        public static NativeMethods.CriticalHandle.XINPUT_Guide xgs;
        //strings
        public static string Game; public static string GameImage; public static string GameArgs;
        public static string Game1; public static string Game2; public static string Game3; public static string Game4; public static string Game5; public static string Game6; public static string Game7; public static string Game8; public static string Game9; public static string Game10; public static string Game11; public static string Game12; public static string Game13; public static string Game14; public static string Game15; public static string Game16; public static string Game17; public static string Game18; public static string Game19; public static string Game20; public static string Game21; public static string Game22; public static string Game23; public static string Game24; public static string Game25; public static string Game26; public static string Game27; public static string Game28; public static string Game29; public static string Game30; public static string Game31; public static string Game32; public static string Game33; public static string Game34; public static string Game35; public static string Game36; public static string Game37; public static string Game38; public static string Game39; public static string Game40; public static string Game41; public static string Game42; public static string Game43; public static string Game44; public static string Game45; public static string Game46; public static string Game47; public static string Game48; public static string Game49; public static string Game50; public static string Game51; public static string Game52; public static string Game53; public static string Game54; public static string Game55; public static string Game56; public static string Game57; public static string Game58; public static string Game59; public static string Game60;
        public static string G1Args; public static string G2Args; public static string G3Args; public static string G4Args; public static string G5Args; public static string G6Args; public static string G7Args; public static string G8Args; public static string G9Args; public static string G10Args; public static string G11Args; public static string G12Args; public static string G13Args; public static string G14Args; public static string G15Args; public static string G16Args; public static string G17Args; public static string G18Args; public static string G19Args; public static string G20Args; public static string G21Args; public static string G22Args; public static string G23Args; public static string G24Args; public static string G25Args; public static string G26Args; public static string G27Args; public static string G28Args; public static string G29Args; public static string G30Args; public static string G31Args; public static string G32Args; public static string G33Args; public static string G34Args; public static string G35Args; public static string G36Args; public static string G37Args; public static string G38Args; public static string G39Args; public static string G40Args; public static string G41Args; public static string G42Args; public static string G43Args; public static string G44Args; public static string G45Args; public static string G46Args; public static string G47Args; public static string G48Args; public static string G49Args; public static string G50Args; public static string G51Args; public static string G52Args; public static string G53Args; public static string G54Args; public static string G55Args; public static string G56Args; public static string G57Args; public static string G58Args; public static string G59Args; public static string G60Args;
        public static string GameImage1; public static string GameImage2; public static string GameImage3; public static string GameImage4; public static string GameImage5; public static string GameImage6; public static string GameImage7; public static string GameImage8; public static string GameImage9; public static string GameImage10; public static string GameImage11; public static string GameImage12; public static string GameImage13; public static string GameImage14; public static string GameImage15; public static string GameImage16; public static string GameImage17; public static string GameImage18; public static string GameImage19; public static string GameImage20; public static string GameImage21; public static string GameImage22; public static string GameImage23; public static string GameImage24; public static string GameImage25; public static string GameImage26; public static string GameImage27; public static string GameImage28; public static string GameImage29; public static string GameImage30; public static string GameImage31; public static string GameImage32; public static string GameImage33; public static string GameImage34; public static string GameImage35; public static string GameImage36; public static string GameImage37; public static string GameImage38; public static string GameImage39; public static string GameImage40; public static string GameImage41; public static string GameImage42; public static string GameImage43; public static string GameImage44; public static string GameImage45; public static string GameImage46; public static string GameImage47; public static string GameImage48; public static string GameImage49; public static string GameImage50; public static string GameImage51; public static string GameImage52; public static string GameImage53; public static string GameImage54; public static string GameImage55; public static string GameImage56; public static string GameImage57; public static string GameImage58; public static string GameImage59; public static string GameImage60;
        public static string ImageHandle1; public static string ImageHandle2; public static string ImageHandle3; public static string ImageHandle4; public static string ImageHandle5; public static string ImageHandle6; public static string ImageHandle7; public static string ImageHandle8; public static string ImageHandle9; public static string ImageHandle10; public static string ImageHandle11; public static string ImageHandle12; public static string ImageHandle13; public static string ImageHandle14; public static string ImageHandle15; public static string ImageHandle16; public static string ImageHandle17; public static string ImageHandle18; public static string ImageHandle19; public static string ImageHandle20; public static string ImageHandle21; public static string ImageHandle22; public static string ImageHandle23; public static string ImageHandle24; public static string ImageHandle25; public static string ImageHandle26; public static string ImageHandle27; public static string ImageHandle28; public static string ImageHandle29; public static string ImageHandle30; public static string ImageHandle31; public static string ImageHandle32; public static string ImageHandle33; public static string ImageHandle34; public static string ImageHandle35; public static string ImageHandle36; public static string ImageHandle37; public static string ImageHandle38; public static string ImageHandle39; public static string ImageHandle40; public static string ImageHandle41; public static string ImageHandle42; public static string ImageHandle43; public static string ImageHandle44; public static string ImageHandle45; public static string ImageHandle46; public static string ImageHandle47; public static string ImageHandle48; public static string ImageHandle49; public static string ImageHandle50; public static string ImageHandle51; public static string ImageHandle52; public static string ImageHandle53; public static string ImageHandle54; public static string ImageHandle55; public static string ImageHandle56; public static string ImageHandle57; public static string ImageHandle58; public static string ImageHandle59; public static string ImageHandle60;
        public static string ServiceStatus = "GClient GT";
        public static string Temp = Path.GetDirectoryName(Path.GetTempPath());
        public static string TempPath = Temp + @"\GClientGT\";
        public static string TempFile = TempPath + @"\GameList.xml";
        public static string GetFirstWord(string text)
        {
            string firstWord = string.Empty;
            if (string.IsNullOrEmpty(text))
            {
                return string.Empty;
            }
            firstWord = text.Split(' ').FirstOrDefault();
            if (string.IsNullOrEmpty(firstWord))
            {
                return string.Empty;
            }
            return firstWord;
        }
        //Directories
        public static string SysPath = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) + @"\GClientGT";
        public static string LogPath = SysPath + @"\LOGS";
        public static string GameListX = SysPath + @"\GameList.xml";
        public static string SysFile = SysPath + @"\GClientGT.ini";
        public static string ImagePath = SysPath + @"\Images\";
        public static string previousPath = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures);
        public static string GameImages = previousPath + @"\Game Images";
        public static string homeFolder = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
        public static string Programs = Environment.GetFolderPath(Environment.SpecialFolder.StartMenu);
        public static string ProgamFilesStandard = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles);
        public static string ProgramFilesx86 = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86);
        public static string CommonPrograms = Environment.GetFolderPath(Environment.SpecialFolder.CommonStartMenu);
        public static string StartMenu = Environment.GetFolderPath(Environment.SpecialFolder.StartMenu) + @"\Programs\Games";
        public static string CommonStartMenu = Environment.GetFolderPath(Environment.SpecialFolder.CommonStartMenu) + @"\Programs\Games";
        public static string GamesFolder = @"C:\Games";
        public static string CommonStartMenuPath = Environment.GetFolderPath(Environment.SpecialFolder.CommonStartMenu);
        public static string XFile = TempPath + @"\GameList.xml";
        //bool
        public static bool GamePadConnected = false;
        public static bool close = true;
        public static bool ShutDownSteam = true;
        public static bool minimize = false;
        public static bool GameRunning = false;
        public static bool GuideButton = false;
        public static bool active = false;
        public static bool AutoStart;
        public static bool Fan;
        public static bool BOOST;
        public static bool WindowedGame = false;
        public static bool Exiting;
        public static bool GUIShowing;
        public static bool RightPanel = false;
        public static bool LeftPanel = false;
        public static bool Grid2Service = false;
        public static bool Added = false;
        public static bool ImageAdded = false;
        public static bool ArgsAdded = false;
        public static bool AddGames = true;
        public static bool GameFolder = true;
        public static bool StartGames = true;
        public static bool StartPrograms = false;
        public static bool ProgFiles = false;
        public static bool SteamGames = true;
        public static bool empty = true;
        //float
        public static float Leftrumble = 0.2f;
        public static float Rightrumble = 0.2f;
        public static float LeftNorumble = 0.0f;
        public static float RightNorumble = 0.0f;
        //Objects
        public static IContainer components;
        public static Bitmap bitmap;
        public static Button button1;
        public static Panel panel1;
        public static Label label1;
        public static Panel panel2;
        public static Bitmap newImage = null;
        public static System.Drawing.Imaging.Encoder encoder = null;
        public static EncoderParameters encoderParameters = null;
        public static EncoderParameter encoderParameter = null;
        public static ImageCodecInfo imageCodecInfo = null;
        public static CheckedListBox listBox1;
        public static Button button2;
        public static Button button3;
        public static PictureBox pictureBox1;
        public static ProgressBar progressBar1;
        public static RichTextBox richTextbox3;
        public static Panel panel3;
        public static CheckBox checkBox4;
        public static CheckBox checkBox3;
        public static CheckBox checkBox2;
        public static CheckBox checkBox1;
        public static CheckBox checkBox5;
        public static BackgroundWorker worker = null;
        public static NotifyIcon notifyIcon1;
        public static ContextMenu contextMenu1;
        public static MenuItem menuItem1;
        public static MenuItem menuItem2;
        public static MenuItem menuItem3;
    }
    public class Dialog : Form
    {
        private const int CS_DROPSHADOW = 0x00020000;
        protected Label lblMessage;
        protected TextBox txtInput;
        protected string _txtInput;
        protected bool _txtPaintInvalidated = false;
        public static Panel txtPl = null;
        public static FlowLayoutPanel flp = null;
        public static FlowLayoutPanel flpButtons = null;
        public static Button btnOK = null;
        public static Button btnCancel = null;
        public static Panel panel2 = null;
        public static Label label1 = null;
        private Dialog()
        {
            Name = Properties.Resources.ResourceManager.GetString("Message");
            Icon = Icon = Properties.Resources.GClientGTLogo;
            FormBorderStyle = FormBorderStyle.FixedSingle;
            MaximizeBox = false;
            MinimizeBox = false;
            pl = new Panel();
            pl.Dock = DockStyle.Fill;
            panel2 = new Panel();
            label1 = new Label();
            label1.AutoSize = true;
            label1.Font = new Font("Century Gothic", 11.75F);
            label1.ForeColor = Color.FromArgb(224, 224, 224);
            label1.Location = new Point(4, 6);
            label1.Name = "label1";
            label1.Size = new Size(96, 21);
            label1.TabIndex = 1;
            label1.Text = "GClient GT";
            panel2.BackColor = Color.FromArgb(45, 45, 45);
            panel2.Controls.Add(label1);
            panel2.Location = new Point(1, 1);
            panel2.Name = "panel2";
            panel2.Size = new Size(498, 33);
            panel2.TabIndex = 3;
            Controls.Add(panel2);
            flp = new FlowLayoutPanel();
            flp.Dock = DockStyle.Fill;
            lblMessage = new Label();
            lblMessage.Font = new Font("Segoe UI", 10);
            lblMessage.ForeColor = Color.White;
            lblMessage.AutoSize = true;
            txtPl = new Panel();
            txtPl.BorderStyle = BorderStyle.None;
            txtPl.Width = 460;
            txtPl.Height = 28;
            txtPl.Padding = new Padding(5);
            txtPl.BackColor = Color.White;
            txtPl.Margin = new Padding(0, 15, 0, 0);
            txtPl.Paint += txtPl_Paint;
            txtInput = new TextBox();
            txtInput.Dock = DockStyle.Fill;
            txtInput.BorderStyle = BorderStyle.None;
            txtInput.Font = new Font("Segoe UI", 9);
            txtInput.KeyDown += txtInput_KeyDown;
            txtInput.BackColor = Color.FromArgb(240, 240, 240);
            txtInput.Multiline = true;
            txtPl.Controls.Add(txtInput);
            flpButtons = new FlowLayoutPanel();
            flpButtons.Dock = DockStyle.Bottom;
            flpButtons.FlowDirection = FlowDirection.RightToLeft;
            flpButtons.Height = 35;
            btnCancel = new Button();
            btnCancel.Text = Properties.Resources.ResourceManager.GetString("Cancel");
            btnCancel.ForeColor = Color.FromArgb(170, 170, 170);
            btnCancel.Font = new Font("Segoe UI", 8);
            btnCancel.Padding = new Padding(3);
            btnCancel.FlatStyle = FlatStyle.Flat;
            btnCancel.Height = 30;
            btnCancel.Click += btnCancel_Click;
            btnOK = new Button();
            btnOK.Text = Properties.Resources.ResourceManager.GetString("OK");
            btnOK.ForeColor = Color.FromArgb(170, 170, 170);
            btnOK.Font = new Font("Segoe UI", 8);
            btnOK.Padding = new Padding(3);
            btnOK.FlatStyle = FlatStyle.Flat;
            btnOK.Height = 30;
            btnOK.Click += btnOK_Click;
            flpButtons.Controls.Add(btnCancel);
            flpButtons.Controls.Add(btnOK);
            flp.Controls.Add(lblMessage);
            flp.SetFlowBreak(lblMessage, true);
            flp.Controls.Add(txtPl);
            flp.SetFlowBreak(txtPl, true);
            flp.Controls.Add(flpButtons);
            pl.Controls.Add(flp);
            Text = Properties.Resources.ResourceManager.GetString("Message");
            Controls.Add(pl);
            Controls.Add(flpButtons);
            FormBorderStyle = FormBorderStyle.None;
            BackColor = Color.FromArgb(35, 35, 35);
            StartPosition = FormStartPosition.CenterScreen;
            Padding = new Padding(20, 55, 20, 20);
            Width = 500;
            Height = 200;
        }
        void txtInput_KeyDown(object sender, KeyEventArgs e)
        {
            TextBox txt = (TextBox)sender;
            if (e.KeyCode == Keys.Enter)
            {
                _txtInput = txt.Text;
                Dispose();
            }
            else
            {
                if (txt.Text.Length > 60)
                {
                    txt.Parent.Height = 80;
                    if (!_txtPaintInvalidated)
                    {
                        txt.Parent.Invalidate();
                        _txtPaintInvalidated = true;
                    }
                }
                if (txt.Text.Length < 60)
                {
                    txt.Parent.Height = 28;
                    if (_txtPaintInvalidated)
                    {
                        txt.Parent.Invalidate();
                        _txtPaintInvalidated = false;
                    }
                }
            }
        }
        public static Panel pl = null;
        public static Pen pen = null;
        public static SolidBrush brush = null;
        private void txtPl_Paint(object sender, PaintEventArgs e)
        {
            pl = (Panel)sender;
            base.OnPaint(e);
            Graphics g = e.Graphics;
            Rectangle rect = new Rectangle(new Point(0, 0), new Size(pl.Width - 1, pl.Height - 1));
            pen = new Pen(Color.FromArgb(10, 100, 255));
            pen.Width = 3;
            brush = new SolidBrush(Color.FromArgb(240, 240, 240));
            g.FillRectangle(brush, rect);
            g.DrawRectangle(pen, rect);
        }
        void btnCancel_Click(object sender, EventArgs e)
        {
            if (g != null)
                g.Dispose();
            if (pen != null)
                pen.Dispose();
            if (btnOK != null)
                btnOK.Dispose();
            if (btnCancel != null)
                btnCancel.Dispose();
            if (flpButtons != null)
                flpButtons.Dispose();
            if (flp != null)
                flp.Dispose();
            if (pl != null)
                pl.Dispose();
            if (brush != null)
                brush.Dispose();
            if (dialog != null)
                dialog.Dispose();
            if (txtPl != null)
                txtPl.Dispose();
            if (panel2 != null)
                panel2.Dispose();
            if (label1 != null)
                label1.Dispose();
            Dispose();
        }
        void btnOK_Click(object sender, EventArgs e)
        {
            _txtInput = txtInput.Text;
            if (g != null)
                g.Dispose();
            if (pen != null)
                pen.Dispose();
            if (btnOK != null)
                btnOK.Dispose();
            if (btnCancel != null)
                btnCancel.Dispose();
            if (flpButtons != null)
                flpButtons.Dispose();
            if (flp != null)
                flp.Dispose();
            if (pl != null)
                pl.Dispose();
            if (brush != null)
                brush.Dispose();
            if (dialog != null)
                dialog.Dispose();
            if (txtPl != null)
                txtPl.Dispose();
            if (panel2 != null)
                panel2.Dispose();
            if (label1 != null)
                label1.Dispose();
            Dispose();
        }
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ClassStyle |= CS_DROPSHADOW;
                return cp;
            }
        }
        public static Dialog dialog = null;
        public static string Show(string message)
        {
            dialog = new Dialog();
            dialog.lblMessage.Text = message;
            dialog.ShowDialog();
            dialog.TopMost = true;
            dialog.BringToFront();
            dialog.Activate();
            dialog.TopMost = false;
            return dialog._txtInput;
        }
        public static Graphics g = null;
        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            if (e != null)
                g = e.Graphics;
            Rectangle rect = new Rectangle(new Point(0, 0), new Size(Width - 1, Height - 1));
            pen = new Pen(Color.FromArgb(10, 100, 255));
            g.DrawRectangle(pen, rect);
        }
    }
    public class DialogNoBoxYesNo : Form
    {
        private const int CS_DROPSHADOW = 0x00020000;
        protected Label lblMessage;
        public static Panel pl = null;
        public static FlowLayoutPanel flpButtons = null;
        public static FlowLayoutPanel flp = null;
        public static Button btnYES = null;
        public static Button btnNO = null;
        public static Panel panel2 = null;
        public static Label label1 = null;
        private DialogNoBoxYesNo()
        {
            Name = Properties.Resources.ResourceManager.GetString("Message");
            Icon = Icon = Properties.Resources.GClientGTLogo;
            FormBorderStyle = FormBorderStyle.FixedSingle;
            MaximizeBox = false;
            MinimizeBox = false;
            pl = new Panel();
            pl.Dock = DockStyle.Fill;
            panel2 = new Panel();
            label1 = new Label();
            label1.AutoSize = true;
            label1.Font = new Font("Century Gothic", 11.75F);
            label1.ForeColor = Color.FromArgb(224, 224, 224);
            label1.Location = new Point(4, 6);
            label1.Name = "label1";
            label1.Size = new Size(96, 21);
            label1.TabIndex = 1;
            label1.Text = "GClient GT";
            panel2.BackColor = Color.FromArgb(45, 45, 45);
            panel2.Controls.Add(label1);
            panel2.Location = new Point(1, 1);
            panel2.Name = "panel2";
            panel2.Size = new Size(498, 33);
            panel2.TabIndex = 3;
            Controls.Add(panel2);
            flp = new FlowLayoutPanel();
            flp.Dock = DockStyle.Fill;
            lblMessage = new Label();
            lblMessage.Font = new Font("Segoe UI", 10);
            lblMessage.ForeColor = Color.White;
            lblMessage.AutoSize = true;
            flpButtons = new FlowLayoutPanel();
            flpButtons.Dock = DockStyle.Bottom;
            flpButtons.FlowDirection = FlowDirection.RightToLeft;
            flpButtons.Height = 35;
            btnNO = new Button();
            btnNO.Text = Properties.Resources.ResourceManager.GetString("NO");
            btnNO.ForeColor = Color.FromArgb(170, 170, 170);
            btnNO.Font = new Font("Segoe UI", 8);
            btnNO.Padding = new Padding(3);
            btnNO.FlatStyle = FlatStyle.Flat;
            btnNO.Height = 30;
            btnNO.Click += btnNO_Click;
            btnYES = new Button();
            btnYES.Text = Properties.Resources.ResourceManager.GetString("YES"); ;
            btnYES.ForeColor = Color.FromArgb(170, 170, 170);
            btnYES.Font = new Font("Segoe UI", 8);
            btnYES.Padding = new Padding(3);
            btnYES.FlatStyle = FlatStyle.Flat;
            btnYES.Height = 30;
            btnYES.Click += btnYES_Click;
            flpButtons.Controls.Add(btnYES);
            flpButtons.Controls.Add(btnNO);
            flp.Controls.Add(lblMessage);
            flp.SetFlowBreak(lblMessage, true);
            flp.Controls.Add(flpButtons);
            pl.Controls.Add(flp);
            Text = Properties.Resources.ResourceManager.GetString("Message");
            Controls.Add(pl);
            Controls.Add(flpButtons);
            FormBorderStyle = FormBorderStyle.None;
            BackColor = Color.FromArgb(35, 35, 35);
            StartPosition = FormStartPosition.CenterScreen;
            Padding = new Padding(20, 55, 20, 20);
            Width = 500;
            Height = 155;
        }
        public static Pen pen = null;
        public static SolidBrush brush = null;
        public static bool yes;
        void btnNO_Click(object sender, EventArgs e)
        {
            yes = false;
            if (g != null)
                g.Dispose();
            if (pen != null)
                pen.Dispose();
            if (btnNO != null)
                btnNO.Dispose();
            if (btnYES != null)
                btnYES.Dispose();
            if (pl != null)
                pl.Dispose();
            if (flp != null)
                flp.Dispose();
            if (flpButtons != null)
                flpButtons.Dispose();
            if (brush != null)
                brush.Dispose();
            if (dialog != null)
                dialog.Dispose();
            if (panel2 != null)
                panel2.Dispose();
            if (label1 != null)
                label1.Dispose();
            Dispose();
        }
        void btnYES_Click(object sender, EventArgs e)
        {
            yes = true;
            if (g != null)
                g.Dispose();
            if (pen != null)
                pen.Dispose();
            if (btnNO != null)
                btnNO.Dispose();
            if (btnYES != null)
                btnYES.Dispose();
            if (pl != null)
                pl.Dispose();
            if (flp != null)
                flp.Dispose();
            if (flpButtons != null)
                flpButtons.Dispose();
            if (brush != null)
                brush.Dispose();
            if (dialog != null)
                dialog.Dispose();
            if (panel2 != null)
                panel2.Dispose();
            if (label1 != null)
                label1.Dispose();
            Dispose();
        }
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ClassStyle |= CS_DROPSHADOW;
                return cp;
            }
        }
        public static DialogNoBoxYesNo dialog = null;
        public static string Show(string message)
        {
            dialog = new DialogNoBoxYesNo();
            dialog.lblMessage.Text = message;
            dialog.ShowDialog();
            if (yes)
                return Properties.Resources.ResourceManager.GetString("YES");
            else
                return Properties.Resources.ResourceManager.GetString("NO");
        }
        public static Graphics g = null;
        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            if (e != null)
                g = e.Graphics;
            Rectangle rect = new Rectangle(new Point(0, 0), new Size(Width - 1, Height - 1));
            pen = new Pen(Color.FromArgb(10, 100, 255));
            g.DrawRectangle(pen, rect);
        }
    }
    public class DialogNoBox : Form
    {
        private const int CS_DROPSHADOW = 0x00020000;
        protected Label lblMessage;
        public static Panel pl = null;
        public static FlowLayoutPanel flpButtons = null;
        public static FlowLayoutPanel flp = null;
        public static Button btnOK = null;
        public static Panel panel2 = null;
        public static Label label1 = null;
        private DialogNoBox()
        {
            Name = Properties.Resources.ResourceManager.GetString("Message");
            Icon = Icon = Properties.Resources.GClientGTLogo;
            FormBorderStyle = FormBorderStyle.FixedSingle;
            MaximizeBox = false;
            MinimizeBox = false;
            pl = new Panel();
            pl.Dock = DockStyle.Fill;
            panel2 = new Panel();
            label1 = new Label();
            label1.AutoSize = true;
            label1.Font = new Font("Century Gothic", 11.75F);
            label1.ForeColor = Color.FromArgb(224, 224, 224);
            label1.Location = new Point(4, 6);
            label1.Name = "label1";
            label1.Size = new Size(96, 21);
            label1.TabIndex = 1;
            label1.Text = "GClient GT";
            panel2.BackColor = Color.FromArgb(45, 45, 45);
            panel2.Controls.Add(label1);
            panel2.Location = new Point(1, 1);
            panel2.Name = "panel2";
            panel2.Size = new Size(498, 33);
            panel2.TabIndex = 3;
            Controls.Add(panel2);
            flp = new FlowLayoutPanel();
            flp.Dock = DockStyle.Fill;
            lblMessage = new Label();
            lblMessage.Font = new Font("Segoe UI", 10);
            lblMessage.ForeColor = Color.White;
            lblMessage.AutoSize = true;
            flpButtons = new FlowLayoutPanel();
            flpButtons.Dock = DockStyle.Bottom;
            flpButtons.FlowDirection = FlowDirection.RightToLeft;
            flpButtons.Height = 35;
            btnOK = new Button();
            btnOK.Text = Properties.Resources.ResourceManager.GetString("OK");
            btnOK.ForeColor = Color.FromArgb(170, 170, 170);
            btnOK.Font = new Font("Segoe UI", 8);
            btnOK.Padding = new Padding(3);
            btnOK.FlatStyle = FlatStyle.Flat;
            btnOK.Height = 30;
            btnOK.Click += btnOK_Click;
            flpButtons.Controls.Add(btnOK);
            flp.Controls.Add(lblMessage);
            flp.SetFlowBreak(lblMessage, true);
            flp.Controls.Add(flpButtons);
            pl.Controls.Add(flp);
            Text = Properties.Resources.ResourceManager.GetString("Message");
            Controls.Add(pl);
            Controls.Add(flpButtons);
            FormBorderStyle = FormBorderStyle.None;
            BackColor = Color.FromArgb(35, 35, 35);
            StartPosition = FormStartPosition.CenterScreen;
            Padding = new Padding(20, 55, 20, 20);
            Width = 500;
            Height = 155;
        }
        public static Pen pen = null;
        public static SolidBrush brush = null;
        void btnOK_Click(object sender, EventArgs e)
        {
            if (g != null)
                g.Dispose();
            if (pen != null)
                pen.Dispose();
            if (btnOK != null)
                btnOK.Dispose();
            if (flpButtons != null)
                flpButtons.Dispose();
            if (flp != null)
                flp.Dispose();
            if (pl != null)
                pl.Dispose();
            if (brush != null)
                brush.Dispose();
            if (dialog != null)
                dialog.Dispose();
            if (panel2 != null)
                panel2.Dispose();
            if (label1 != null)
                label1.Dispose();
            Dispose();
        }
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ClassStyle |= CS_DROPSHADOW;
                return cp;
            }
        }
        public static DialogNoBox dialog = null;
        public static string Show(string message)
        {
            dialog = new DialogNoBox();
            dialog.lblMessage.Text = message;
            dialog.ShowDialog();
            dialog.TopMost = true;
            dialog.BringToFront();
            dialog.Activate();
            dialog.TopMost = false;
            return null;
        }
        public static Graphics g = null;
        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            if (e != null)
                g = e.Graphics;
            Rectangle rect = new Rectangle(new Point(0, 0), new Size(Width - 1, Height - 1));
            pen = new Pen(Color.FromArgb(10, 100, 255));
            g.DrawRectangle(pen, rect);
        }
    }
}
